#========================================================
# subpackage
#
# brief: manage arch subpackages
#
# usage: subpackage(module_name)
#
# details: the packages are expected to exist in the module_name subdirectory.
#          Creates install targets for the header files (including detail directory).
#========================================================
macro(subpackage _subpackage_name)
    add_subdirectory(${_subpackage_name})
    file(GLOB _include_files "${CMAKE_CURRENT_SOURCE_DIR}/${_subpackage_name}/*.h")
    install(FILES ${_include_files} DESTINATION ${INCLUDE_INSTALL_DIR}/panda/arch/${_subpackage_name})
    install(DIRECTORY ${_subpackage_name}/detail DESTINATION ${INCLUDE_INSTALL_DIR}/panda/arch/${_subpackage_name})
endmacro(subpackage)
