# CMake modules that are exported for use externally when Panda is installed.
# APPEND the required files to INSTALLABLE_CMAKE_FILES

#========================================================
# export_cmake_module
#
# brief: install a cmake module file into the MODULES_INSTALL_DIR location
#
# usage: export_cmake_module(module_file export_variables)
#
# details: module_file is the cmake module file to export.
#          export_variables passed will be expanded with current
#          values and inserted as default values for the module.
#========================================================
function(export_cmake_module module_file export_variables)
    set(export_variables_temp ${export_variables} ${ARGN})
    set(out_file "${module_file}")
    set(out_file_tmp "${CMAKE_BINARY_DIR}/${out_file}.in")
    file(WRITE ${out_file_tmp} "# ---- begin system specific default values -----\n")
    foreach(cachevar ${export_variables_temp})
        if(${cachevar})
            file(APPEND ${out_file_tmp} "if(NOT ${cachevar})\n")
            file(APPEND ${out_file_tmp} "    set(${cachevar} ${${cachevar}})\n")
            file(APPEND ${out_file_tmp} "endif()\n")
        endif(${cachevar})
    endforeach()
    file(APPEND ${out_file_tmp} "# ---- end system specific default values -----\n\n")
    file(READ "${CMAKE_SOURCE_DIR}/${module_file}" FILE_CONTENTS)
    file(APPEND ${out_file_tmp} "${FILE_CONTENTS}")
    configure_file(${out_file_tmp} ${out_file} COPYONLY)
    install(FILES ${CMAKE_BINARY_DIR}/${out_file} DESTINATION ${MODULES_INSTALL_DIR}/cmake)
endfunction(export_cmake_module)

# Doxygen
list(APPEND INSTALLABLE_CMAKE_FILES cmake/doxygen.cmake cmake/DoxyfileAPI.in)

# Boost
export_cmake_module("cmake/boost.cmake" "BOOST_INCLUDE_DIRS" "BOOST_LIBRARY_DIRS" "BOOST_LIBRARY_DIR_RELEASE" "BOOST_LIBRARY_DIR_DEBUG" "BOOST_DIR")

# Cuda
export_cmake_module("cmake/cuda.cmake" "CUDA_TOOLKIT_ROOT_DIR")

# Code Coverage
list(APPEND INSTALLABLE_CMAKE_FILES cmake/code_coverage.cmake)

# Git Version
list(APPEND INSTALLABLE_CMAKE_FILES cmake/git_version.cmake)

# Git Pre-Commit Hooks
list(APPEND INSTALLABLE_CMAKE_FILES cmake/git_pre_commit_hooks.cmake)

# Google Test
list(APPEND INSTALLABLE_CMAKE_FILES cmake/googletest.cmake)

# OpenCL
export_cmake_module("cmake/opencl.cmake" "OPENCL_INCLUDE_DIR" "OPENCL_LIBRARIES")

# Valgrind
list(APPEND INSTALLABLE_CMAKE_FILES cmake/valgrind.cmake)

# Dependency Registration tooling
list(APPEND INSTALLABLE_CMAKE_FILES cmake/dependency_register.cmake)

# And finally add this list of files to the cmake subdirectory of the install directory
install(FILES ${INSTALLABLE_CMAKE_FILES} DESTINATION ${MODULES_INSTALL_DIR}/cmake)
