# googletest build - must be built with the same compiler flags

if(NOT SKA_PANDA_GTEST_GUARD_VAR)
    set(SKA_PANDA_GTEST_GUARD_VAR TRUE)
else()
    return()
endif()

if(APPLE)
    add_definitions(-DGTEST_USE_OWN_TR1_TUPLE=1)
else()
    add_definitions(-DGTEST_USE_OWN_TR1_TUPLE=0)
endif()

add_subdirectory("thirdparty/googletest")
set(GTEST_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/thirdparty/googletest/include)
set(GTEST_LIBRARY_DIR ${PROJECT_BINARY_DIR}/thirdparty/googletest)
set(GTEST_LIBRARIES gtest)
