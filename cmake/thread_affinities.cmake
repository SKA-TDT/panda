if(NOT SKA_PANDA_THREAD_AFFINITIES_GUARD_VAR)
    set(SKA_PANDA_THREAD_AFFINITIES_GUARD_VAR TRUE)
else()
    return()
endif()

# Find threading library that allows us to set affinities
set(THREADS_PREFER_PTHREAD_FLAG ON)

find_package(Threads)

if(CMAKE_USE_PTHREADS_INIT)
    if(NOT APPLE)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DHAS_PTHREAD_SET_AFFINITY")
    endif()
endif()
