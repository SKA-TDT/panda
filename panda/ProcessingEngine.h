/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_PROCESSINGENGINE_H
#define SKA_PANDA_PROCESSINGENGINE_H

#include "panda/Engine.h"
#include "panda/Thread.h"
#include <thread>
#include <atomic>


namespace ska {
namespace panda {
class ProcessingEngineConfig;

/**
 * @brief
 *     Manages an event loop and the threads associated with it
 *
 */
class ProcessingEngine
{
    BOOST_CONCEPT_ASSERT((EngineConcept<ProcessingEngine>));

    public:
        ProcessingEngine(ProcessingEngineConfig const& config);
        ~ProcessingEngine();
        ProcessingEngine(ProcessingEngine const&) = delete;
        ProcessingEngine(ProcessingEngine&&) = delete;

        operator panda::Engine&();

        /**
         * @brief send an event for processing by the engine
         * @details asynchronous call
         */
        template<class Handler>
        void post(Handler fn);

        /**
         * @brief poll one handler from the io_service
         */
        std::size_t poll_one();

        /**
         * @return the number of dedicated threads
         */
        std::size_t number_of_threads() const;

        /**
         * @return wait for threads to complete their current tasks
         */
        void join();

        /**
         * @return the threads comprising this engine for inspection purposes
         */
        std::vector<std::unique_ptr<Thread>> const& threads() const {return _threads;}

    private:
        std::atomic_bool _exit;
        std::vector<std::unique_ptr<Thread>> _threads;
        panda::Engine _event_loop;
        boost::asio::io_service::work _work;
};

} // namespace panda
} // namespace ska
#include "panda/detail/ProcessingEngine.cpp"

#endif // SKA_PANDA_PROCESSINGENGINE_H
