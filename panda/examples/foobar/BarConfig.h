#include "CommonDefs.h"
#include "panda/ConfigModule.h"
#include "panda/PoolSelector.h"

namespace foobar {

class BarConfig : public ska::panda::PoolSelector<PoolManagerType, ska::panda::ConfigModule>
{
        typedef ska::panda::PoolSelector<PoolManagerType, ska::panda::ConfigModule> BaseT;

    public:
        BarConfig(PoolManagerType& pool_manager)
            : BaseT(pool_manager, "bar")
        {
        }
};

} // namespace foobar
