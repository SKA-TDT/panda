#include "panda/Config.h"

namespace foobar {

typedef ska::panda::Config<ska::panda::Cpu> ConfigBaseT;

typedef typename ConfigBaseT::PoolManagerType PoolManagerType;

} // namespaace foobar
