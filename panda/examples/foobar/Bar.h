#include "BarConfig.h"
#include <iostream>

namespace foobar {

class Bar {
    public:
        // This tells the pool scheduler that Bar can only be run on a CPU core
        typedef ska::panda::Cpu Architecture;

    public:
        Bar(BarConfig const& config)
            : _config(config)
        {}

        // launch async task to process msg
        void operator()(std::string const& msg)
        {
            _config.pool().submit(*this, msg);
        }

        //! [Sync operator]
        // sync calls
        // called by ResourcePool when a suitable resource becomes available
        void operator()(ska::panda::PoolResource<Architecture>&, std::string const& msg)
        {
            std::cout << msg << ", and bar with a " + ska::panda::template to_string<Architecture>() + " device";
            std::cout << "\n";
        }
        //! [Sync operator]

    private:
        BarConfig const& _config;
};

} // namespace foobar
