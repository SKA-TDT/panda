/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_UTILS_TYPETAG_H
#define SKA_PANDA_UTILS_TYPETAG_H


namespace ska {
namespace panda {

/**
 * @brief Tag for specialisation of any given type T
 * @details Use to specialise/overload a function based on a type without actually having to provide
 *          the type itself.
 * @tparam type : typedef for the Type wrapped by the Tag
 * e.g.
 * @code
 *
 * class {
 *  public:
 *      MyClass(int v) : _v(v) {}
 *
 *      template<class Functor>
 *      void forward_as(Functor const& functor, TypeTag<MyType>()) {
 *          functor(MyType(_v));
 *      }
 *  private:
 *      int _v;
 * };
 * @endcode
 */
template<typename T>
struct TypeTag {
    typedef T type;
};


} // namespace panda
} // namespace ska

#endif // SKA_PANDA_UTILS_TYPETAG_H
