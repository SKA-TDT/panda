/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_FILL_H
#define SKA_PANDA_FILL_H

#include "panda/DeviceIterator.h"
#include <iterator>
#include <type_traits>

namespace ska {
namespace panda {

/**
 * @brief device independent version of std::fill
 * @details fill all elements from begin to end with the value provided
 */
template<typename SrcIteratorType, typename EndIteratorType>
inline void fill(SrcIteratorType&& begin, EndIteratorType&& end, typename std::remove_pointer<typename std::iterator_traits<typename std::remove_reference<SrcIteratorType>::type>::value_type>::type const&);

} // namespace panda
} // namespace ska

#include "panda/detail/Fill.cpp"

#endif // SKA_PANDA_FILL_H
