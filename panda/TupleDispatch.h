/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TUPLEDISPATCH_H
#define SKA_PANDA_TUPLEDISPATCH_H


namespace ska {
namespace panda {

// helper functions to generate a number sequence
template<int ...>
struct seq { };

template<int N, int ...S>
struct gens : gens<N-1, N-1, S...> { };

template<int ...S>
struct gens<0, S...> {
  typedef seq<S...> type;
};


/**
 * @brief A sequence of integers corresponding to the elements in a tuple
 *        Allow you to iterate over the entire tuple
 */
template<typename TupleType>
struct TupleUnpackSequence : public gens<std::tuple_size<TupleType>::value>::type {};

/**
 * @brief
 *     expands a std::tuple and uses each element as a separate argument to a function
 * @details
 * see http://stackoverflow.com/questions/7858817/unpacking-a-tuple-to-call-a-matching-function-pointer
 * 
 */
template<typename FunctionType, typename... Args >
class TupleDispatch
{
    public:
        typedef typename std::tuple<Args...> TupleType;
        //typedef typename std::function<void(Args...)> FunctionType;

    public:
        TupleDispatch(FunctionType const& fn);
        void exec(TupleType&& tuple);
    
        void reset(FunctionType const& fn);

    private:
        template<int ...S> void _dispatch(TupleType&& tuple, seq<S...>);

        typedef typename gens<sizeof...(Args)>::type UnpackSequence;
        FunctionType _fn;
};

} // namespace panda
} // namespace ska

#include "detail/TupleDispatch.cpp"

#endif // SKA_PANDA_TUPLEDISPATCH_H 
