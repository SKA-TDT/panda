/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_POOLCONFIG_H
#define SKA_PANDA_POOLCONFIG_H


#include "panda/ConfigModule.h"
#include "panda/DeviceConfig.h"
#include "panda/ProcessingEngineConfig.h"
#include "panda/ArchitectureConcept.h"
#include <string>

namespace ska {
namespace panda {

/**
 * @brief
 *    Individual Pool configuration
 *
 * @details
 *
 */
template<typename... Architectures>
class PoolConfig : public ConfigModule
{
//      typedef Concept<ArchitectureConcept>::Check<Architectures...> ArchitectureConceptCheckArchitectures;

    public:
        PoolConfig(std::string name = std::string("pool"));
        ~PoolConfig();

        /**
         * @brief the number of resources of a specified architecture that has been specified
         **/
        template<typename Architecture>
        std::size_t device_count() const;

        /**
         * @brief the number of devices of all architectures that are specified
         **/
        unsigned total_device_count() const;

        /**
         * @brief the engine configuration that drives this pool
         **/
        ProcessingEngineConfig& engine_config();
        ProcessingEngineConfig const& engine_config() const;

        /**
         * @brief set the option descriptions for the Pool configuration
         */
        void add_options(OptionsDescriptionEasyInit& opts) override;

        // @return the tag mapped to a specific architecture
        template<typename Architecture>
        static inline
        std::string const& tag();

    private:
        ProcessingEngineConfig _engine_config;
};

} // namespace panda
} // namespace ska
#include "panda/detail/PoolConfig.cpp"

#endif // SKA_PANDA_POOLCONFIG_H
