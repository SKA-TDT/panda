/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CUMULATIVEDELAYERRORPOLICY_H
#define SKA_PANDA_CUMULATIVEDELAYERRORPOLICY_H

#include "panda/ErrorPolicyConcept.h"

#include <chrono>
#include <vector>

namespace ska {
namespace panda {
class Error;


static std::vector<std::chrono::seconds> default_intervals = {
    std::chrono::seconds(0),
    std::chrono::seconds(1),
    std::chrono::seconds(5),
    std::chrono::seconds(10),
    std::chrono::seconds(30),
    std::chrono::seconds(60),
    std::chrono::minutes(3),
    std::chrono::minutes(5),
    std::chrono::minutes(10),
    std::chrono::minutes(20)
};

/**
 * @brief
 *    A connection error policy.
 * @details
 * 
 *   first error  : an immediate retry.
 *   second error : a delay of 1 second
 *   third error  : a delay of 5 seconds
 *   forth error  : a delay of 10 seconds
 *   fifth error  : a delay of 30 seconds
 *   sixth error  : a delay of 1 minute
 *   seventh error : a delay of 3 minutes
 *   eighth error : a delay of 5 minutes
 *   ninth error : a delay of 10 minutes
 *   tenth error : a delay of 20 minutes
 *   subsequent errors : 20 mins
 *
 *   Any succesful connection will result in the count being reset.
 * \ingroup connectivity
 * @{
 */
class CumulativeDelayErrorPolicy
{
    BOOST_CONCEPT_ASSERT((ErrorPolicyConcept<CumulativeDelayErrorPolicy>));

    public:
        typedef std::vector<std::chrono::seconds> IntervalType;

    public:
        CumulativeDelayErrorPolicy(IntervalType intervals = default_intervals );
        ~CumulativeDelayErrorPolicy();

        /**
         * @return true if connection should be made immediately
         */
        bool connection_error(Error const& error);

        /**
         * @return true if connection should be made immediately
         */
        bool error(Error const& error);

        /**
         * @return true if connection should be made immediately
         */
        bool send_error(Error const& error);

        bool try_connect();

        void connected();

    private:
        unsigned _error_count;
        IntervalType _intervals;
        std::chrono::time_point<std::chrono::system_clock> _last_error;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_CUMULATIVEDELAYERRORPOLICY_H
/**
 *  @}
 *  End Document Group
 **/
