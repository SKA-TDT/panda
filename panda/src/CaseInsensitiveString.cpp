/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/CaseInsensitiveString.h"


namespace ska {
namespace panda {


bool CaseInsensitiveStringCharTraits::eq(char c1, char c2) { return toupper(c1) == toupper(c2); }
bool CaseInsensitiveStringCharTraits::ne(char c1, char c2) { return toupper(c1) != toupper(c2); }
bool CaseInsensitiveStringCharTraits::lt(char c1, char c2) { return toupper(c1) <  toupper(c2); }

int CaseInsensitiveStringCharTraits::compare(const char* s1, const char* s2, size_t n) {
    while( n-- != 0 ) {
        if( toupper(*s1) < toupper(*s2) ) return -1;
        if( toupper(*s1) > toupper(*s2) ) return 1;
        ++s1; ++s2;
    }
    return 0;
}

const char* CaseInsensitiveStringCharTraits::find(const char* s, int n, char a) {
    while( n-- > 0 && toupper(*s) != toupper(a) ) {
        ++s;
    }
    return s;
}

CaseInsensitiveString::CaseInsensitiveString()
{
}

CaseInsensitiveString& CaseInsensitiveString::operator+=(std::string const& s)
{
    static_cast<BaseT&>(*this) += s.c_str();
    return *this;
}

CaseInsensitiveString::CaseInsensitiveString(std::string const& s)
    : std::basic_string<char, CaseInsensitiveStringCharTraits>(s.c_str())
{
}

} // namespace panda
} // namespace ska

std::string operator+(std::string const& a, ska::panda::CaseInsensitiveString const& b)
{
    return a + b.c_str();
}

std::istream& operator>>(std::istream& is, ska::panda::CaseInsensitiveString& str)
{
    std::string tmp;
    is >> tmp;
    str = tmp;
    return is;
}

std::ostream& operator<<(std::ostream& os, ska::panda::CaseInsensitiveString const& str)
{
    os << str.c_str();
    return os;
}
