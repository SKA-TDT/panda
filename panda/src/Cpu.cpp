/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Cpu.h"
#include "panda/Log.h"
#include <iostream>
#include <iterator>

namespace ska {
namespace panda {

Cpu::Cpu()
    : Architecture("Cpu")
{
}

Cpu::~Cpu()
{
}

PoolResource<Cpu>::PoolResource(unsigned int)
    : _thread(_affinities)
{
}

PoolResource<Cpu>::~PoolResource()
{
}

void PoolResource<Cpu>::configure(DeviceConfig<Cpu> const& dc)
{
    _affinities=dc.affinities();
    _thread.set_core_affinity(_affinities);
}

std::size_t PoolResource<Cpu>::device_memory() const
{
    return std::allocator_traits<std::allocator<char>>::max_size(std::allocator<char>());
}

std::vector<unsigned> const& PoolResource<Cpu>::affinities() const {
    return _affinities;
}

std::vector<std::shared_ptr<PoolResource<Cpu>>> DiscoverResources<Cpu>::operator()() {
    std::vector<std::shared_ptr<PoolResource<Cpu>>> res;

    unsigned num_cores = std::thread::hardware_concurrency();
    if(num_cores ==0) num_cores = 4; // if unable to determine cores, make a guess
    for(unsigned int i = 0; i < num_cores; ++i) {
        res.emplace_back(std::make_shared<PoolResource<Cpu>>(i));
    }
    PANDA_LOG << "Cpu cores : " << num_cores << "\n";
    return res;
}

DeviceConfig<Cpu>::DeviceConfig()
{
}

DeviceConfig<Cpu>::DeviceConfig(DeviceConfig const& cpy)
    : BaseT(cpy)
    , _affinities(cpy._affinities)
{
}

DeviceConfig<Cpu>& DeviceConfig<Cpu>::operator=(DeviceConfig const& config)
{
    _affinities=config._affinities;
    _device_count = config.device_count();
    return *this;
}


std::string const& DeviceConfig<Cpu>::tag_name()
{
    static const std::string tag("cpu");
    return tag;
}

void DeviceConfig<Cpu>::add_options(OptionsDescriptionEasyInit& add_options) {
    add_options
    ("affinity", boost::program_options::value<CommaSeparatedOptions<unsigned>>(&_affinities)->multitoken(), "specify the cores to run on (e.g. 0,1,2,3)");

    DeviceConfigBase<DeviceConfig<Cpu>>::add_options(add_options);
}

std::vector<unsigned> const& DeviceConfig<Cpu>::affinities() const
{
    return static_cast<std::vector<unsigned> const&>(_affinities);
}

void DeviceConfig<Cpu>::affinities(std::vector<unsigned> const& affinities)
{
    _affinities = affinities;
}

} // namespace panda
} // namespace ska
