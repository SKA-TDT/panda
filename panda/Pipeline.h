/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_PIPELINE_H
#define SKA_PANDA_PIPELINE_H

#include "panda/DataManager.h"
#include "panda/TupleDispatch.h"
#include <functional>

namespace ska {
namespace panda {

template<typename... Args>
struct FunctionOfTupleType;

template<typename... Args>
struct FunctionOfTupleType<std::tuple<Args...>>
{
    typedef std::function<void(typename panda::DataType<Args>::type&...)> type;
};

template<typename FunctionType, typename... Args>
struct TupleDispatchType;

template<typename FunctionType, typename... Args>
struct TupleDispatchType<FunctionType, std::tuple<Args...>>
{
    typedef TupleDispatch<FunctionType, Args...> type;
};

// Functor to call a variadic list of shared_ptr args to the equivalent derefreferenced call
template<typename Fn, typename... Args>
struct shared_to_ref_call
{
    public:
        shared_to_ref_call(Fn const& fn) : _fn(fn) {}

        void operator()(std::shared_ptr<Args>&&... args) {
            _fn(std::forward<Args&>(*args)...);
        }

    private:
        Fn _fn;
};

template<typename FunctionType, typename Pack>
struct UnpackTupleType;

template<typename FunctionType, typename... Args>
struct UnpackTupleType<FunctionType, std::tuple<Args...>>
{
    typedef shared_to_ref_call<FunctionType, Args...> type;
};


/**
 * @brief
 *    All Pipelines should Inherit from this class
 * @details
 *    Provides the interface between a data producer and a specific functor that represents
 *    the desired computation.
 *    This provides strong type checking, a standard function syntax and compile
 *    compile time resolution.
 *
 *  \ingroup resource
 *  @{
 */

template<class PipelineTraits>
class Pipeline
{
    public:
        typedef PipelineTraits ProducerType;

    private:
        typedef typename panda::DataType<typename ProducerType::DataSetType>::type DataTypes;

    public:
        typedef typename FunctionOfTupleType<DataTypes>::type FunctionType;

    private:
        typedef typename DataManager<ProducerType>::DataSetType DataSetType;
        typedef typename UnpackTupleType<FunctionType, DataTypes>::type DispatchFunctionType;
        typedef typename TupleDispatchType<DispatchFunctionType, DataSetType>::type DispatcherType;


    public:
        Pipeline(ProducerType& chunker, FunctionType const& runnable);
        ~Pipeline();

        /**
         * @brief start the pipeline running (by calling start())
         * @return 0 for success, or 1 for an error
         * Will not throw. Also see start().
         */
        int exec();

        /**
         * @brief start the pipeline running
         * @throw this member function may throw - also see exec()
         */
        void start();

        /**
         * @brief halt the pipeline at the end of the current processing
         *        iteration (as defined by run()). returns immediately
         */
        void stop();

        /**
         * @brief will return true if start() or exec() has been called and stop() has not been called
         */
        bool is_running() const;

        /**
         * @brief replace the current running function with the provided function
         */
        void set_function(FunctionType const& fn);

    private:
        DataManager<ProducerType> _data;
        bool _run;
        DispatcherType _dispatcher;
};

} // namespace panda
} // namespace ska

#include "panda/detail/Pipeline.cpp"
#endif // SKA_PANDA_PIPELINE_H
/**
 *  @}
 *  End Document Group
 **/
