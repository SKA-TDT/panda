/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_PRODUCERCONCEPT_H
#define SKA_PANDA_PRODUCERCONCEPT_H

#include "panda/Concept.h"
#include "panda/TupleUtilities.h"

namespace ska {
namespace panda {
/**
 * \ingroup concepts
 * @{
 * @class ProducerConcept
 * @brief The ProducerConcept class checks the requirements on a Producer.
 *
 * @details
 * The Producer must provide the following (possible private) methods
 * @code
 *
 * // called when the Producer should halt either temporaily or before shutdown
 * void stop(); 
 *
 * // called when a client is waiting for data
 * // its OK to do nothing here if you have threads internal to the class working
 * // independently
 * bool process();
 *
 * // called as soon as a DataManager becomes available, and before any call to process()
 * void init();
 *
 * @endcode
 * 
 * DataTypes typedef must be publicly defined. It should be a ```std::tuple``` of the
 *                   data types that will be filled by the Producer
 *
 */

template <class X>
struct ProducerConcept: Concept<X> {
        typedef Concept<X> Base;


//     Can't do this because set_chunk_manager() is private (!)
//      typedef typename X::ChunkManagerType ChunkManagerType;

    public:
        BOOST_CONCEPT_USAGE(ProducerConcept) {
            typedef typename X::DataSetType DataSetType;
            static_assert(panda::is_tuple<DataSetType>::value, "DataTypes should be a std::tuple");

            X& x(Base::reference());

            /// Producer concept classes must provide a public stop() method
            x.stop();
        }
};

/**
 *  @}
 *  End Document Group
 **/

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_PRODUCERCONCEPT_H
