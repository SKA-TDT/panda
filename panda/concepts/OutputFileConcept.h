/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_OUTPUTFILECONCEPT_H
#define SKA_PANDA_OUTPUTFILECONCEPT_H


#include "panda/Concept.h"
#include <fstream>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
namespace ska {
namespace panda {

/**
 * @brief
 *    checks the concepts to support writing data files is supported
 *
 * @details
 * \ingroup concepts
 * @{
 */
template <class X, typename DataType>
struct OutputFileConcept : public Concept<X>
{
        typedef Concept<X> BaseT;

    public:
        BOOST_CONCEPT_USAGE(OutputFileConcept) {
            std::ofstream os;
            /** 
             * @method consistent_with_existing_file
             *         bool consistent_with_existing_file(DataType const&);
             * @param  ensure the data type provided is consistent with the last headers() call 
             * @brief  called when asked to write data to check if the data is consistent with the
             *         currently opened file.
             * @return true if its OK to write in the file, false if a new file should be generated
             * @details default implementation always returns true
             */
             decltype(std::declval<X>().consistent_with_existing_file(std::declval<DataType>())) n1 = true;

            /** 
             * @brief insert headers into a file when it is first opened
             * @method void headers(std::ostream&, DataType const&)
             */ 
             static_assert(std::is_same<void, decltype(std::declval<X>().headers(os, std::declval<DataType>()))>::value, "OutputFileConcept missing headers(std::ostream&, DataType const&)") ;

            /**
             * @brief insert footers into a file just before file is closed
             * @method void footers(std::ostream&)
             */
             static_assert(std::is_same<void, decltype(std::declval<X>().footers(os))>::value, "OutputFileConcept missing footers(std::ostream&) method");

            /**
             * @brief method that streams the data provided to an existing stream
             * @nethod write(std::ostream&, DataType const&)
             */
             static_assert(std::is_same<void, decltype(std::declval<X>().write(os, std::declval<DataType>()))>::value, "OutputFileConcept missing write(std::ostream&, DataType const&) method");
        }
};

} // namespace panda
} // namespace ska

/**
 *  @}
 *  End Document Group
 **/

#pragma GCC diagnostic pop
#endif // SKA_PANDA_OUTPUTFILECONCEPT_H 
