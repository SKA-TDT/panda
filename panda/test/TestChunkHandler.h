/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TEST_TESTCHUNKHANDLER_H
#define SKA_PANDA_TEST_TESTCHUNKHANDLER_H

#include "panda/test/TestHandler.h"
#include <memory>
#include <type_traits>

namespace ska {
namespace panda {
namespace test {

/**
 * \ingroup testutil
 * @{
 */

/**
 * @brief
 *     A TestHandler that accepts any one of the templated ChunkTypes as its argument
 *
 * @details
 *     The chunk type passed is retained
 *     Ensure to set blocking to false if using sync mode
 */
template<class ChunkType, typename... ChunkTypes>
class TestChunkHandler : public TestChunkHandler<ChunkTypes...>
{
        typedef TestChunkHandler<ChunkTypes...> BaseT;

    public:
        TestChunkHandler(bool blocking=false);

        /**
         * @brief function to be called on execution
         */
        void operator()(ChunkType& chunk);
        void operator()(std::shared_ptr<ChunkType> const& chunk);

        /**
         * @brief reset the handler to its pre-executed state
         */
        void reset();

        /**
         * @brief return the chunk that was sent when handler was executed
         */
        template<typename DataType
                ,typename std::enable_if<std::is_same<DataType, ChunkType>::value
                                        , bool>::type = true
                >
        ChunkType const& data() const;

    private:
        mutable std::shared_ptr<ChunkType> _chunk;
};


template<class ChunkType>
class TestChunkHandler<ChunkType>
{

    public:
        TestChunkHandler(bool blocking=false);
        ~TestChunkHandler();

        /**
         * @brief function to be called on execution
         */
        void operator()(ChunkType& chunk);
        void operator()(std::shared_ptr<ChunkType> const& chunk);

        /**
         * @brief return the data that was sent when handler was executed
         * @details use this interface when multiple datatypes are involved
         */
        template<typename DataType
                ,typename std::enable_if<std::is_same<DataType, ChunkType>::value
                                        , bool>::type = true
                >
        ChunkType const& data() const;

        /**
         * @brief return the chunk that was sent when handler was executed
         */
        typename std::remove_const<ChunkType>::type const& chunk() const;

        /**
         * @brief reset the handler to its pre-executed state
         */
        void reset();

        /**
         * @brief wait (block) until the handler has completed
         */
        void wait() const;

        /**
         * @brief wait (block) until the handler has completed or timeout is reached
         * @returns true if completed succesfully, false if the timeout has been reached
         */
        bool wait(std::chrono::seconds const& timeout) const;

        /**
         * @returns true if this handler has been executed, otherwise false
         */
        bool executed() const;

    private:
        // make sure we cannot call this by mistake by making it private
        void operator()();

    private:
        class TestChunkHandlerImpl;

    protected:
        std::shared_ptr<TestChunkHandlerImpl> _pimpl; // share data between handler copies
};

/**
 *  @}
 *  End Document Group
 **/

} // namespace test
} // namespace panda
} // namespace ska
#include "panda/test/detail/TestChunkHandler.cpp"

#endif // SKA_PANDA_TEST_TESTCHUNKHANDLER_H
