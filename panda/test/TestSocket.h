/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TESTSOCKET_H
#define SKA_PANDA_TESTSOCKET_H

#include "panda/Error.h"
#include "panda/Engine.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#include <boost/asio.hpp>
#pragma GCC diagnostic pop
#include <iostream>

namespace ska {
namespace panda {

/**
 * \ingroup testutil
 * @{
 */

/**
 * TODOdoc
 */
class Engine;

/**
 * @brief
 *   A dummy network socket
 */

class TestSocketBase {
    public:
        typedef std::function<void(TestSocketBase&)> OnConnectFunction;
        typedef OnConnectFunction OnSendFunction;
        typedef std::function<void(TestSocketBase&)> OnBindFunction;

    public:
        TestSocketBase(Engine& engine);
        ~TestSocketBase();

        // -----------------------
        // -- testing interface --
        // -----------------------
        /**
         * @brief allow all the bytes to be sent in the next send call
         */
        void send_all();

        /**
         * @brief allow all the bytes to be sent in the next recv call
         */
        void recv_all();

        /**
         * @brief restrict the max number of bytes to be consumed in the next send call
         */
        void send_max_bytes(unsigned max);

        /**
         * @brief restrict the max number of bytes to be consumed in the next recv call
         */
        void recv_max_bytes(unsigned max);

        /**
         * @brief set the error to open whilst transmitting data after a succesful connection
         */
        void reset_open_error();
        void set_open_error(boost::system::error_code const& code);

        /**
         * @brief set the error to send on connection
         */
        void reset_connection_error();
        void set_connection_error(boost::system::error_code const& code);

        /**
         * @brief set the error to send whilst transmitting data after a succesful connection
         */
        void reset_send_error();
        void set_send_error(boost::system::error_code const& code);

        /**
         * @brief set the error to recv whilst transmitting data after a succesful connection
         */
        void reset_recv_error();
        void set_recv_error(boost::system::error_code const& code);

        /**
         * @brief set a function to call whenever a connection is requested
         */
        void set_on_connect_function(OnConnectFunction const&);

        /**
         * @brief set a function to call whenever a send is requested
         */
        void set_on_send_function(OnSendFunction const&);

        /**
         * @brief set a function to call whenever a recv is requested
         */
        void set_on_recv_function(OnSendFunction const&);

        /**
         * @brief set a function to call whenever a bind is requested
         */
        void set_on_bind_function(OnBindFunction const&);

        /**
         * @brief indicate closure of the connection
         */
        void close();

        /**
         * @brief return true if open() has been called on the socket
         */
        bool is_open() const;

    protected:
        Engine& _engine;
        long _max_bytes_to_send;
        long _max_bytes_to_recv;
        bool _connected;
        bool _open;
        boost::system::error_code _connect_error;
        boost::system::error_code _send_error;
        boost::system::error_code _open_error;
        boost::system::error_code _recv_error;
        OnConnectFunction _on_connect_function;
        OnSendFunction _on_send_function;
        OnSendFunction _on_recv_function;
        OnBindFunction _on_bind_function;
};

template<typename Protocol>
class TestSocket : public TestSocketBase
{
    public:
        typedef boost::asio::ip::basic_endpoint<Protocol> EndPointType;

    public:
        TestSocket(Engine& engine);
        TestSocket(TestSocket const&) = delete;
        TestSocket(TestSocket&&) = default;
        ~TestSocket();

        // ----------------------
        // -- socket interface --
        // ----------------------
        void open(Protocol const&);
        void open(Protocol const&, boost::system::error_code& ec);

        void async_connect(EndPointType const &, std::function<void(boost::system::error_code const&)> const& handler)
        {
            if(!_open) { throw panda::Error("attempt to connect on an unopened scoket"); }
            if(_on_connect_function) {
                _engine.post( [this, handler](){ 
                                            _on_connect_function(*this); 
                                            if(!_connect_error) _connected = true;
                                            _engine.post(std::bind(handler, _connect_error));
                                      } );
            }
            else {
                if(!_connect_error) _connected = true;
                _engine.post(std::bind(handler, _connect_error));
            }
        }

        template <typename ConstBufferSequence, typename EndPoint, typename WriteHandler>
#ifdef BOOST_ASIO_INITFN_RESULT_TYPE
        BOOST_ASIO_INITFN_RESULT_TYPE(WriteHandler, void (boost::system::error_code, std::size_t))
#else
        void
#endif
        async_send_to(const ConstBufferSequence& buffers, EndPoint const&, BOOST_ASIO_MOVE_ARG(WriteHandler) handler)
        {
            std::size_t max_to_send = 0;
            if(_max_bytes_to_send < 0 ) { 
                max_to_send = boost::asio::buffer_size(buffers);
            }
            else {
                max_to_send = (std::size_t)_max_bytes_to_send;
            }
            if(_on_send_function) {
                _on_send_function(*this);
            }
            _engine.post(std::bind(handler, _send_error, max_to_send));            
        } 

        template <typename BufferSequence, typename WriteHandler>
        void async_receive(BufferSequence buffer, BOOST_ASIO_MOVE_ARG(WriteHandler) handler)
        {
            std::size_t max_to_recv = 0;
            if(_max_bytes_to_recv < 0 ) { 
                max_to_recv = boost::asio::buffer_size(buffer);
            }
            else {
                max_to_recv = (std::size_t)_max_bytes_to_recv;
            }
            if(_on_recv_function) {
                _on_recv_function(*this);
            }
            if(!_connected) {
                std::cout << "TestSocket warning: attempt to call async_receive without being connected" << std::endl;
            }
            _engine.post(std::bind(handler, _recv_error, max_to_recv));            
        } 


        template <typename BufferSequence, typename EndPoint, typename WriteHandler>
        void async_receive_from(BufferSequence buffer, EndPoint&, BOOST_ASIO_MOVE_ARG(WriteHandler) handler)
        {
            std::size_t max_to_recv = 0;
            if(_max_bytes_to_recv < 0 ) { 
                max_to_recv = boost::asio::buffer_size(buffer);
            }
            else {
                max_to_recv = (std::size_t)_max_bytes_to_recv;
            }
            if(_on_recv_function) {
                _on_recv_function(*this);
            }
            _engine.post(std::bind(handler, _recv_error, max_to_recv));            
        } 

        template<typename EndPoint>
        void bind(EndPoint&, boost::system::error_code&) {
            if(_on_bind_function) {
                _on_bind_function(*this);
            }
        }
};

template<typename Protocol>
TestSocket<Protocol>::TestSocket(Engine& engine)
    : TestSocketBase(engine)
{
}

template<typename Protocol>
TestSocket<Protocol>::~TestSocket()
{
}

template<typename Protocol>
void TestSocket<Protocol>::open(Protocol const& p)
{
    boost::system::error_code ec;
    this->open(p, ec);
    if(ec) {
        throw ec;
    }
}

template<typename Protocol>
void TestSocket<Protocol>::open(Protocol const&, boost::system::error_code& ec)
{
    ec = _open_error;
    _connected = false;
    _open = true;
}

/**
 *  @}
 *  End Document Group
 **/

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_TESTSOCKET_H
