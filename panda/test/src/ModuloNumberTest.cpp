/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ModuloNumberTest.h"
#include "panda/ModuloNumber.h"


namespace ska {
namespace panda {
namespace test {


ModuloNumberTest::ModuloNumberTest()
    : ::testing::Test()
{
}

ModuloNumberTest::~ModuloNumberTest()
{
}

void ModuloNumberTest::SetUp()
{
}

void ModuloNumberTest::TearDown()
{
}

TEST_F(ModuloNumberTest, test_constuctor)
{
    int const modulo=10;
    int const v1=5; // below modulo
    ModuloNumber<int> mv(modulo, v1);
    ASSERT_EQ(static_cast<int>(mv), v1);

    int const v2=modulo+1; // above modulo
    ModuloNumber<int> mv2(modulo, v2);
    ASSERT_EQ(static_cast<int>(mv2), 1);

    // constructed with value ==  modulo
    ModuloNumber<int> mv3(modulo, modulo);
    ASSERT_EQ(static_cast<int>(mv3), 0);
}

TEST_F(ModuloNumberTest, test_preincrement)
{
    int const modulo=10;
    int const v1=5; // well below modulo
    ModuloNumber<int> mv(modulo, v1);
    ModuloNumber<int>& rv = ++mv;
    ASSERT_EQ(static_cast<int>(rv), v1 + 1);
    ASSERT_EQ(&rv, &mv);

    // expect wrap around
    ModuloNumber<int> mv2(modulo, modulo - 1);
    ++mv2;
    ASSERT_EQ(static_cast<int>(mv2), 0);
}

TEST_F(ModuloNumberTest, test_predecrement)
{
    int const modulo=10;
    int const v1=5; // well below modulo
    ModuloNumber<int> mv(modulo, v1);
    ModuloNumber<int>& rv = --mv;
    ASSERT_EQ(static_cast<int>(rv), v1 - 1);
    ASSERT_EQ(&rv, &mv);

    // expect wrap around
    ModuloNumber<int> mv2(modulo, 0);
    --mv2;
    ASSERT_EQ(static_cast<int>(mv2), modulo - 1);
}

TEST_F(ModuloNumberTest, test_postincrement)
{
    int const modulo=10;
    int const v1=5; // well below modulo
    ModuloNumber<int> mv(modulo, v1);
    ModuloNumber<int> rv = mv++;
    ASSERT_EQ(static_cast<int>(rv), v1);
    ASSERT_EQ(static_cast<int>(mv), v1 + 1);

    // expect wrap around
    ModuloNumber<int> mv2(modulo, modulo - 1);
    ModuloNumber<int> rv2=mv2++;
    ASSERT_EQ(static_cast<int>(rv2), modulo - 1);
    ASSERT_EQ(static_cast<int>(mv2), 0);
}

TEST_F(ModuloNumberTest, test_postdecrement)
{
    int const modulo=10;
    int const v1=5; // well below modulo
    ModuloNumber<int> mv(modulo, v1);
    ModuloNumber<int> rv = mv--;
    ASSERT_EQ(static_cast<int>(rv), v1);
    ASSERT_EQ(static_cast<int>(mv), v1 - 1);

    // expect wrap around
    ModuloNumber<int> mv2(modulo, modulo);
    ModuloNumber<int> rv2=mv2--;
    ASSERT_EQ(static_cast<int>(rv2), 0);
    ASSERT_EQ(static_cast<int>(mv2), modulo - 1);
}

TEST_F(ModuloNumberTest, test_equality)
{
    int const modulo=10;
    int const v1=5; // well below modulo
    int const v2=0;
    ModuloNumber<int> mv(modulo, v1);
    ModuloNumber<int> mv2(modulo + 1, v1);
    ModuloNumber<int> mv3(modulo, v2);
    ASSERT_TRUE(mv2 == mv);
    ASSERT_FALSE(mv2 != mv);
    ASSERT_TRUE(mv3 != mv);
    ASSERT_TRUE(mv3 != mv2);
    ASSERT_FALSE(mv3 == mv);
    ASSERT_FALSE(mv3 == mv2);
}

TEST_F(ModuloNumberTest, test_plusequal)
{
    int const modulo=10;
    int const v1=5; // well below modulo
    ModuloNumber<int> mv(modulo, v1);
    ModuloNumber<int> mv2(modulo, v1);

    mv += v1;
    ASSERT_EQ(static_cast<int>(mv), 0);
    mv += mv2;
    ASSERT_EQ(static_cast<int>(mv), v1);
}

TEST_F(ModuloNumberTest, test_minusequal)
{
    int const modulo=10;
    int const v1=5; // well below modulo
    ModuloNumber<int> mv(modulo, v1);
    ModuloNumber<int> mv2(modulo, v1);

    mv -= v1;
    ASSERT_EQ(static_cast<int>(mv), 0);
    mv -= mv2;
    ASSERT_EQ(static_cast<int>(mv), v1);

    ModuloNumber<int> mv3(modulo, 1);
    mv3 -= 3;
    ASSERT_EQ(static_cast<int>(mv3), modulo - 2);
}

TEST_F(ModuloNumberTest, test_plus)
{
    int const modulo=10;
    int const v1=5; // well below modulo
    ModuloNumber<int> mv(modulo, v1);
    ModuloNumber<int> mv2(modulo, 8);

    ModuloNumber<int> result = mv + v1;
    ASSERT_EQ(static_cast<int>(result), 0);
    ModuloNumber<int> result2 = mv + mv2;
    ASSERT_EQ(static_cast<int>(result2), 3);
}

TEST_F(ModuloNumberTest, test_minus)
{
    int const modulo=10;
    int const v1=5; // well below modulo
    ModuloNumber<int> mv(modulo, v1);
    ModuloNumber<int> mv2(modulo, v1);

    ModuloNumber<int> result = mv - v1;
    ASSERT_EQ(static_cast<int>(result), 0);
    ModuloNumber<int> result2 = mv - mv2;
    ASSERT_EQ(static_cast<int>(result2), 0);

    ModuloNumber<int> result3 = mv - modulo;
    ASSERT_EQ(static_cast<int>(result3), static_cast<int>(mv));
}

TEST_F(ModuloNumberTest, test_less_than)
{
    int const modulo=10;
    // should be false when values are equal
    for(unsigned i=0; i<modulo; ++i) {
        ModuloNumber<unsigned> mv(modulo, i);
        ModuloNumber<unsigned> mv2(modulo, i);
        ASSERT_FALSE(mv < mv2);
        ASSERT_FALSE(mv2 < mv);
    }

    // when forward_distance(mv, mv2) is smaller than forward_distane(mv2, mv)
    for(unsigned i=0; i<modulo; ++i) {
        SCOPED_TRACE(i);
        for(unsigned j=1; j<modulo/2; ++j) {
            ModuloNumber<unsigned> mv(modulo, i);
            ModuloNumber<unsigned> mv2 = mv + j;
            ASSERT_TRUE(mv < mv2);
            ASSERT_FALSE(mv2 < mv);
        }
    }
}

} // namespace test
} // namespace panda
} // namespace ska
