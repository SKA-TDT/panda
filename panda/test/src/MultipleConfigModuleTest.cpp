/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "MultipleConfigModuleTest.h"
#include "panda/MultipleConfigModule.h"
#include "panda/ConfigModule.h"
#include "panda/ConfigActive.h"


namespace ska {
namespace panda {
namespace test {


MultipleConfigModuleTest::MultipleConfigModuleTest()
    : BaseT()
{
}

MultipleConfigModuleTest::~MultipleConfigModuleTest()
{
}

void MultipleConfigModuleTest::SetUp()
{
}

void MultipleConfigModuleTest::TearDown()
{
}

class TestConfigModule : public ConfigModule
{
    public:
        TestConfigModule(std::string const& tag="test")
            : ConfigModule(tag)
        {
        }

    protected:
        void add_options(OptionsDescriptionEasyInit&) override
        {
        }
};

class TestConfigModuleA : public TestConfigModule
{
    public:
        TestConfigModuleA()
           : TestConfigModule("A")
        {
        }
};

class TestConfigModuleB : public TestConfigModule
{
    public:
        TestConfigModuleB()
           : TestConfigModule("B")
        {
        }
};

class TestConfigModuleC : public TestConfigModule
{
    public:
        TestConfigModuleC()
           : TestConfigModule("C")
        {
        }
};

TEST_F(MultipleConfigModuleTest, test_no_types_specified)
{
    // Use case: default constructor
    MultipleConfigModule<TestConfigModule> config;
    ASSERT_EQ(config.name(), std::string("test"));

    // Use case: tag constructor - ensure tag propagates into base class
    MultipleConfigModule<TestConfigModule> config_tag("A");
    ASSERT_EQ(config_tag.name(), std::string("A"));
}

TEST_F(MultipleConfigModuleTest, test_single_type_specified)
{
    typedef MultipleConfigModule<TestConfigModule, TestConfigModuleA> Type;
    Type config("base");
    ASSERT_EQ(config.name(), std::string("base"));

    const Type& const_config = config;

    TestConfigModuleA& a = config.config<TestConfigModuleA>();
    TestConfigModuleA const& const_a = const_config.config<TestConfigModuleA>();
    ASSERT_EQ(&a, &const_a);
    ASSERT_EQ(a.name(), std::string("A"));
}

TEST_F(MultipleConfigModuleTest, test_multi_type_specified)
{
    typedef MultipleConfigModule<TestConfigModule, TestConfigModuleA, TestConfigModuleB, TestConfigModuleC> Type;
    Type config("base");
    ASSERT_EQ(config.name(), std::string("base"));

    const Type& const_config = config;

    TestConfigModuleA& a = config.config<TestConfigModuleA>();
    TestConfigModuleA const& const_a = const_config.config<TestConfigModuleA>();
    ASSERT_EQ(&a, &const_a);
    ASSERT_EQ(a.name(), std::string("A"));

    TestConfigModuleB& b = config.config<TestConfigModuleB>();
    TestConfigModuleB const& const_b = const_config.config<TestConfigModuleB>();
    ASSERT_EQ(&b, &const_b);
    ASSERT_EQ(b.name(), std::string("B"));

    TestConfigModuleC& c = config.config<TestConfigModuleC>();
    TestConfigModuleC const& const_c = const_config.config<TestConfigModuleC>();
    ASSERT_EQ(&c, &const_c);
    ASSERT_EQ(c.name(), std::string("C"));
}

TEST_F(MultipleConfigModuleTest, test_deactivate_all_no_activate_methods)
{
    typedef MultipleConfigModule<TestConfigModule, TestConfigModuleA, TestConfigModuleB, TestConfigModuleC> Type;
    Type config("base");
    ASSERT_NO_THROW(config.deactivate_all());
}

struct ConfigActiveA : public ConfigActive
{
    ConfigActiveA()
        : ConfigActive("A", "help A")
    {
        active(true);
    }
};

struct ConfigActiveB : public ConfigActive
{
    ConfigActiveB()
        : ConfigActive("B", "help B")
    {
        active(true);
    }
};

TEST_F(MultipleConfigModuleTest, test_deactivate_all_activate_methods)
{
    typedef MultipleConfigModule<TestConfigModule, TestConfigModuleA, ConfigActiveA, ConfigActiveB> Type;
    Type config("base");
    ASSERT_TRUE(config.template config<ConfigActiveA>().active());
    ASSERT_TRUE(config.template config<ConfigActiveB>().active());
    ASSERT_NO_THROW(config.deactivate_all());
    ASSERT_FALSE(config.template config<ConfigActiveA>().active());
    ASSERT_FALSE(config.template config<ConfigActiveB>().active());
}

} // namespace test
} // namespace panda
} // namespace ska
