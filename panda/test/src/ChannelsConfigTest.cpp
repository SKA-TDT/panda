/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ChannelsConfigTest.h"
#include "panda/ChannelsConfig.h"


namespace ska {
namespace panda {
namespace test {


ChannelsConfigTest::ChannelsConfigTest()
    : ::testing::Test()
{
}

ChannelsConfigTest::~ChannelsConfigTest()
{
}

void ChannelsConfigTest::SetUp()
{
}

void ChannelsConfigTest::TearDown()
{
}

TEST_F(ChannelsConfigTest, test_config)
{
    boost::property_tree::ptree pt_channels;
    boost::property_tree::ptree pt_exporter_1;
    boost::property_tree::ptree pt_exporter_2_a;
    boost::property_tree::ptree pt_exporter_2_b;

    pt_channels.put_child("exporter_1", pt_exporter_1);
    pt_channels.add_child("exporter_2", pt_exporter_2_a);
    pt_channels.add_child("exporter_2", pt_exporter_2_b);

    panda::ChannelsConfig c;
    boost::program_options::variables_map vm;
    c.parse_property_tree(pt_channels, vm);
    boost::program_options::notify(vm);

    ASSERT_EQ(3U, c.channels().size());
}


} // namespace test
} // namespace panda
} // namespace ska
