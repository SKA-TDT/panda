/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "EndpointConfigTest.h"
#include "panda/EndpointConfig.h"
#include "panda/Log.h"


namespace ska {
namespace panda {
namespace test {


EndpointConfigTest::EndpointConfigTest()
    : ::testing::Test()
{
}

EndpointConfigTest::~EndpointConfigTest()
{
}

void EndpointConfigTest::SetUp()
{
}

void EndpointConfigTest::TearDown()
{
}

TEST_F(EndpointConfigTest, test_init)
{
    IpAddress address(1234,"1.1.1.1");

    EndpointConfig config1,config2;

    config1.address(IpAddress(2345,"2.2.2.2"));
    config2.address(address);

    ASSERT_TRUE(config1.address()!=config2.address());

    config1.address(address);

    ASSERT_TRUE(config1.address()==config2.address());
}

} // namespace test
} // namespace panda
} // namespace ska
