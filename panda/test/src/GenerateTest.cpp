/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "GenerateTest.h"
#include "panda/test/TestArch.h"
#include "panda/Generate.h"
#include <vector>


namespace ska {
namespace panda {
namespace test {


GenerateTest::GenerateTest()
    : BaseT()
{
}

GenerateTest::~GenerateTest()
{
}

TEST_F(GenerateTest, test_cpu)
{
    std::vector<unsigned> vec(10, 0);
    unsigned count=0;
    panda::generate(vec.begin(), vec.end(), [&](){ return count++; });
    ASSERT_EQ(vec.size(), 10);
    for(std::size_t i=0; i< vec.size(); ++i) {
        ASSERT_EQ(vec[i], (std::size_t)i);
    }
}

TEST_F(GenerateTest, test_default_impl)
{
    // setup an initialised vector on the device
    std::vector<unsigned> vec(10, 0);
    Device<TestArch> device(1);
    DeviceMemory<TestArch, int> dev_vec(10, device);
    panda::copy(vec.begin(), vec.end(), dev_vec.begin());

    // perform generate
    // We do not generate to the end so we can test for overwrites
    unsigned count=0;
    panda::generate(dev_vec.begin(), dev_vec.end() -1
                   , [&](){ return count++; });

    // return the result to host so we can verify them
    panda::copy(dev_vec.begin(), dev_vec.end(), vec.begin());

    for(unsigned i=0; i<vec.size()-1; ++i)
    {
        ASSERT_EQ(vec[i], i);
    }

    // ensure no overflow has occured
    ASSERT_EQ(vec.back(), 0);
}


} // namespace test
} // namespace panda
} // namespace ska
