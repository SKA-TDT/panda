/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ConfigActiveTest.h"
#include "panda/ConfigActive.h"


namespace ska {
namespace panda {
namespace test {


ConfigActiveTest::ConfigActiveTest()
    : BaseT()
{
}

ConfigActiveTest::~ConfigActiveTest()
{
}

void ConfigActiveTest::SetUp()
{
}

void ConfigActiveTest::TearDown()
{
}

TEST_F(ConfigActiveTest, test_name)
{
    ConfigActive config("test", "help msg");
    ASSERT_EQ(config.name(), "test");
}

TEST_F(ConfigActiveTest, test_active)
{
    ConfigActive config("test", "help msg");
    ASSERT_FALSE(config.active()) << "Expected default value to be false";

    config.active(true);
    ASSERT_TRUE(config.active());

    config.active(false);
    ASSERT_FALSE(config.active());
}

TEST_F(ConfigActiveTest, test_options_parse)
{
    char cmd[] = "panda_test";
    char config_option[] = "--test.active";
    char config_val[] = "true";
    char* argv[3] = { cmd, config_option, config_val };

    ConfigActive config("test", "help msg");
    boost::program_options::options_description desc = config.command_line_options();
    ASSERT_EQ(1U, desc.options().size());

    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::command_line_parser(3, argv)
                                  .options(desc).run(), vm);
    boost::program_options::notify(vm);

    ASSERT_TRUE(config.active());
}

} // namespace test
} // namespace panda
} // namespace ska
