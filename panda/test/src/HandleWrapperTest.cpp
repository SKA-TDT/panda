/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "HandleWrapperTest.h"
#include "panda/detail/HandleWrapper.h"
#include "panda/test/TestChunkHandler.h"
#include "panda/test/TestChunk.h"


namespace ska {
namespace panda {
namespace test {


HandleWrapperTest::HandleWrapperTest()
    : ::testing::Test()
{
}

HandleWrapperTest::~HandleWrapperTest()
{
}

void HandleWrapperTest::SetUp()
{
}

void HandleWrapperTest::TearDown()
{
}

TEST_F(HandleWrapperTest, test_chunk_handler_state_maintenance)
{
    TestChunkHandler<TestChunk_A> handler;
    panda::detail::HandleWrapper<TestChunk_A&> wrapper(handler);
    TestChunk_A chunk;
    wrapper(chunk);
    ASSERT_TRUE(handler.executed());
}

} // namespace test
} // namespace panda
} // namespace ska
