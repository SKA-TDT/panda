/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ProcessingEngineTest.h"
#include "panda/test/TestHandler.h"
#include "panda/ProcessingEngineConfig.h"
#include "panda/ProcessingEngine.h"

namespace ska {
namespace panda {
namespace test {

ProcessingEngineTest::ProcessingEngineTest()
    : ::testing::Test()
{
}

ProcessingEngineTest::~ProcessingEngineTest()
{
}

void ProcessingEngineTest::SetUp()
{
}

void ProcessingEngineTest::TearDown()
{
}

TEST_F(ProcessingEngineTest, test_sync_operation)
{
    /**
     * @test when no specific threads are assigned the call should act syncronously
     */
    ProcessingEngineConfig config;
    config.number_of_threads(0);

    ProcessingEngine engine(config);
    TestHandler handler(false); // non-blocking handler

    engine.post(handler);
    handler.wait();

    ASSERT_EQ(std::this_thread::get_id(), handler.caller_thread_id()); // only expect one thread
}

TEST_F(ProcessingEngineTest, test_single_thread_operation)
{
    /**
     * @test a single dedicated thread is assigned
     */
    ProcessingEngineConfig config;
    config.number_of_threads(1);

    ProcessingEngine engine(config);
    TestHandler handler_1;
    TestHandler handler_2;

    engine.post(handler_1);
    engine.post(handler_2);
    handler_1.wait();
    handler_2.wait();

    ASSERT_NE(std::this_thread::get_id(), handler_1.caller_thread_id());
    ASSERT_EQ(handler_1.caller_thread_id(), handler_2.caller_thread_id());
}

TEST_F(ProcessingEngineTest, test_multi_thread_operation)
{
    /**
     * @test mutliple dedicated threads are assigned
     */
    ProcessingEngineConfig config;
    config.number_of_threads(2);

    ProcessingEngine engine(config);
    TestHandler handler_1;
    TestHandler handler_2;

    engine.post(handler_1);
    engine.post(handler_2);
    handler_1.wait();
    handler_2.wait();

    ASSERT_NE(std::this_thread::get_id(), handler_1.caller_thread_id());
    ASSERT_NE(std::this_thread::get_id(), handler_2.caller_thread_id());
    //ASSERT_NE(handler_1.caller_thread_id(), handler_2.caller_thread_id()); // TestHandler sleeps the thread, but we need to keep thread busy for this test to work consistently
}

TEST_F(ProcessingEngineTest, test_cpu_affinity)
{
    /**
     * @test mutliple threads are bound to specified cores
     */
    ProcessingEngineConfig config;

    // Set requested core ID's
    std::vector<unsigned> affinities = {0,1};
    config.affinities(affinities);

    ProcessingEngine engine(config);
    TestHandler handler_1;
    TestHandler handler_2;

    engine.post(handler_1);
    engine.post(handler_2);
    handler_1.wait();
    handler_2.wait();

    // assert that all threads are bound to the cores specified in vector 'affinities'
    for(std::size_t i=0;i<engine.number_of_threads();i++)
    {
        SCOPED_TRACE(i);
        bool check_affinity = engine.threads()[i]->has_core_affinity(affinities);
#if HAS_PTHREAD_SET_AFFINITY
        ASSERT_TRUE(check_affinity);
#else // HAS_PTHREAD_SET_AFFINITY
        ASSERT_FALSE(check_affinity);
#endif // HAS_PTHREAD_SET_AFFINITY
    }
}

} // namespace test
} // namespace panda
} // namespace ska
