/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "SystemTest.h"
#include "panda/System.h"
#include "panda/test/TestResource.h"
#include "panda/ProcessingEngineConfig.h"


namespace ska {
namespace panda {

struct ArchA {};
struct ArchB {};
struct ArchC {};

template<>
struct PoolResource<ArchA> : public test::TestResource {
    ~PoolResource() {}
};

template<>
struct PoolResource<ArchB> : public test::TestResource {
    ~PoolResource() {}
};

template<>
struct DiscoverResources<ArchA>
{
    inline std::vector<std::shared_ptr<PoolResource<ArchA>>> operator()() { return { std::make_shared<PoolResource<ArchA>>(), std::make_shared<PoolResource<ArchA>>()}; }
};

template<>
struct DiscoverResources<ArchB>
{
    inline std::vector<std::shared_ptr<PoolResource<ArchB>>> operator()() { return { std::make_shared<PoolResource<ArchB>>(), std::make_shared<PoolResource<ArchB>>()}; }
};

template<>
struct DiscoverResources<ArchC>
{
    // none available
    inline std::vector<std::shared_ptr<PoolResource<ArchC>>> operator()() { return {}; }
};

namespace test {

SystemTest::SystemTest()
    : ::testing::Test()
    , _engine(ProcessingEngineConfig(1))
{
}

SystemTest::~SystemTest()
{
}

void SystemTest::SetUp()
{
}

void SystemTest::TearDown()
{
}

TEST_F(SystemTest, test_provision_pool)
{
    panda::System<ArchA, ArchB, ArchC> s;
    {
        panda::ResourcePool<1, ArchA, ArchB, ArchC> m1(_engine);
        ASSERT_EQ(2U, s.count<ArchA>());
        s.provision<ArchA>(m1, 2);
        ASSERT_EQ(0U, s.count<ArchA>());
        ASSERT_EQ(2U, s.count<ArchB>());
        s.provision<ArchB>(m1, 1);

        panda::ResourcePool<3, ArchA> m2(_engine);
        ASSERT_THROW(s.provision<ArchA>(m2, 1), panda::Error);

        // case no resources at all found
        panda::ResourcePool<5, ArchC> m3(_engine);
        ASSERT_THROW(s.provision<ArchC>(m3,1), panda::Error);
        //ASSERT_NO_THROW(s.provision<ArchB>(m3, 1)); // should not compile
        ASSERT_EQ(2U, s.system_count<ArchA>());
        ASSERT_EQ(2U, s.system_count<ArchB>());
    }
    // Pool now out of scope - resources should be returned to system
    ASSERT_EQ(2U, s.count<ArchA>());
    ASSERT_EQ(2U, s.count<ArchB>());
    ASSERT_EQ(0U, s.count<ArchC>());

}

} // namespace test
} // namespace panda
} // namespace ska
