/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "PropertyTreeUtilsTest.h"
#include "panda/PropertyTreeUtils.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#include <boost/property_tree/xml_parser.hpp>
#include <boost/range/adaptor/filtered.hpp>
#pragma GCC diagnostic pop

namespace ska {
namespace panda {
namespace test {

static
void print_tree(boost::property_tree::ptree const& tree) 
{
    std::stringstream ss;
    boost::property_tree::xml_parser::write_xml(ss, tree);
    std::cout << ss.str() << std::endl;
}


PropertyTreeUtilsTest::PropertyTreeUtilsTest()
    : ::testing::Test()
    , _str_val_1("string1")
{
    _tree.put<int>(boost::property_tree::ptree::path_type("ints.int_1"), _int_val_1);
    _tree.put<int>(boost::property_tree::ptree::path_type("ints.int_2"), _int_val_2);
    _tree.put<int>(boost::property_tree::ptree::path_type("ints.layered.int_1"), _int_val_3);
    _tree.put<int>(boost::property_tree::ptree::path_type("ints_list"), _int_val_1);
    _tree.add<int>(boost::property_tree::ptree::path_type("ints_list"), _int_val_2);
    _tree.put<std::string>(boost::property_tree::ptree::path_type("strings"), _str_val_1);
}

PropertyTreeUtilsTest::~PropertyTreeUtilsTest()
{
}

void PropertyTreeUtilsTest::SetUp()
{
}

void PropertyTreeUtilsTest::TearDown()
{
}

TEST_F(PropertyTreeUtilsTest, test_parse_property_tree_empty)
{
    boost::property_tree::ptree empty_tree;
    boost::program_options::options_description desc; // empty description
    ASSERT_NO_THROW(panda::parse_property_tree<char>(empty_tree, desc));

    desc.add_options()
        ("output", boost::program_options::value<unsigned>()->default_value(0u));
    ASSERT_NO_THROW(panda::parse_property_tree<char>(empty_tree, desc));
}

TEST_F(PropertyTreeUtilsTest, test_parse_property_tree)
{
    print_tree(_tree);
    boost::program_options::options_description desc; // empty description
    desc.add_options()
        ("ints.int_1", boost::program_options::value<int>()->default_value(0))
        ("ints.int_2", boost::program_options::value<int>()->default_value(0))
        ("ints.layered.int_1", boost::program_options::value<int>()->default_value(0))
        ("ints_list", boost::program_options::value<std::vector<int>>())
        ("not_specified", boost::program_options::value<int>());
    auto opts=panda::parse_property_tree<char>(_tree, desc);
    {
        auto results = boost::adaptors::filter(opts.options, [](boost::program_options::option const&v) { return v.string_key == "ints.int_1"; });
        auto it = results.begin();
        ASSERT_FALSE( it == results.end() );
        ASSERT_EQ( std::to_string(_int_val_1), it->value[0] );
        ASSERT_TRUE( ++it == results.end() ) << it->string_key;
    }
    {
        auto results = boost::adaptors::filter(opts.options, [](boost::program_options::option const&v) { return v.string_key == "ints.int_2"; });
        auto it = results.begin();
        ASSERT_FALSE( it == results.end() );
        ASSERT_EQ( std::to_string(_int_val_2), it->value[0] );
        ASSERT_TRUE( ++it == results.end() ) << it->string_key;
    }
    {
        auto results = boost::adaptors::filter(opts.options, [](boost::program_options::option const&v) { return v.string_key == "ints.layered.int_1"; });
        auto it = results.begin();
        ASSERT_FALSE( it == results.end() );
        ASSERT_EQ( std::to_string(_int_val_3), it->value[0] );
        ASSERT_TRUE( ++it == results.end() ) << it->string_key;
    }
    {
        auto results = boost::adaptors::filter(opts.options, [](boost::program_options::option const&v) { return v.string_key == "ints_list"; });
        auto it = results.begin();
        ASSERT_FALSE( it == results.end() );
        ASSERT_EQ( std::to_string(_int_val_1), it->value[0] );
        ASSERT_FALSE( ++it == results.end() );
        ASSERT_EQ( std::to_string(_int_val_2), it->value[0] );
        ASSERT_TRUE( ++it == results.end() );
    }
}

} // namespace test
} // namespace panda
} // namespace ska
