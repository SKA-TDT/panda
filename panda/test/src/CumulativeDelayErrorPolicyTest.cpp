/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "CumulativeDelayErrorPolicyTest.h"
#include "panda/CumulativeDelayErrorPolicy.h"
#include "panda/Error.h"
#include <thread>

namespace ska {
namespace panda {
namespace test {


CumulativeDelayErrorPolicyTest::CumulativeDelayErrorPolicyTest()
    : ::testing::Test()
{
}

CumulativeDelayErrorPolicyTest::~CumulativeDelayErrorPolicyTest()
{
}

void CumulativeDelayErrorPolicyTest::SetUp()
{
}

void CumulativeDelayErrorPolicyTest::TearDown()
{
}

TEST_F(CumulativeDelayErrorPolicyTest, test_connection_error)
{
    auto delay = std::chrono::seconds(1);
    CumulativeDelayErrorPolicy::IntervalType test_delays { std::chrono::seconds(0), delay, std::chrono::seconds(2 * delay) };
    CumulativeDelayErrorPolicy policy( test_delays );
    ASSERT_TRUE(policy.try_connect());
    Error e;
    ASSERT_TRUE(policy.connection_error(e));
    ASSERT_TRUE(policy.try_connect());
    ASSERT_FALSE(policy.connection_error(e));
    ASSERT_FALSE(policy.try_connect());
    std::this_thread::sleep_for(delay);
    ASSERT_TRUE(policy.try_connect());

    // check the delay sequence is reset after a connected call
    policy.connected();
    ASSERT_TRUE(policy.try_connect());
    ASSERT_TRUE(policy.connection_error(e));
    ASSERT_TRUE(policy.try_connect());
    ASSERT_FALSE(policy.connection_error(e));
    ASSERT_FALSE(policy.try_connect());
}

TEST_F(CumulativeDelayErrorPolicyTest, test_error)
{
    auto delay = std::chrono::seconds(1);
    CumulativeDelayErrorPolicy::IntervalType test_delays { std::chrono::seconds(0), delay, std::chrono::seconds(2 * delay) };
    CumulativeDelayErrorPolicy policy( test_delays );
    ASSERT_TRUE(policy.try_connect());
    Error e;
    ASSERT_TRUE(policy.error(e));
    ASSERT_TRUE(policy.try_connect());
    ASSERT_FALSE(policy.error(e));
    ASSERT_FALSE(policy.try_connect());
    std::this_thread::sleep_for(delay);
    ASSERT_TRUE(policy.try_connect());

    // check the delay sequence is reset after a connected call
    policy.connected();
    ASSERT_TRUE(policy.try_connect());
    ASSERT_TRUE(policy.error(e));
    ASSERT_TRUE(policy.try_connect());
    ASSERT_FALSE(policy.error(e));
    ASSERT_FALSE(policy.try_connect());
}

} // namespace test
} // namespace panda
} // namespace ska
