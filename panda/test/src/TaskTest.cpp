/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "TaskTest.h"
#include "panda/detail/Task.h"
#include "panda/TupleUtilities.h"
#include <array>


namespace ska {
namespace panda {
namespace test {


TaskTest::TaskTest()
    : ::testing::Test()
{
}

TaskTest::~TaskTest()
{
}

void TaskTest::SetUp()
{
}

void TaskTest::TearDown()
{
}


// Traits class that does not expect data
template<typename... Archs >
struct TestTaskTraits {
    public:
        typedef std::tuple<Archs...> Architectures;

        TestTaskTraits() { std::fill(_called.begin(), _called.end(), false); }

        template<typename Arch>
        void operator()(PoolResource<Arch>&) {
            _called[panda::Index<Arch, Architectures>::value] = true;
        }

        template<typename Arch>
        bool called() const {
            return _called[panda::Index<Arch, Architectures>::value];
        }

    private:
        std::array<bool, std::tuple_size<Architectures>::value> _called;
};

TEST_F(TaskTest, without_data)
{
    TestTaskTraits<ArchA, ArchB> traits;
    panda::detail::Task<TestTaskTraits<ArchA, ArchB>> task(traits);
    task(resource_a());
    ASSERT_TRUE(traits.called<ArchA>());
    ASSERT_FALSE(traits.called<ArchB>());
    task(resource_b());
    ASSERT_TRUE(traits.called<ArchB>());
}

// Traits class that records the data that has been sent
template<typename... DataTypes>
struct TestTraitsData : public TestTaskTraits<ArchA, ArchB> {

    template<typename Arch>
    void operator()(PoolResource<Arch>&, DataTypes&&... data) {
        _data[panda::Index<Arch, Architectures>::value] = std::make_tuple(std::forward<DataTypes>(data)...);
    }

    template<typename Arch>
    std::tuple<DataTypes...> const& data() const
    {
        return _data[panda::Index<Arch, Architectures>::value];
    }

    private:
        std::array<std::tuple<DataTypes...>,2> _data;
};

TEST_F(TaskTest, data_propagation)
{
    int a=99;
    float b=978.8;
    char c='x';
    typedef TestTraitsData<int, float, char> TraitsType;
    TraitsType traits;

    panda::detail::Task<TraitsType, int&, float&, char&> task(traits, a, b , c);

    // check call on task is propagated to the traits class, with the data intact
    task(resource_a());
    ASSERT_EQ(traits.data<ArchA>(), std::make_tuple(a,b,c));
    ASSERT_NE(traits.data<ArchB>(), std::make_tuple(a,b,c));

    task(resource_b());
    ASSERT_EQ(traits.data<ArchB>(), std::make_tuple(a,b,c));
}

TEST_F(TaskTest, is_compatible_with_compatible_resources)
{
    TestTaskTraits<ArchA, ArchB> traits;
    panda::detail::Task<TestTaskTraits<ArchA, ArchB>> task(traits);
    ASSERT_TRUE(( task.is_compatible<ArchA, ArchA>(resource_a()) ));
    ASSERT_TRUE(( task.is_compatible<ArchB, ArchB>(resource_b()) ));
}

TEST_F(TaskTest, is_compatible_with_derived_architectures)
{
    TestTaskTraits<ArchADerived, ArchBDerived> traits;
    panda::detail::Task<TestTaskTraits<ArchADerived, ArchBDerived>> task(traits);
    ASSERT_FALSE(( task.is_compatible<ArchADerived, ArchA>(resource_a()) ));
    ASSERT_FALSE(( task.is_compatible<ArchBDerived, ArchB>(resource_b()) ));
}

TEST_F(TaskTest, is_compatible_with_incompatible_resource)
{
    TestTaskTraits<ArchA> traits;
    panda::detail::Task<TestTaskTraits<ArchA>> task(traits);
    ASSERT_FALSE(( task.is_compatible<ArchA, ArchB>(resource_b()) ));
}

} // namespace test
} // namespace panda
} // namespace ska
