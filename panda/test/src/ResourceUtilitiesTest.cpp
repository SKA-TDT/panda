/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ResourceUtilitiesTest.h"
#include "panda/ResourceUtilities.h"


namespace ska {
namespace panda {

template<>
class PoolResource<test::ArchAVersioned> : public Resource
{
    public:
        PoolResource(int version) : _version(version) {}
        ~PoolResource() {}

        template<typename TaskType>
        void run(TaskType&) {}
        void abort() {}
        int version() const { return _version; }

    private:
        int _version;
};

namespace test {

/// specialised operator for comparing resources and capabilities
template<typename Capability>
bool operator>=(PoolResource<ArchAVersioned> const & r, Capability const & c) {
    return r.version() >= c.version();
}

template<typename Capability>
bool operator<=(PoolResource<ArchAVersioned> const & r, Capability const & c) {
     return r.version() <= c.version();
}

ResourceUtilitiesTest::ResourceUtilitiesTest()
    : ::testing::Test()
{
}

ResourceUtilitiesTest::~ResourceUtilitiesTest()
{
}

void ResourceUtilitiesTest::SetUp()
{
}

void ResourceUtilitiesTest::TearDown()
{
}

TEST_F(ResourceUtilitiesTest, test_minimum)
{
    PoolResource<ArchAVersioned> resource_vers2(2), resource_vers3(3);
    ASSERT_TRUE(( ResourceSupports<MinimumVersion<ArchCapability<3>>, ArchAVersioned>::compatible(resource_vers3) ));
    ASSERT_TRUE(( ResourceSupports<MinimumVersion<ArchCapability<2>>, ArchAVersioned>::compatible(resource_vers3) ));
    ASSERT_FALSE(( ResourceSupports<MinimumVersion<ArchCapability<4>>, ArchAVersioned>::compatible(resource_vers3) ));
    ASSERT_FALSE(( ResourceSupports<MinimumVersion<ArchCapability<3>>, ArchAVersioned>::compatible(resource_vers2) ));
}

TEST_F(ResourceUtilitiesTest, test_maximum)
{
    PoolResource<ArchAVersioned> resource_vers3(3), resource_vers4(4);
    ASSERT_TRUE(( ResourceSupports<MaximumVersion<ArchCapability<3>>, ArchAVersioned>::compatible(resource_vers3) ));
    ASSERT_TRUE(( ResourceSupports<MaximumVersion<ArchCapability<4>>, ArchAVersioned>::compatible(resource_vers3) ));
    ASSERT_FALSE(( ResourceSupports<MaximumVersion<ArchCapability<2>>, ArchAVersioned>::compatible(resource_vers3) ));
    ASSERT_FALSE(( ResourceSupports<MaximumVersion<ArchCapability<3>>, ArchAVersioned>::compatible(resource_vers4) ));
}

TEST_F(ResourceUtilitiesTest, test_range)
{
    PoolResource<ArchAVersioned> resource_vers2(2), resource_vers3(3), resource_vers5(5), resource_vers6(6);
    ASSERT_TRUE(( ResourceSupports<Range<ArchCapability<3>, ArchCapability<5>>, ArchAVersioned>::compatible(resource_vers3) ));
    ASSERT_TRUE(( ResourceSupports<Range<ArchCapability<3>, ArchCapability<5>>, ArchAVersioned>::compatible(resource_vers5) ));
    ASSERT_FALSE(( ResourceSupports<Range<ArchCapability<3>, ArchCapability<5>>, ArchAVersioned>::compatible(resource_vers2) ));
    ASSERT_FALSE(( ResourceSupports<Range<ArchCapability<3>, ArchCapability<5>>, ArchAVersioned>::compatible(resource_vers6) ));
}

TEST_F(ResourceUtilitiesTest, test_version)
{
    PoolResource<ArchAVersioned> resource_vers2(2), resource_vers3(3), resource_vers5(5), resource_vers6(6), resource_vers7(7), resource_vers9(9);
    ASSERT_TRUE(( ResourceSupports<Version<Range<ArchCapability<3>, ArchCapability<5>>, Range<ArchCapability<7>, ArchCapability<8>>>, ArchAVersioned>::compatible(resource_vers3) ));
    ASSERT_TRUE(( ResourceSupports<Version<Range<ArchCapability<3>, ArchCapability<5>>, Range<ArchCapability<7>, ArchCapability<8>>>, ArchAVersioned>::compatible(resource_vers7) ));
    ASSERT_TRUE(( ResourceSupports<Version<Range<ArchCapability<3>, ArchCapability<5>>, MaximumVersion<ArchCapability<8>>>, ArchAVersioned>::compatible(resource_vers7) ));

    ASSERT_FALSE(( ResourceSupports<Version<Range<ArchCapability<3>, ArchCapability<5>>, Range<ArchCapability<7>, ArchCapability<8>>>, ArchAVersioned>::compatible(resource_vers2) ));
    ASSERT_FALSE(( ResourceSupports<Version<Range<ArchCapability<3>, ArchCapability<5>>, Range<ArchCapability<7>, ArchCapability<8>>>, ArchAVersioned>::compatible(resource_vers6) ));
    ASSERT_FALSE(( ResourceSupports<Version<Range<ArchCapability<3>, ArchCapability<5>>, MaximumVersion<ArchCapability<8>>>, ArchAVersioned>::compatible(resource_vers9) ));
}

} // namespace test
} // namespace panda
} // namespace ska
