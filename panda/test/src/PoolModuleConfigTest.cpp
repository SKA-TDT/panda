/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "PoolModuleConfigTest.h"
#include "panda/PoolModuleConfig.h"
#include "panda/Config.h"


namespace ska {
namespace panda {
namespace test {


PoolModuleConfigTest::PoolModuleConfigTest()
    : ::testing::Test()
{
}

PoolModuleConfigTest::~PoolModuleConfigTest()
{
}

void PoolModuleConfigTest::SetUp()
{
}

void PoolModuleConfigTest::TearDown()
{
}

class ConfigA : public ConfigModule
{
    public:
        ConfigA() : ConfigModule("ConfigA") {}
        void add_options(OptionsDescriptionEasyInit&) override {}
};

class ConfigB : public ConfigModule
{
    public:
        ConfigB() : ConfigModule("ConfigB") {}
        void add_options(OptionsDescriptionEasyInit&) override {}
};

class TestConfig : public panda::Config<Cpu>
{
        typedef panda::Config<Cpu> BaseT;

    public:
        TestConfig() : BaseT("test", "a test") {}
        ConfigModule& get_subsection(std::string const& s) { return BaseT::get_subsection(s); }
};

class TestConfigModule : public panda::ConfigModule
{
    typedef panda::ConfigModule BaseT;

    public:
        TestConfigModule() : BaseT("test") {}
        ConfigModule& get_subsection(std::string const& s) { return BaseT::get_subsection(s); }
        void add_options(OptionsDescriptionEasyInit&) override {}
};

TEST_F(PoolModuleConfigTest, test_setup_config_module)
{
    System<Cpu> system;
    PoolManagerConfig<decltype(system)> cfg;
    PoolManager<System<Cpu>> pm(system, cfg); 
    TestConfigModule cm;
    PoolModuleConfig<typename TestConfig::PoolManagerType, ConfigA, ConfigB> modules(pm, cm);
    ASSERT_NO_THROW(cm.get_subsection("ConfigA"));
    ASSERT_NO_THROW(cm.get_subsection("ConfigB"));

    // verify we can get a config
    modules.config<ConfigA>();
    modules.config<ConfigB>();

}


TEST_F(PoolModuleConfigTest, test_setup_from_Config)
{
    TestConfig cm;
    PoolModuleConfig<typename TestConfig::PoolManagerType, ConfigA, ConfigB> modules(cm);
    ASSERT_NO_THROW(cm.get_subsection("ConfigA"));
    ASSERT_NO_THROW(cm.get_subsection("ConfigB"));
}


} // namespace test
} // namespace panda
} // namespace ska
