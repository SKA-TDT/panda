/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "PoolManagerConfigTest.h"
#include "panda/PoolManagerConfig.h"
#include "panda/test/TestSystem.h"


namespace ska {
namespace panda {
namespace test {


PoolManagerConfigTest::PoolManagerConfigTest()
    : ::testing::Test()
{
}

PoolManagerConfigTest::~PoolManagerConfigTest()
{
}

void PoolManagerConfigTest::SetUp()
{
}

void PoolManagerConfigTest::TearDown()
{
}

TEST_F(PoolManagerConfigTest, test_default_reservation_count)
{
    std::string pool_id("pool_id");

    boost::property_tree::ptree pool_manager_config_tree;
    boost::property_tree::ptree pool_config_a;
    boost::property_tree::ptree device_config_a;
    pool_config_a.put_child("ArchA", device_config_a);
    pool_config_a.put<std::string>("id", pool_id);
    pool_manager_config_tree.put_child("pool", pool_config_a);

    boost::property_tree::ptree pool_config_b;
    boost::property_tree::ptree device_config_b;
    pool_config_b.put_child("ArchA", device_config_b);

    {
        TestSystem system;
        panda::PoolManagerConfig<TestSystem> config;

        // pool id not yet known
        config.reserve(pool_id);
        ASSERT_EQ(0U, config.reserved_count<ArchA>());
    }
    {
        TestSystem system;
        panda::PoolManagerConfig<TestSystem> config;
        boost::program_options::variables_map vm;
        config.parse_property_tree(pool_manager_config_tree, vm);

        boost::program_options::notify(vm);

        // pool id not yet known
        config.reserve(pool_id);
        ASSERT_EQ(1U, config.reserved_count<ArchA>());
    }
    // add another pool
    pool_manager_config_tree.add_child("pool", pool_config_b);
    {
        TestSystem system;
        panda::PoolManagerConfig<TestSystem> config;
        boost::program_options::variables_map vm;
        config.parse_property_tree(pool_manager_config_tree, vm);

        boost::program_options::notify(vm);

        // pool id not yet known
        for(auto const& id : config.pool_ids() ) {
            config.reserve(id);
        }
        ASSERT_EQ(2U, config.reserved_count<ArchA>());
    }
}


} // namespace test
} // namespace panda
} // namespace ska
