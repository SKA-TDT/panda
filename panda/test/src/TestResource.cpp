/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/TestResource.h"

namespace ska {
namespace panda {
namespace test {


TestResource::TestResource(std::size_t memory)
    : _current(nullptr), _object(nullptr), _do_throw(false)
    , _memory(memory)
{
}

TestResource::~TestResource()
{
    complete_job();
}

void TestResource::abort()
{
    complete_job();
}

void TestResource::complete_job() {
    {
        std::unique_lock<std::mutex> loc(_mutex);
        if( _current ) {
           _current = nullptr;
           _wait_condition.notify_all();
        }
    }
    // this lock will prevent us from returning until the run() has completed as well
    std::unique_lock<std::mutex> lock(_mutex);
}

void* TestResource::current_job() const {
    return _current;
}

} // namespace test
} // namespace panda
} // namespace ska
