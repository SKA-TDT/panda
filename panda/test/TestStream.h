/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TESTSTREAM_H
#define SKA_PANDA_TESTSTREAM_H

#include "panda/Engine.h"
#include "panda/Producer.h"
#include <string>
#include <deque>
#include <mutex>
#include <condition_variable>

namespace ska {
namespace panda {
namespace test {

/**
 * \ingroup testutil
 * @{
 */

/**
 * @brief
 *    A simple pass through streamer
 * @details
 * 
 */

template<typename DerivedType, typename... DataTypes>
class TestStreamTemplate : public Producer<DerivedType, DataTypes...>
{
    public:
        typedef TestStreamTemplate<DerivedType, DataTypes...> SelfType;
        typedef std::function<void()> ReadyReadHandlerType;
        typedef std::function<void(SelfType&)> InitHandlerType;

    public:
        TestStreamTemplate(InitHandlerType const& handler);
        ~TestStreamTemplate();

        bool process();

        /**
         * @brief send the provided data
         *        also see @function operator<<
         */
        void data(DataTypes const&... data);

        /**
         * @brief blocks until data becomes available for reading and calls the handler
         */
        void wait_ready_read(ReadyReadHandlerType const& handler);

        /**
         * @brief post a handler to be called when data becomes available to read
         */
        void async_ready_read(ReadyReadHandlerType const& handler);

        typedef std::function<void(std::tuple<DataTypes...> const&)> ReadHandlerType;
        void async_read(ReadHandlerType const& handler);

        void init();
        void stop();

    private:
        template<typename T>
        struct ChunkGetter {
            inline void operator()(SelfType& s, std::tuple<DataTypes...> const& data)
            {
                auto chunk = s.template get_chunk<T>();
                *chunk = std::get<panda::Index<T,std::tuple<DataTypes...>>::value>(data);
                // chunk goes out of scope here
            }
        };

    private:
        InitHandlerType _init_handler;
        panda::Engine _engine;
        std::deque<ReadyReadHandlerType> _listeners;
        std::tuple<DataTypes...> _data;
        std::mutex _mutex;
        std::condition_variable _wait_condition;
};

template<typename... DataTypes>
class TestStream : public TestStreamTemplate<TestStream<DataTypes...>, DataTypes...>
{
        typedef TestStreamTemplate<TestStream<DataTypes...>, DataTypes...> BaseT;

    public:
        TestStream(typename BaseT::InitHandlerType const& handler) : BaseT(handler) {}
};

/**
 * @brief send a TestChunk to the stream as data to be served
 */
template<typename TestStreamType, typename ChunkType>
TestStreamType& operator<<(TestStreamType & stream, ChunkType const & type)
{
    stream.data(std::string(type.data(), type.size()));
    return stream;
}

/**
 *  @}
 *  End Document Group
 **/

} // namespace test
} // namespace panda
} // namespace ska
#include "panda/test/detail/TestStream.cpp"

#endif // SKA_PANDA_TESTSTREAM_H
