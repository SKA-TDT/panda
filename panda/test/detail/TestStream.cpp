/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/TestStream.h"


namespace ska {
namespace panda {
namespace test {


template<typename DerivedType, typename... DataTypes>
TestStreamTemplate<DerivedType, DataTypes...>::TestStreamTemplate(InitHandlerType const& handler)
    : _init_handler(handler)
{
}

template<typename DerivedType, typename... DataTypes>
TestStreamTemplate<DerivedType, DataTypes...>::~TestStreamTemplate()
{
}

template<typename DerivedType, typename... DataTypes>
void TestStreamTemplate<DerivedType, DataTypes...>::init()
{
    _init_handler(*this);
}


template<typename DerivedType, typename... DataTypes>
void TestStreamTemplate<DerivedType, DataTypes...>::stop()
{
}

template<typename DerivedType, typename... DataTypes>
void TestStreamTemplate<DerivedType, DataTypes...>::data(DataTypes const&... data)
{
    std::unique_lock<std::mutex> lock(_mutex);
    _data = std::move(std::make_tuple(data...)); // make a copy for async_read() handlers
    panda::ForEach<decltype(_data), ChunkGetter>::exec(*this, _data);
    
    // -- inform interested parties that data is available
    for(auto const& handler : _listeners) {
        _engine.post(handler);
    }
    _listeners.clear();
    _wait_condition.notify_all();

    _engine.run();
    _engine.reset();
}
    
template<typename DerivedType, typename... DataTypes>
bool TestStreamTemplate<DerivedType, DataTypes...>::process()
{
    _engine.poll_one();
    return false;
}

template<typename DerivedType, typename... DataTypes>
void TestStreamTemplate<DerivedType, DataTypes...>::wait_ready_read(TestStreamTemplate::ReadyReadHandlerType const& handler)
{
    {
        // lock context
        std::unique_lock<std::mutex> lock(_mutex);
        while(_data.size() == 0) {
            _wait_condition.wait(lock);
        }
    }
    handler();
}

template<typename DerivedType, typename... DataTypes>
void TestStreamTemplate<DerivedType, DataTypes...>::async_ready_read(TestStreamTemplate::ReadyReadHandlerType const& handler)
{
    {
        // lock context
        std::unique_lock<std::mutex> lock(_mutex);
        if(_data.size() == 0) {
            _listeners.push_back(handler);
            return;
        }
    }
    handler();
}

template<typename DerivedType, typename... DataTypes>
void TestStreamTemplate<DerivedType, DataTypes...>::async_read(TestStreamTemplate::ReadHandlerType const& handler)
{
    {
        std::unique_lock<std::mutex> lock(_mutex);
        if(_data.size())
        {
            std::string tmp(std::move(_data));
            _data.clear();
            return handler(tmp);
        }
    }

    // wait for incomming data
    async_ready_read( [this, handler]() {  async_read(handler); } );
}

} // namespace test
} // namespace panda
} // namespace ska
