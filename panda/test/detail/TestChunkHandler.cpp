/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/TestChunkHandler.h"


namespace ska {
namespace panda {
namespace test {

template<class ChunkType>
class TestChunkHandler<ChunkType>::TestChunkHandlerImpl : public TestHandler
{
    public:
        TestChunkHandlerImpl(bool blocking);
        ~TestChunkHandlerImpl();

        /**
         * @brief function to be called on execution
         */
        void operator()(ChunkType& chunk);
        void operator()(std::shared_ptr<ChunkType> const& chunk);

        /**
         * @brief return the data that was sent when handler was executed
         * @details use this interface when multiple datatypes are involved
         */
        template<typename DataType
                ,typename std::enable_if<std::is_same<DataType, ChunkType>::value
                                        , bool>::type = true
                >
        ChunkType const& data() const;

        /**
         * @brief return the chunk that was sent when handler was executed
         */
        typename std::remove_const<ChunkType>::type const& chunk() const;

        /**
         * @brief reset the handler to its pre-executed state
         */
        void reset();

    private:
        // make sure we cannot call this by mistake by making it private
        void operator()();

    protected:
        std::shared_ptr<ChunkType> _chunk; // share data between handler copies
};

template<class ChunkType, class... ChunkTypes>
TestChunkHandler<ChunkType, ChunkTypes...>::TestChunkHandler(bool blocking)
    : BaseT(blocking)
{
}

template<class ChunkType, class... ChunkTypes>
void TestChunkHandler<ChunkType, ChunkTypes...>::operator()(ChunkType& chunk)
{
    _chunk.reset(new ChunkType(chunk));
    BaseT::operator()();   // do the usual Testhandler things
}

template<class ChunkType, class... ChunkTypes>
void TestChunkHandler<ChunkType, ChunkTypes...>::operator()(std::shared_ptr<ChunkType> const& chunk)
{
    _chunk = chunk;
    BaseT::operator()();   // do the usual Testhandler things
}

template<class ChunkType, class... ChunkTypes>
void TestChunkHandler<ChunkType, ChunkTypes...>::reset()
{
    _chunk.reset();
    BaseT::reset();
}

template<class ChunkType, class... ChunkTypes>
template<typename DataType
        ,typename std::enable_if<std::is_same<DataType, ChunkType>::value
                                , bool>::type
        >
ChunkType const& TestChunkHandler<ChunkType, ChunkTypes...>::data() const
{
    if(!_chunk) throw std::runtime_error("no data recieved");
    return *_chunk;
}

template<class ChunkType>
TestChunkHandler<ChunkType>::TestChunkHandlerImpl::TestChunkHandlerImpl(bool blocking)
    : TestHandler(blocking)
{
}

template<class ChunkType>
TestChunkHandler<ChunkType>::TestChunkHandlerImpl::~TestChunkHandlerImpl()
{
}

template<class ChunkType>
void TestChunkHandler<ChunkType>::TestChunkHandlerImpl::operator()(ChunkType& chunk)
{
    _chunk.reset(new ChunkType(chunk));
    (*this)();       // do the usual Testhandler things
}

template<class ChunkType>
void TestChunkHandler<ChunkType>::TestChunkHandlerImpl::operator()(std::shared_ptr<ChunkType> const& chunk)
{
    _chunk = chunk; // store a copy of the chunk we were passed.
    (*this)();       // do the usual Testhandler things
}

template<class ChunkType>
template<typename DataType
        ,typename std::enable_if<std::is_same<DataType, ChunkType>::value
                                , bool>::type
        >
ChunkType const& TestChunkHandler<ChunkType>::TestChunkHandlerImpl::data() const
{
    if(!_chunk) throw std::runtime_error("no data recieved");
    return *_chunk;
}

template<class ChunkType>
typename std::remove_const<ChunkType>::type const& TestChunkHandler<ChunkType>::TestChunkHandlerImpl::chunk() const
{
    return *_chunk;
}

template<class ChunkType>
void TestChunkHandler<ChunkType>::TestChunkHandlerImpl::reset()
{
    TestHandler::reset();
    //*_chunk=ChunkType("not set");
    _chunk.reset();
}

template<class ChunkType>
void TestChunkHandler<ChunkType>::TestChunkHandlerImpl::operator()()
{
    TestHandler::operator()();
}

template<class ChunkType>
TestChunkHandler<ChunkType>::TestChunkHandler(bool blocking)
    : _pimpl(new TestChunkHandlerImpl(blocking))
{
}

template<class ChunkType>
TestChunkHandler<ChunkType>::~TestChunkHandler()
{
}

template<class ChunkType>
void TestChunkHandler<ChunkType>::operator()(ChunkType& chunk)
{
    (*_pimpl)(chunk);
}

template<class ChunkType>
void TestChunkHandler<ChunkType>::operator()(std::shared_ptr<ChunkType> const& chunk)
{
    (*_pimpl)(chunk);
}

template<class ChunkType>
template<typename DataType
        ,typename std::enable_if<std::is_same<DataType, ChunkType>::value
                                , bool>::type
        >
ChunkType const& TestChunkHandler<ChunkType>::data() const
{
    return _pimpl->data();
}

template<class ChunkType>
typename std::remove_const<ChunkType>::type const& TestChunkHandler<ChunkType>::chunk() const
{
    return _pimpl->chunk();
}

template<class ChunkType>
void TestChunkHandler<ChunkType>::reset()
{
    _pimpl->reset();
}

template<class ChunkType>
void TestChunkHandler<ChunkType>::operator()()
{
    (*_pimpl)();
}

template<class ChunkType>
void TestChunkHandler<ChunkType>::wait() const
{
    _pimpl->wait();
}

template<class ChunkType>
bool TestChunkHandler<ChunkType>::wait(std::chrono::seconds const& timeout) const
{
    return _pimpl->wait(timeout);
}

template<class ChunkType>
bool TestChunkHandler<ChunkType>::executed() const
{
    return _pimpl->executed();
}

} // namespace test
} // namespace panda
} // namespace ska
