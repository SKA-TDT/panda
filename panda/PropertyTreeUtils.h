/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_PROPERTYTREEUTILS_H
#define SKA_PANDA_PROPERTYTREEUTILS_H

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#pragma GCC diagnostic pop

namespace ska {
namespace panda {

/**
 * @brief program_options compatible parser for ptree 
 */
template<class charT>
boost::program_options::basic_parsed_options<charT>
parse_property_tree(boost::property_tree::ptree const&, const boost::program_options::options_description&);

} // namespace panda
} // namespace ska
#include "panda/detail/PropertyTreeUtils.cpp"

#endif // SKA_PANDA_PROPERTYTREEUTILS_H 
