/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_DISCOVERRESOURCES_H
#define SKA_PANDA_DISCOVERRESOURCES_H

#include "panda/ArchitectureConcept.h"

#include <vector>
#include <memory>

namespace ska {
namespace panda {

/**
 * @brief suitable templates to discover different architectures on the system
 * @details specialise for each architecture required
 *          implement the operator() to return suitable objects representing the devices found
 * @tparam
 *    an Architecture tag to indicate the type of resource
 */
template<typename Arch>
struct DiscoverResources
{
    BOOST_CONCEPT_ASSERT((ArchitectureConcept<Arch>));

    std::vector<std::shared_ptr<Arch>> operator()() {
        static_assert( true, "Architecture not supported" );
        return std::vector<std::shared_ptr<Arch>>(); // never used but needed to keep compiler happy
    }
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_DISCOVERRESOURCES_H 
