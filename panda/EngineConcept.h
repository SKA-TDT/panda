/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ENGINECONCEPT_H
#define SKA_PANDA_ENGINECONCEPT_H

#include "panda/Concept.h"



/**
 * @brief The EngineConcept class check the requirements on an Engine type
 *
 * @details The Engine is a thread pool manager which basically wraps an boost::asio::io_service. It must provide a poll_one() method to run a job on the queue in the current thread. It must also provide a post() method to submit jobs to the queue.
 *
 * See Engine.h and ProcessignEngine.h for examples
 *
 */

namespace ska {
namespace panda {

template <class X>
struct EngineConcept: Concept<X> {
        typedef Concept<X> Base;

    public:
        BOOST_CONCEPT_USAGE(EngineConcept) {
            X& x(Base::reference());

            /// An Engine concept class must provide a poll_one() method
            x.poll_one();

            /// An Engine concept class must provide the ability to post a handler through a post method
            x.post([](){});
        }
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_ENGINECONCEPT_H
