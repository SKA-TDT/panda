/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ALTERA_DEVICEPOINTER_H
#define SKA_PANDA_ALTERA_DEVICEPOINTER_H

#include "panda/arch/altera/DeviceIterator.h"
#include "panda/arch/altera/Architecture.h"
#include "panda/arch/altera/Fpga.h"
#include "panda/arch/altera/CommandQueue.h"

namespace ska {
namespace panda {
namespace altera {

class Fpga;

/**
 * @brief
 *     manage a Altera opencL fpga pointer
 * @details
 *
 */
template<typename DataType>
class DevicePointer
{
    public:
        typedef panda::DeviceIterator<OpenCl, DataType> Iterator;
        typedef panda::DeviceIterator<OpenCl, const DataType> ConstIterator;

    public:
        explicit DevicePointer(Fpga const& device, std::size_t size, CommandQueue& queue);
        DevicePointer(DevicePointer const&);
        DevicePointer(DevicePointer&&);
        ~DevicePointer();
        DevicePointer& operator=(DevicePointer&&);

        void* tag() const;

        /**
         * @brief return the size of the memory allocated (in DataType)
         */
        std::size_t const& size() const;

        /**
         * @brief return a (opencl device) pointer to the first element
         */
        Iterator begin();

        /**
         * @brief return a (opencl device) pointer to the last element + 1
         */
        Iterator end();

        /**
         * @brief return a (opencl device) pointer to the first element
         */
        ConstIterator begin() const;

        /**
         * @brief return a (opencl device) pointer to the last element + 1
         */
        ConstIterator end() const;

        /**
         * @brief return a (opencl device) pointer to the first element
         */
        ConstIterator cbegin() const;

        /**
         * @brief return a (opencl device) pointer to the last element + 1
         */
        ConstIterator cend() const;

        /**
         * @brief return the context of a device
         */
        Fpga const& device() const;

        /**
         * @brief write data from a contiguous buffer to the device using a default queue
         */
        cl_int enque_write(std::size_t offset, DataType const& data, std::size_t size) const;

        /**
         * @brief write data from a contiguous buffer to the device using a specific queue
         */
        cl_int enque_write(cl_command_queue queue, std::size_t offset, DataType const& data, std::size_t size) const;

        /**
         * @brief read data from a device to host memory
         */
        cl_int enque_read(std::size_t offset, DataType& data, std::size_t size) const;
        cl_int enque_read(cl_command_queue queue, std::size_t offset, DataType& data, std::size_t size) const;

        /**
         * @details convert to the opencl memory buffer object
         */
        operator cl_mem const& () const;

    private:
        std::size_t _size;
        Fpga const* _device;
        cl_mem _cl_mem;
        CommandQueue* _queue;
        void* _tag;
};

} // namespace altera

// template to determining arch types
template<typename... T>
struct ArchitectureType<altera::DevicePointer<T...>>
{
    typedef altera::OpenCl type;
};

} // namespace panda
} // namespace ska
#include "panda/arch/altera/detail/DevicePointer.cpp"

#endif // SKA_PANDA_ALTERA_DEVICEPOINTER_H
