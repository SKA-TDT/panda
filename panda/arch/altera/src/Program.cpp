/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/altera/Program.h"
#include "panda/Error.h"
#include <vector>


namespace ska {
namespace panda {
namespace altera {

Program::Program(Aocx const& aocx, Fpga const& device)
    : _device(device)
{
    //loads data of binary image (.aocx)
    const unsigned char* binary_data = reinterpret_cast<const unsigned char*>(aocx.binary_string().data());
    const std::size_t binary_size=aocx.binary_size();

    cl_int error;
    _program = clCreateProgramWithBinary(_device.context(), 1, &_device.device_id(), &binary_size, &binary_data, NULL, &error);
    if (error != CL_SUCCESS) {
        panda::Error e("failed to create altera opencl program object ");
        throw e;
    }

    error = clBuildProgram(_program, 1, &_device.device_id(), "", NULL, NULL);
    if (error != CL_SUCCESS) {
        panda::Error e("failed to build altera opencl program ");
        throw e;
    }
    clRetainProgram(_program);
}

Program::~Program()
{
    clReleaseProgram(_program);
}

cl_program const& Program::program() const
{
    return _program;
}

Fpga const& Program::device() const
{
    return _device;
}

} //namespace altera
} // namespace panda
} // namespace ska
