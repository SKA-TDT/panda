/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "DevicePointerTest.h"
#include "panda/arch/altera/Altera.h"
#include "panda/arch/altera/DevicePointer.h"
#include "panda/System.h"

namespace ska {
namespace panda {
namespace test {


DevicePointerTest::DevicePointerTest()
    : ::testing::Test()
{
}

DevicePointerTest::~DevicePointerTest()
{
}

void DevicePointerTest::SetUp()
{
}

void DevicePointerTest::TearDown()
{
}

TEST_F(DevicePointerTest, test_alloc)
{
    typedef altera::DevicePointer<int> Type;
    typedef typename ArchitectureType<Type>::type Arch;
    panda::System<Arch> system;
    ASSERT_GT(system.count<Arch>(), std::size_t(0)) << "No devices found to test";
    for(auto const& device : system.resources<Arch>())
    {
        altera::CommandQueue queue(*device);
        Type device_ptr(*device, 100, queue);
        unsigned int compare_val=100;
        ASSERT_EQ(compare_val, device_ptr.size());
    }
}

TEST_F(DevicePointerTest, test_write_read)
{
    std::vector<int> data { 0, 1, 2, 3, 4};
    typedef altera::DevicePointer<int> Type;
    typedef typename ArchitectureType<Type>::type Arch;
    panda::System<Arch> system;
    ASSERT_GT(system.count<Arch>(), std::size_t(0)) << "No devices found to test";
    for(auto const& device : system.resources<Arch>())
    {
        altera::CommandQueue queue(*device);
        Type device_ptr(*device, data.size(),queue);
        device_ptr.enque_write(0, *data.begin(), data.size());

        std::vector<int> out(data.size(), 0 );
        device_ptr.enque_read(0, *out.data(), out.size());
        ASSERT_EQ(data, out);
    }
}

TEST_F(DevicePointerTest, test_begin)
{
    std::vector<double> data { 0, 1, 2, 3, 4};
    typedef altera::DevicePointer<double> Type;
    typedef typename ArchitectureType<Type>::type Arch;
    panda::System<Arch> system;
    ASSERT_GT(system.count<Arch>(), std::size_t(0)) << "No devices found to test";
    for(auto const& device : system.resources<Arch>())
    {
        altera::CommandQueue queue(*device);
        Type device_ptr(*device, data.size(),queue);
        DeviceIterator<altera::OpenCl, double> begin = device_ptr.begin();
        // verify we can convert an iterator pointing to the start of the block to a cl_mem object
        ASSERT_NE(nullptr, static_cast<cl_mem>(begin));

        DeviceIterator<altera::OpenCl, double> const end = device_ptr.end();
        ASSERT_FALSE(begin == end);
        unsigned int count=0;
        while(begin!=end) {
            ++begin;
            ++count;
        }
        ASSERT_EQ(count, data.size());
    }
}

} // namespace test
} // namespace panda
} // namespace ska
