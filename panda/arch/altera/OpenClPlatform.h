/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_OPENCLPLATFORM_H
#define SKA_PANDA_OPENCLPLATFORM_H

#include "panda/arch/altera/OpenCl.h"
#include <vector>

namespace ska {
namespace panda {
namespace altera {

/**
 * @brief wrapper around oopencl platfrom structs and functionality
 */
class OpenClPlatform
{
    public:
        OpenClPlatform(cl_platform_id const& platform_id);
        OpenClPlatform(OpenClPlatform const&) = delete;
        ~OpenClPlatform();

        /**
         * @brief return the given name of the platform
         */
        const char* name() const;

        /**
         * @brief return the deviceIds of this platorm
         */
        std::vector<cl_device_id> device_ids() const;

    private:
        mutable std::vector<char> _platform_name;
        cl_platform_id _platform_id;
};

} // namespace altera
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_OPENCLPLATFORM_H
