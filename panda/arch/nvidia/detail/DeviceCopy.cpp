/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Version.h"
#include "panda/CompilerInfo.h"
#include "panda/arch/nvidia/DeviceCopy.h"
#include "panda/arch/nvidia/detail/ThrustUtils.h"
#include <vector>
#include <utility>
#include <iterator>
#include <type_traits>

namespace ska {
namespace panda {
namespace nvidia {

template<typename IteratorType, typename IteratorCategory>
inline
void DeviceCopy<IteratorType, IteratorCategory>::copy_to_device(IteratorType&& begin, IteratorType&& end, nvidia::CudaDevicePointer<typename IteratorType::value_type>& cuda_ptr)
{
    std::vector<typename IteratorType::value_type> local_mem;
    std::copy(std::forward<IteratorType>(begin), std::forward<IteratorType>(end), std::back_inserter(local_mem)); // we need memory to be contiguous for cudaMemcpy to be efficient
    DeviceCopy<typename std::vector<typename IteratorType::value_type>::const_iterator>::copy_to_device(local_mem.cbegin(), local_mem.cend(), cuda_ptr);
}

// specialisation for iterators with contiguous memory
template<typename IteratorType>
inline
void DeviceCopy<IteratorType, std::random_access_iterator_tag>::copy_to_device(IteratorType&& begin, IteratorType&& end, nvidia::CudaDevicePointer<typename IteratorType::value_type>& cuda_ptr)
{
    cuda_ptr.write(std::forward<IteratorType>(begin), std::forward<IteratorType>(end));
}

// generic iterator version
template<typename IteratorType, typename IteratorCategory>
inline
CudaDevicePointer<typename IteratorType::value_type> DeviceCopy<IteratorType, IteratorCategory>::copy_to_device(IteratorType&& begin, IteratorType&& end)
{
    std::vector<typename IteratorType::value_type> local_mem;
    std::copy(std::forward<IteratorType>(begin), std::forward<IteratorType>(end), std::back_inserter(local_mem)); // we need memory to be contiguous for cudaMemcpy to be efficient
    CudaDevicePointer<typename IteratorType::value_type> cuda_ptr(local_mem.size());
    cuda_ptr.write(local_mem.begin(), local_mem.end());
    return cuda_ptr;
}

// specialisation for iterators with contiguous memory
template<typename IteratorType>
CudaDevicePointer<typename IteratorType::value_type> DeviceCopy<IteratorType, std::random_access_iterator_tag>::copy_to_device(IteratorType&& begin, IteratorType&& end)
{
    auto size = end - begin;
    CudaDevicePointer<typename IteratorType::value_type> cuda_ptr(size);
    cuda_ptr.write(std::forward<IteratorType>(begin), std::forward<IteratorType>(end));
    return cuda_ptr;
}

template<typename IteratorType>
inline
CudaDevicePointer<typename IteratorType::value_type> device_copy(IteratorType&& begin, IteratorType&& end)
{
    return nvidia::DeviceCopy<IteratorType>::copy_to_device(std::forward<IteratorType>(begin)
                                                          , std::forward<IteratorType>(end)
                                                           );
}

namespace detail {

template<typename DeviceIteratorType, typename ThrustType=typename is_thrust_iterator<DeviceIteratorType>::type>
struct DeviceCopyImpl {
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    inline static typename std::remove_reference<DeviceIteratorType>::type copy_to_device(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
    {
        typedef std::iterator_traits<typename std::remove_reference<SrcIteratorType>::type> SrcIteratorTraits;
        static_assert(std::is_same<std::random_access_iterator_tag, typename SrcIteratorTraits::iterator_category>::value, "Read directly from non-contiguous iterator type not supported (use a preallocated buffer instead)");
    #ifdef SKA_PANDA_ENABLE_CUDA
        std::size_t size = std::distance(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end));
        cudaMemcpy((void*)&*dst_it,(const void*)&(*begin), size * sizeof(typename SrcIteratorTraits::value_type), cudaMemcpyHostToDevice);
        return dst_it + size;
    #else // SKA_PANDA_ENABLE_CUDA
        return dst_it;
    #endif // SKA_PANDA_ENABLE_CUDA
    }

    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    inline static typename std::remove_reference<DstIteratorType>::type copy_to_host(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
    {
        static_assert(sizeof(typename std::iterator_traits<typename std::remove_reference<DstIteratorType>::type>::value_type) >= sizeof(typename std::iterator_traits<SrcIteratorType>::value_type), "type conversions during copy from device not yet supported - please ask if you need it");
        std::size_t size = std::distance(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end));
        cudaMemcpy((void *)&*dst_it, (const void*)&(*begin), size * sizeof(typename std::iterator_traits<typename std::remove_reference<DstIteratorType>::type>::value_type), cudaMemcpyDeviceToHost);
        return dst_it + size;
    }

    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    inline static typename std::remove_reference<DstIteratorType>::type copy_within_device(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
    {
        typedef std::iterator_traits<typename std::remove_reference<SrcIteratorType>::type> SrcIteratorTraits;
        static_assert(std::is_same<std::random_access_iterator_tag, typename SrcIteratorTraits::iterator_category>::value, "Read directly from non-contiguous iterator type not supported (use a preallocated buffer instead)");
    #ifdef SKA_PANDA_ENABLE_CUDA
        std::size_t size = std::distance(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end));
        cudaMemcpy((void*)&*dst_it,(const void*)&(*begin), size * sizeof(typename SrcIteratorTraits::value_type), cudaMemcpyDeviceToDevice);
        return dst_it + size;
    #else // SKA_PANDA_ENABLE_CUDA
        return dst_it;
    #endif // SKA_PANDA_ENABLE_CUDA
    }
};

struct DeviceCopyImplThrust
{
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    inline static typename std::remove_reference<DstIteratorType>::type  copy_to_device(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
    {
        typedef typename std::remove_reference<DstIteratorType>::type ReturnItType;
        return static_cast<ReturnItType>(thrust::copy(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end), std::forward<DstIteratorType>(dst_it)));
    }

    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    inline static auto copy_to_host(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
        -> decltype(thrust::copy(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end), std::forward<DstIteratorType>(dst_it)))
    {
        return thrust::copy(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end), std::forward<DstIteratorType>(dst_it));
    }

    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    inline static typename std::remove_reference<DstIteratorType>::type copy_within_device(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
    {
        return thrust::copy(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end), std::forward<DstIteratorType>(dst_it));
    }
};

template<typename IteratorT, typename Enable=void>
struct DeviceCopyImplThrustDevicePtrWorkAround : public DeviceCopyImplThrust
{
};

// cuda thrust device_ptr code in older versions of thrust cannot handle copy calls for non-trivial types without using nvcc
// In order to work around this we define a header for this case and hide the implementation so that only the nvcc compiler can see it
// This means that the user must explcitly instantiate this class (or rather the panda::copy call) using nvcc so that it is in the library in order to be able to link

template<typename DeviceIteratorType>
struct DeviceCopyImplThrustDevicePtrWorkAround<DeviceIteratorType
                                             , EnableIfIsNotThrustCppCompat<DeviceIteratorType>
                                              >
{
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static typename std::remove_reference<DstIteratorType>::type  copy_to_device(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it);

    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static auto copy_to_host(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
        -> decltype(DeviceCopyImplThrust::copy_to_host(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end), std::forward<DstIteratorType>(dst_it)));

    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static typename std::remove_reference<DstIteratorType>::type copy_within_device(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it);
};

template<typename DeviceIteratorType>
struct DeviceCopyImpl<DeviceIteratorType, std::true_type>
    : public DeviceCopyImplThrustDevicePtrWorkAround<DeviceIteratorType>
{
};

#ifdef __THRUST_DEVICE_PTR_ON_CPP_WORKING__
template<typename IteratorT>
template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
inline
typename std::remove_reference<DstIteratorType>::type DeviceCopyImplThrustDevicePtrWorkAround<IteratorT, EnableIfIsNotThrustCppCompat<IteratorT>>::copy_to_device(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
{
    return DeviceCopyImplThrust::copy_to_device(std::forward<SrcIteratorType>(begin)
                                               ,std::forward<EndIteratorType>(end)
                                               ,std::forward<DstIteratorType>(dst_it));
}

template<typename IteratorT>
template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
inline
auto DeviceCopyImplThrustDevicePtrWorkAround<IteratorT, EnableIfIsNotThrustCppCompat<IteratorT>>::copy_to_host(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
        -> decltype(DeviceCopyImplThrust::copy_to_host(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end), std::forward<DstIteratorType>(dst_it)))
{
    return DeviceCopyImplThrust::copy_to_host(std::forward<SrcIteratorType>(begin)
                                            , std::forward<EndIteratorType>(end)
                                            , std::forward<DstIteratorType>(dst_it));
}

template<typename IteratorT>
template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
inline
typename std::remove_reference<DstIteratorType>::type DeviceCopyImplThrustDevicePtrWorkAround<IteratorT, EnableIfIsNotThrustCppCompat<IteratorT>>::copy_within_device(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
{
    return DeviceCopyImplThrust::copy_within_device(std::forward<SrcIteratorType>(begin)
                                                  , std::forward<EndIteratorType>(end)
                                                  , std::forward<DstIteratorType>(dst_it));
}
#endif // __THRUST_DEVICE_PTR_ON_CPP_WORKING__

} // detail

} // namespace nvidia

// DeviceCopy specialisations
// Cpu -> Device
template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
inline
typename std::remove_reference<DstIteratorType>::type DeviceCopy<Cpu, nvidia::Cuda, SrcIteratorTypeTag, EndIteratorTypeTag, DstIteratorTypeTag>::copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
{
    return nvidia::detail::DeviceCopyImpl<DstIteratorType>::copy_to_device(std::forward<SrcIteratorType>(begin)
                , std::forward<EndIteratorType>(end)
                , std::forward<DstIteratorType>(dst_it));
}

template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
template<typename SrcIteratorType, typename EndIteratorType, typename... DstType>
thrust::device_ptr<DstType...>& DeviceCopy<Cpu, nvidia::Cuda, SrcIteratorTypeTag, EndIteratorTypeTag, DstIteratorTypeTag>::copy(SrcIteratorType&& begin, EndIteratorType&& end, thrust::device_ptr<DstType...>& dst_it)
{
    typedef std::iterator_traits<typename std::remove_reference<SrcIteratorType>::type> SrcIteratorTraits;
    static_assert(std::is_same<std::random_access_iterator_tag, typename SrcIteratorTraits::iterator_category>::value, "Read directly from non-contiguous iterator type not supported (use a preallocated buffer instead)");
    return thrust::copy(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end), dst_it);
}

// Device -> Cpu
template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
typename std::remove_reference<DstIteratorType>::type DeviceCopy<nvidia::Cuda, Cpu, SrcIteratorTypeTag, EndIteratorTypeTag, DstIteratorTypeTag>::copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
{
#ifdef SKA_PANDA_ENABLE_CUDA
    return nvidia::detail::DeviceCopyImpl<SrcIteratorType>::copy_to_host(std::forward<SrcIteratorType>(begin)
                , std::forward<EndIteratorType>(end)
                , std::forward<DstIteratorType>(dst_it));
#else
    return dst_it;
#endif // SKA_PANDA_ENABLE_CUDA
}

template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
template<typename DstIteratorType, typename... SrcType>
typename std::remove_reference<DstIteratorType>::type DeviceCopy<nvidia::Cuda, Cpu, SrcIteratorTypeTag, EndIteratorTypeTag, DstIteratorTypeTag>::copy(thrust::device_ptr<SrcType...>& begin, thrust::device_ptr<SrcType...> const& end, DstIteratorType&& dst_it)
{
    static_assert(sizeof(typename std::iterator_traits<typename std::remove_reference<DstIteratorType>::type>::value_type) >= sizeof(typename std::iterator_traits<thrust::device_ptr<SrcType...>>::value_type), "type conversions during copy from device not yet supported - please ask if you need it");
    return thrust::copy(begin, end, std::forward<DstIteratorType>(dst_it));
}

//Device -> Device
template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
typename std::remove_reference<DstIteratorType>::type DeviceCopy<nvidia::Cuda, nvidia::Cuda, SrcIteratorTypeTag, EndIteratorTypeTag, DstIteratorTypeTag>::copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
{
#ifdef SKA_PANDA_ENABLE_CUDA
    return nvidia::detail::DeviceCopyImpl<SrcIteratorType>::copy_within_device(std::forward<SrcIteratorType>(begin)
                , std::forward<EndIteratorType>(end)
                , std::forward<DstIteratorType>(dst_it));
#else
    return dst_it;
#endif // SKA_PANDA_ENABLE_CUDA
}

template<typename IteratorType>
void copy(IteratorType&& begin, IteratorType&& end, nvidia::CudaDevicePointer<typename IteratorType::value_type>& cuda_ptr)
{
    nvidia::DeviceCopy<IteratorType>::copy_to_device(std::forward<IteratorType>(begin)
                                        , std::forward<IteratorType>(end)
                                        , cuda_ptr
                                        );
}


} // namespace panda
} // namespace ska
