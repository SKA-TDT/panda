/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CUDAHOSTPOINTER_H
#define SKA_PANDA_CUDAHOSTPOINTER_H


namespace ska {
namespace panda {
namespace nvidia {

/**
 * @brief
 *  Manage the lifetime of a cudaHostPtr
 * 
 * @details
 *   Memory allocated on contruction and destroyed on destruction
 */
template<typename DataType>
class CudaHostPointer
{
    public:
        /**
         * @brief allocates the required size of data as a Cuda host Pointer
         */
        CudaHostPointer(std::size_t size);
        CudaHostPointer(CudaHostPointer const&) = delete; // make sure the pointer doesn't get free'd inadvertantly
        CudaHostPointer(CudaHostPointer&&);
        ~CudaHostPointer();
        inline DataType* operator()();
        inline DataType* begin();
        inline DataType* end();
        inline const DataType* begin() const;
        inline const DataType* end() const;
        std::size_t size() const;

    private:
        DataType* _h_data;
        std::size_t _size;
};

} // namespace nvidia
} // namespace panda
} // namespace ska
#include "panda/arch/nvidia/detail/CudaHostPointer.cpp"

#endif // SKA_PANDA_CUDAHOSTPOINTER_H 
