/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_PRODUCER_H
#define SKA_PANDA_PRODUCER_H

#include "panda/Engine.h"
#include "panda/DataManager.h"
#include <queue>


namespace ska {
namespace panda {

/**
 * @brief
 *    Base class for chunking incoming continuous streams into chunks
 * @details
 *
 */
template<typename DerivedType, typename... ChunkTypes>
class Producer
{
        typedef DataManager<DerivedType> ChunkManagerType;
        typedef Producer<DerivedType, ChunkTypes...> SelfType;

    public:
        typedef std::tuple<ChunkTypes...> DataSetType;

    public:
        Producer();
        virtual ~Producer();

        /**
         * @brief
         *   called immediately a chunk_manager becomes available
         */
        virtual void init() {}

        /**
         * @brief
         *   called before the chunk_manager is destroyed
         *   Define in derived class for alternative behaviour
         */
        void stop() {}

        /*
         * @brief returns a managed chunk for writing
         * @details cannot be used until the chunk manager has been provided (i.e in the constructor)
         */
        template<typename ChunkType, typename... Args>
        std::shared_ptr<typename DataType<ChunkType>::type> get_chunk(Args&&...);

        /*
         * @brief called to provide a thread to operate any engines etc.
         * @returns false if OK to continue, true to abort
         * @details specialize in the derived class - n.b. no need for virtual
         *          default will do nothing and return false
         */
        bool process();

    protected:
        /**
         * @brief call from init() to indicate that this data type is nor continuous streaming data
         */
        template<typename DataType>
        void mark_as_service_data();

    private:

        friend ChunkManagerType;

        template<typename T, template<typename> class... StreamTypes>
        friend class ProducerAggregator;
        /*
         * @brief sets the function to generate, propagate & dispose of chunks
         *        used internally by the DataManager class
         */
        void set_chunk_manager(ChunkManagerType* manager);

    private:
        ChunkManagerType* _chunk_manager;
};

} // namespace panda
} // namespace ska

#include "panda/detail/Producer.cpp"

#endif // SKA_PANDA_PRODUCER_H
