/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_LOGLINEHOLDERSENTRY_H
#define SKA_PANDA_LOGLINEHOLDERSENTRY_H

#include <type_traits>

namespace ska {
namespace panda {

template<typename Stream, typename Type>
class ActiveHolder;

// @funtion create_ActiveHolder generates the appropriate internal implementation object for managing the return type from a stream operator
// overload the create_ActiveHolder function for any required specialisations
template<typename Stream, typename Type>
ActiveHolder<Stream, Type>* create_ActiveHolder(Stream& s, Type&& t)
{
    return new ActiveHolder<Stream, Type>(s, std::forward<Type>(t));
}

template<typename Stream>
ActiveHolder<Stream, decltype( std::declval<const std::exception>().what())>* create_ActiveHolder(Stream& s, std::exception const & t)
{
    return new ActiveHolder<Stream, decltype( t.what() )>(s, t.what());
}

template<typename Stream>
ActiveHolder<Stream, decltype( std::declval<std::runtime_error>().what())>* create_ActiveHolder(Stream& s, std::runtime_error& t)
{
    return new ActiveHolder<Stream, decltype( t.what() )>(s, t.what());
}

// helper class to determine the type of Imlementation object
template<typename OStream, typename Type>
struct ActiveHolderType {
    typedef decltype( create_ActiveHolder( std::declval<OStream&>(), std::forward<Type>(std::declval<Type>()) ) ) type;
};

/// Hold the results of a stream << operator and create a suitable follow on container for any further << operations
template<typename Stream, typename Type>
class ActiveHolder
{
    public:
        typedef decltype( std::declval<Stream&>() << std::forward<Type>(std::declval<Type>()) ) ResultType;

    public:
        ActiveHolder(Stream& stream, Type&& value)
            : _stream(stream<<value)
        {
        }
    
        template<typename T>
        typename ActiveHolderType<ResultType, T>::type exec(T&& t)
        {
            return create_ActiveHolder(_stream, std::forward<T>(t));
        }

    private:
        ResultType _stream;
};

/**
 * @brief
 *     The top level sentry to guuard the LogLineHolder. This provideds the interface to allow opeartor<< to work
 *     in both active and non-active contexts
 */
template<typename OStream, typename Type>
class LogLineHolderSentry
{
        typedef typename std::remove_pointer<typename ActiveHolderType<OStream, Type>::type>::type ImplType;
        std::unique_ptr<ImplType> _holder;
        typedef typename ImplType::ResultType Stream;

    public:
        LogLineHolderSentry(ImplType* h)
            : _holder(h) {}
        LogLineHolderSentry(LogLineHolderSentry&&) = default;

        template<typename S, typename T>
        LogLineHolderSentry(S& s, T&& t)
            : _holder(create_ActiveHolder(s, std::forward<T>(t)))
        {
        }

        LogLineHolderSentry()
            : _holder(nullptr) {}

        ~LogLineHolderSentry()
        {
        }

        template<typename T>
        LogLineHolderSentry<Stream, T> operator<<(T&& t)
        {
            typedef LogLineHolderSentry<Stream, T> ReturnType;
            try {
                if(_holder)
                    return ReturnType(_holder->template exec<T>(std::forward<T>(t)));
            } catch(...) {}
            return ReturnType();
        }

};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_LOGLINEHOLDERSENTRY_H 
