/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TASKBASE_H
#define SKA_PANDA_TASKBASE_H

#include "panda/Resource.h"
#include "panda/ResourceJob.h"
#include "panda/TupleUtilities.h"
#include <tuple>
#include <mutex>

namespace ska {
namespace panda {
namespace detail {

/**
 * @brief
 *    Base class for the Task object.
 * @details
 *    Provides a lookup table for functions based on resource type
 */

struct LockingResourceJob : public panda::ResourceJob
{
    public:
        inline
        bool try_lock()
        {
            // the std try_lock may return spurious false results
            do {
                if(_lock.try_lock()) {
                    _locked = true;
                    return true;
                }
            } while(!_locked);
            return false;
        }

    private:
        std::mutex _lock;
        bool _locked;
};

template<typename... Architectures>
struct TaskBase;

template<typename... Architectures>
struct TaskBase<std::tuple<Architectures...>> : public detail::LockingResourceJob
{
    public:
        template<typename Arch>
        using LauncherFunction = std::function<void(PoolResource<Arch>&)>;
        template<typename Arch>
        using CompatabilityFunction = std::function<bool(PoolResource<Arch>const&)>;
        typedef std::tuple<LauncherFunction<Architectures>...> LauncherLookupType;
        typedef std::tuple<CompatabilityFunction<Architectures>...> CompatabilityLookupType;

    public:
        TaskBase(std::tuple<LauncherLookupType, CompatabilityLookupType> functions)
          : _launchers(std::move(std::get<0>(functions)))
          , _compat_checkers(std::move(std::get<1>(functions))) {}


        virtual ~TaskBase() {}

        template<typename Arch>
        CompatabilityFunction<Arch> const& compatability_checker() const {
            return std::get<panda::Index<CompatabilityFunction<Arch>, typename std::remove_const<decltype(_compat_checkers)>::type>::value>(_compat_checkers);
        }

        template<typename Arch>
        void operator()(PoolResource<Arch>& r) const {
          std::get<panda::Index<LauncherFunction<Arch>, typename std::remove_const<decltype(_launchers)>::type>::value>(_launchers)(r);
        }

        template<typename Arch>
        LauncherFunction<Arch> const& launcher() const {
            return std::get<panda::Index<LauncherFunction<Arch>, typename std::remove_const<decltype(_launchers)>::type>::value>(_launchers);
        }

    private:
        const LauncherLookupType _launchers;
        const CompatabilityLookupType _compat_checkers;
};

} // namespace detail
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_TASKBASE_H
