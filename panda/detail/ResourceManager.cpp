/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/ResourceManager.h"
#include "panda/Log.h"

#include <stdexcept>

namespace ska {
namespace panda {

template<class Resource>
ResourceManager<Resource>::ResourceManager(Engine& engine)
    : _destructor(false)
    , _engine(engine)
    , _total_res(0)
{
}

template<class Resource>
ResourceManager<Resource>::~ResourceManager()
{
    stop();
}

template<class Resource>
void ResourceManager<Resource>::stop()
{
    {
        std::lock_guard<std::mutex> lock(_resource_mutex);
        _destructor = true;
    }
    while(_free_resource.size() != _total_res)
    {
        try {
            _engine.poll_one();
        }
        catch(panda::Error const& e) {
            PANDA_LOG_ERROR << "exception processing engine in stop:" << e;
        }
        catch(std::exception const& e) {
            PANDA_LOG_ERROR << "exception processing engine in stop:" << e.what();
        }
        catch(...) {
            PANDA_LOG_ERROR << "unknown exception processing engine in stop";
        }
    }
}

template<class Resource>
inline Engine& ResourceManager<Resource>::engine() const
{
    return _engine;
}

template<class Resource>
void ResourceManager<Resource>::add_resource( Resource r ) {
    std::lock_guard<std::mutex> lock(_resource_mutex);
    ++_total_res;
    _free_resource.emplace_back(r);
    _match_resources();
}

template<class Resource>
void ResourceManager<Resource>::_match_resources() {
     // ensure _resource_mutex is locked before calling this
     if( _queue.size() > 0 ) {
        if( _free_resource.size() > 0 ) {
            _engine.post( std::bind(&ResourceManager<Resource>::_run_job, this, std::move(_free_resource.front()), std::move(_queue.front()) ));
            _queue.pop_front();
            _free_resource.pop_front();
        }
        else {
            PANDA_LOG_DEBUG << "waiting for free resource";
        }
     }
}

template<class Resource>
void ResourceManager<Resource>::_run_job(Resource r, Handler handler) {
    handler(ResourceTracker<Resource>(std::move(r), *this));
}

template<class Resource>
int ResourceManager<Resource>::free_resources() const {
    std::lock_guard<std::mutex> lock(_resource_mutex);
    return _free_resource.size();
}

template<class Resource>
int ResourceManager<Resource>::requests_queued() const {
    std::lock_guard<std::mutex> lock(_resource_mutex);
    return _queue.size();
}

template<class Resource>
void ResourceManager<Resource>::async_request( Handler handler ) {
    std::lock_guard<std::mutex> lock(_resource_mutex);
    if(_destructor) return;
    if( _free_resource.size() > 0 ) {
        _engine.post( std::bind(&ResourceManager<Resource>::_run_job, this, std::move(_free_resource.front()), std::move(handler) ));
        _free_resource.pop_front();
        return;
    }
    _queue.emplace_back(std::move(handler));
#if !NDEBUG
    PANDA_LOG_DEBUG << "waiting for free resource";
#endif
}

template<class Resource>
void ResourceManager<Resource>::_resource_free( Resource res ) {

    std::lock_guard<std::mutex> lock(_resource_mutex);
#if !NDEBUG
    PANDA_LOG_DEBUG << "returning resource";
#endif

    // if we have a job wating pass it on
    if( _queue.size() > 0 ) {
        _engine.post( std::bind(&ResourceManager<Resource>::_run_job, this, std::move(res), std::move(_queue.front()) ));
        _queue.pop_front();
        return;
    }

    _free_resource.emplace_back(std::move(res));
}

} // namespace panda
} // namespace ska
