/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/PoolManagerConfig.h"
#include "panda/PoolConfig.h"
#include "panda/TupleUtilities.h"


namespace ska {
namespace panda {

namespace detail {
    template<typename ConfigType>
    class ReservationHelper {
        public:
            ReservationHelper(ConfigType const& cfg, std::size_t& total_count) 
                : _config(cfg)
                , _total(total_count)
            {}
            
            template<typename Architecture>
            void operator()(Reservation<Architecture>& r) {
                r.set_config(_config);
                r.set_number(_config.template device_count<Architecture>());
                _total+=r.number();
            }

        private:
            ConfigType const& _config;
            std::size_t& _total;
    };
} // namespace detail

template<typename SystemType>
PoolManagerConfig<SystemType>::PoolManagerConfig(std::string name, std::string pool_tag)
    : ConfigModule(std::move(name))
    , _pool_tag(std::move(pool_tag))
{
    add_factory(_pool_tag, [this](){ return new PoolConfigType(_pool_tag); });
}

template<typename SystemType>
PoolManagerConfig<SystemType>::~PoolManagerConfig()
{
}

template<typename SystemType>
void PoolManagerConfig<SystemType>::add_options(OptionsDescriptionEasyInit& )
{
}

template<typename SystemType>
template<typename Architecture>
std::size_t PoolManagerConfig<SystemType>::reserved_count() const
{
    std::size_t count = 0;
    for( auto const& pair : _reservations ) {
        count += std::get<panda::Index<detail::Reservation<Architecture>, typename std::decay<decltype(pair.second)>::type>::value>(pair.second).number();
    }
    return count;
}

template<typename SystemType>
std::size_t PoolManagerConfig<SystemType>::reserve(std::string const& pool_id) 
{
    std::size_t count = 0;
    auto const& cfg = pool_config(pool_id);
    panda::for_each(_reservations[pool_id], detail::ReservationHelper<typename std::decay<decltype(cfg)>::type>(cfg, count));
    return count;
}

template<typename SystemType>
typename PoolManagerConfig<SystemType>::PoolConfigType const& PoolManagerConfig<SystemType>::pool_config(std::string const& pool_id) const
{
    return static_cast<PoolConfigType const&>(get_subsection(_pool_tag, pool_id));
}

template<typename SystemType>
typename PoolManagerConfig<SystemType>::PoolConfigType& PoolManagerConfig<SystemType>::pool_config(std::string const& pool_id)
{
    return static_cast<PoolConfigType&>(get_subsection(_pool_tag, pool_id));
}

template<typename SystemType>
std::vector<std::string> PoolManagerConfig<SystemType>::pool_ids() const
{
    std::vector<std::string> res;
    auto it = subsection(_pool_tag);
    while(it != subsection_end()) {
        res.push_back(it->id());
        ++it;
    }
    return res;
}

template<typename SystemType>
typename PoolManagerConfig<SystemType>::ReservationsMapType const& PoolManagerConfig<SystemType>::reservations() const
{
    return _reservations;
}

} // namespace panda
} // namespace ska
