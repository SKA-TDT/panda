/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/ProducerAggregator.h"
//#include "panda/TupleUtilities.h"


namespace ska {
namespace panda {

template<typename DerivedType, template<typename> class... StreamTypes>
ProducerAggregator<DerivedType, StreamTypes...>::ProducerAggregator(StreamTypes<DerivedType>*... args)
    : _streams(std::unique_ptr<StreamTypes<DerivedType>>(args)...)
{
}

template<typename DerivedType, template<typename> class... StreamTypes>
ProducerAggregator<DerivedType, StreamTypes...>::ProducerAggregator(std::unique_ptr<StreamTypes<DerivedType>>&&... args)
    : _streams(std::move(args)...)
{
}

template<typename DerivedType, template<typename> class... StreamTypes>
ProducerAggregator<DerivedType, StreamTypes...>::~ProducerAggregator()
{
}

namespace detail {
    template<typename T>
    struct Initializer 
    {
        Initializer(T& m) : _m(m) {}

        template<typename StreamType>
        void operator()(StreamType& stream) {
            _m.template _set_stream<StreamType>(stream);
        }

        T& _m;
    };

    struct ProcessWrapper 
    {
        ProcessWrapper() : _rv(false) {}

        template<typename StreamType>
        inline void operator()(StreamType& stream) {
            _rv |= stream->process();
        }

        bool rv() const { return _rv; }
        bool _rv;
    };

    struct StopWrapper 
    {
        StopWrapper() {}

        template<typename StreamType>
        void operator()(StreamType& stream) {
            stream->stop();
        }
    };

} // namespace detal

template<typename DerivedType, template<typename> class... StreamTypes>
bool ProducerAggregator<DerivedType, StreamTypes...>::process()
{
    detail::ProcessWrapper processor;
    panda::for_each(_streams, processor );
    return processor.rv();
}

template<typename DerivedType, template<typename> class... StreamTypes>
void ProducerAggregator<DerivedType, StreamTypes...>::stop()
{
    detail::StopWrapper stopper;
    panda::for_each(_streams, stopper);
}

template<typename DerivedType, template<typename> class... StreamTypes>
void ProducerAggregator<DerivedType, StreamTypes...>::set_chunk_manager(ChunkManagerType* manager)
{
    BaseT::set_chunk_manager(manager);
    detail::Initializer<SelfType> initer(*this);
    panda::for_each(_streams, initer);
}

template<typename DerivedType, template<typename> class... StreamTypes>
template<typename Stream>
void ProducerAggregator<DerivedType, StreamTypes...>::_set_stream(Stream& s) const
{
    s->set_chunk_manager(this->_chunk_manager);
}

} // namespace panda
} // namespace ska
