/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_RECYCLER_H
#define SKA_PANDA_RECYCLER_H

#include <forward_list>
#include <mutex>
#include <memory>

namespace ska {
namespace panda {

/**
 * @brief
 * 
 * @details
 * @tparam RecycleHandler A function that is called immediately rior to recycling the obejct
 *                        It should return true if recycling is required, false to allow the object to be deleted
 */
template<typename RecycledType, typename RecycleHandler>
class Recycler : public std::enable_shared_from_this<Recycler<RecycledType, RecycleHandler>>
{
        typedef Recycler<RecycledType, RecycleHandler> SelfType;

    public:
        Recycler(RecycleHandler);
        ~Recycler();

        void push(RecycledType* obj);

        /**
         * @brief delete all the objects in the cache
         *        after this call the recycler will no longer store any objects until reset()
         *        is called
         */
        void clear();

        /**
         * @brief reset object to be able to store objects after a call to clear() has disabled it
         */
        void reset();

        /**
         * @brief get a recycled object
         */
        std::shared_ptr<RecycledType> pop();

        /**
         * @brief create a new object that will be recycled on destruction
         */
        template<typename... Args>
        std::shared_ptr<RecycledType> create(Args&&... args);

    private:
        bool _destructor;
        RecycleHandler _recycle_fn;
        std::forward_list<RecycledType*> _recycled;
        std::mutex _mutex;
};

} // namespace panda
} // namespace ska
#include "panda/detail/Recycler.cpp"

#endif // SKA_PANDA_RECYCLER_H 
