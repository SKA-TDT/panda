/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Buffer.h"

namespace ska {
namespace panda {

/**
 * @brief default DeviceCopy implementation
 * @details copies from deviceA -> Cpu -> deviceB
 */
template<typename SrcArch, typename DstArch
        , typename SrcIteratorTypeTag
        , typename EndIteratorTypeTag
        , typename DstIteratorTypeTag>
template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
DstIteratorType DeviceCopy<SrcArch, DstArch, SrcIteratorTypeTag, EndIteratorTypeTag, DstIteratorTypeTag>::copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
{
    typedef std::iterator_traits<typename std::remove_reference<DstIteratorType>::type> DstIteratorTraits;

    // create a temporay buffer to transfer between devices
    panda::Buffer<typename DstIteratorTraits::value_type> buffer(std::distance(begin,end));
    panda::copy(begin, end, buffer.begin());
    panda::copy(buffer.begin(), buffer.end(), dst_it);
    return dst_it;
}

} // namespace panda
} // namespace ska
