/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/TimeDurationFormatter.h"


namespace ska {
namespace panda {

template<typename ClockType, typename StreamType>
TimeDurationFormatter<ClockType, StreamType>::TimeDurationFormatter(StreamType& stream)
    : _stream(stream)
    , _count(0)
    , _running_average(0.)
    , _call_running_average(0.)
{
   _last_start = ClockType::now();
}

template<typename ClockType, typename StreamType>
TimeDurationFormatter<ClockType, StreamType>::~TimeDurationFormatter()
{
    _stream << "Timer Summary\n" 
            << "\taverage time inside call   : " << std::chrono::duration_cast<std::chrono::microseconds>(_running_average).count() << "us\n"
            << "\taverage time between calls : " << std::chrono::duration_cast<std::chrono::microseconds>(_call_running_average).count() << "us\n"
            << "\taverage time outside_call  : " << std::chrono::duration_cast<std::chrono::microseconds>(_call_running_average - _running_average).count() << "us\n";
}

template<typename ClockType, typename StreamType>
void TimeDurationFormatter<ClockType, StreamType>::operator()(std::chrono::time_point<ClockType> const& start, std::chrono::time_point<ClockType> const& end) 
{
    DurationType elapsed = end - start;
    DurationType call_elapsed = start - _last_start;
    _last_start = start;
    _running_average = ( decltype(_running_average)(_running_average.count() * _count) + elapsed );
    _call_running_average = (decltype(_call_running_average)(_call_running_average.count() * _count) + call_elapsed );
    _running_average = DurationType( _running_average.count() / ++_count);
    _call_running_average = DurationType( _call_running_average.count() / _count);
    //_stream << "elapsed time: " << elapsed.count() << "ms\n";
}

} // namespace panda
} // namespace ska
