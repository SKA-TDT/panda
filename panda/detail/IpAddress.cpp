/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Error.h"
#include "panda/Log.h"
#include <boost/asio/ip/basic_resolver.hpp>
#include <boost/asio/io_service.hpp>


namespace ska {
namespace panda {

template<typename ProtocolType>
IpAddress::IpAddress(boost::asio::ip::basic_endpoint<ProtocolType> const& ep)
    : _port(ep.port())
    , _address(ep.address())
{
}

template<typename EndPointType>
EndPointType IpAddress::end_point() const
{
    ConceptChecker<EndPointConcept>::Check<EndPointType>::use();
    if (_hostname.empty()) {
        if(address().is_v4() && address().to_string() == "0.0.0.0")
        {
            return EndPointType(EndPointType::protocol_type::v4(), _port);
        }
    }
    else
    {
        boost::system::error_code ec;
        boost::asio::io_service io_service;
        typedef boost::asio::ip::basic_resolver<typename EndPointType::protocol_type> ResolverType;
        ResolverType resolver(io_service);
        typename ResolverType::query query(_hostname, "");
        typename ResolverType::iterator iter = resolver.resolve(query, ec);
        if (ec) {
            panda::Error e("Failed to resolve target host: ");
            e << _hostname;
            throw e;
        }
        else {
            _address = iter->endpoint().address();
            if(_port==0) {
                _port = iter->endpoint().port();
            }
        }
    }
    return EndPointType(address(), _port);
}

} // namespace panda
} // namespace ska
