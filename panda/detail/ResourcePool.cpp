/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/ResourcePool.h"
#include "panda/detail/ResourcePoolImpl.h"
#include "panda/ResourceJob.h"

namespace ska {
namespace panda {

template<unsigned MaxPriority, typename... ArchitecturesT>
ResourcePool<MaxPriority, ArchitecturesT...>::ResourcePool(ProcessingEngine& engine)
    : _pimpl(std::make_shared<detail::ResourcePoolImpl<MaxPriority, ArchitecturesT...>>(engine))
    , _priority(0)
{
}

template<unsigned MaxPriority, typename... ArchitecturesT>
ResourcePool<MaxPriority, ArchitecturesT...>::~ResourcePool()
{
}

template<unsigned MaxPriority, typename... ArchitecturesT>
void ResourcePool<MaxPriority, ArchitecturesT...>::set_priority(unsigned priority)
{
    // preprepare error message so we always get nothing on bad_alloc conditions
    static std::string max_priority_msg = std::string("Pool: requested priority level not supported (max ") + std::to_string(MaxPriority) + ") : ";

    // sanity check
    if(priority > MaxPriority) {
        panda::Error e(max_priority_msg);
        e << priority;
        throw e;
    }

    _priority = priority;
}

template<unsigned MaxPriority, typename... ArchitecturesT>
template<typename Arch>
void ResourcePool<MaxPriority, ArchitecturesT...>::add_resource( std::shared_ptr<Resource> r ) {
    _pimpl->template add_resource<Arch>(std::move(r));
}

template<unsigned MaxPriority, typename... ArchitecturesT>
template<typename Arch>
std::deque<std::shared_ptr<PoolResource<Arch>>> const& ResourcePool<MaxPriority, ArchitecturesT...>::free_resources() const
{
    return _pimpl->template free_resources<Arch>();
}

template<unsigned MaxPriority, typename... ArchitecturesT>
template<typename Arch>
unsigned ResourcePool<MaxPriority, ArchitecturesT...>::free_resources_count() const {
    return _pimpl->template free_resources_count<Arch>();
}

template<unsigned MaxPriority, typename... ArchitecturesT>
template<unsigned Priority, typename TaskTraits, typename... DataTypes>
std::shared_ptr<ResourceJob> ResourcePool<MaxPriority, ArchitecturesT...>::submit(TaskTraits& traits, DataTypes&&... types) const
{
    static_assert( Priority < MaxPriority , "submit priority level requested not supported by this class"); // ensure we never go out of bounds at compile time

    auto task = std::make_shared<detail::Task<TaskTraits, DataTypes...>>(traits, std::forward<DataTypes>(types)...);
    _pimpl->submit(Priority + _priority, task);
    return std::static_pointer_cast<ResourceJob>(task);
}

template<unsigned MaxPriority, typename... ArchitecturesT>
template<typename TaskTraits, typename... DataTypes>
std::shared_ptr<ResourceJob> ResourcePool<MaxPriority, ArchitecturesT...>::submit(TaskTraits& traits, DataTypes&&... types) const
{
    return this->submit<0, TaskTraits, DataTypes...>(traits, std::forward<DataTypes>(types)...);
}

template<unsigned MaxPriority, typename... ArchitecturesT>
void ResourcePool<MaxPriority, ArchitecturesT...>::wait() const {
    return _pimpl->wait();
}

} // namespace panda
} // namespace ska
