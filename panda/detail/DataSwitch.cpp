/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/DataSwitch.h"

namespace ska {
namespace panda {

template<typename... DataTypes>
DataSwitch<DataTypes...>::DataSwitch(DataSwitchConfig& config)
    : _config(config)
    , _switches(std::make_tuple(new DataChannel<DataTypes>(config)...))
{
}

template<typename... DataTypes>
DataSwitch<DataTypes...>::~DataSwitch()
{
    delete_tuple(_switches);
}

template<typename... DataTypes>
template<typename DataType>
typename std::enable_if<HasType<DataChannel<DataType>*, typename DataSwitch<DataTypes...>::ChannelTuple>::value, DataChannel<DataType>*>::type DataSwitch<DataTypes...>::channel()
{
    return std::get<Index<DataChannel<DataType>*, ChannelTuple>::value>(_switches);
}

template<typename... DataTypes>
template<typename DataType>
typename std::enable_if<!std::is_const<DataType>::value && HasType<DataChannel<const DataType>*, typename DataSwitch<DataTypes...>::ChannelTuple>::value, DataChannel<const DataType>*>::type DataSwitch<DataTypes...>::channel()
{
    return std::get<Index<DataChannel<const DataType>*, ChannelTuple>::value>(_switches);
}

template<typename... DataTypes>
template<typename DataType>
typename std::enable_if<HasType<DataChannel<DataType>*, typename DataSwitch<DataTypes...>::ChannelTuple>::value, void>::type
DataSwitch<DataTypes...>::send(ChannelId const& channel_id, DataType& data)
{
    channel<DataType>()->send(channel_id, data);
}

template<typename... DataTypes>
template<typename DataType>
typename std::enable_if<!std::is_const<DataType>::value && HasType<DataChannel<const DataType>*, typename DataSwitch<DataTypes...>::ChannelTuple>::value, void>::type
DataSwitch<DataTypes...>::send(ChannelId const& channel_id, DataType const& data)
{
    channel<const DataType>()->send(channel_id, data);
}

template<typename... DataTypes>
template<typename DataType>
typename std::enable_if<!std::is_const<DataType>::value && HasType<DataChannel<const DataType>*, typename DataSwitch<DataTypes...>::ChannelTuple>::value, void>::type
DataSwitch<DataTypes...>::send(ChannelId const& channel_id, std::shared_ptr<DataType> const& data)
{
    channel<const DataType>()->send(channel_id, data);
}

template<typename... DataTypes>
template<typename DataType>
typename std::enable_if<HasType<DataChannel<DataType>*, typename DataSwitch<DataTypes...>::ChannelTuple>::value, void>::type DataSwitch<DataTypes...>::send(ChannelId const& channel_id, std::shared_ptr<DataType> const& data) {
    channel<DataType>()->send(channel_id, data);
}

template<typename... DataTypes>
template<typename DataType>
        typename std::enable_if<HasType<DataChannel<DataType>*, typename DataSwitch<DataTypes...>::ChannelTuple>::value
                               , typename DataChannel<DataType>::DataChannelHandler&>::type
DataSwitch<DataTypes...>::add(ChannelId const& channel_id, typename DataChannel<DataType>::DataChannelHandler h)
{
    return channel<DataType>()->add(channel_id, std::move(h));
}

template<typename... DataTypes>
template<typename DataType>
        typename std::enable_if<!std::is_const<DataType>::value && HasType<DataChannel<const DataType>*
                               , typename DataSwitch<DataTypes...>::ChannelTuple>::value
                               , typename DataChannel<const DataType>::DataChannelHandler&>::type
DataSwitch<DataTypes...>::add(ChannelId const& channel_id, typename DataChannel<const DataType>::DataChannelHandler h)
{
    return channel<const DataType>()->add(channel_id, std::move(h));
}


template<typename... DataTypes>
template<typename DataType>
bool DataSwitch<DataTypes...>::is_active(ChannelId const& channel_id)
{
    return channel<DataType>()->is_active(channel_id);
}

template<typename... DataTypes>
template<typename DataType>
void DataSwitch<DataTypes...>::activate(ChannelId const& channel_id)
{
    channel<DataType>()->activate(channel_id);
}

template<typename... DataTypes>
template<typename DataType>
void DataSwitch<DataTypes...>::deactivate(ChannelId const& channel_id)
{
    channel<DataType>()->deactivate(channel_id);
}

template<typename... DataTypes>
struct Activator {

    public:
        Activator(ChannelId const& channel)
            : _channel(channel) {}

        template<class T>
        void operator()(T channel_ptr) {
            channel_ptr->activate(_channel);
        }

    private:
        ChannelId const&  _channel;
};

template<typename... DataTypes>
void DataSwitch<DataTypes...>::activate_all(ChannelId const& channel)
{
    panda::for_each(_switches, Activator<DataTypes...>(channel));
}

template<typename... DataTypes>
struct Deactivator {

    public:
        Deactivator(ChannelId const& channel)
            : _channel(channel) {}

        template<class T>
        void operator()(T channel_ptr) {
            channel_ptr->deactivate(_channel);
        }

    private:
       ChannelId const&  _channel;
};

template<typename... DataTypes>
void DataSwitch<DataTypes...>::deactivate_all(ChannelId const& channel)
{
    panda::for_each(_switches, Deactivator<DataTypes...>(channel));
}

} // namespace panda
} // namespace ska
