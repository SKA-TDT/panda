/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Producer.h"
#include "panda/Error.h"
#include "panda/Stream.h"
#include "panda/ServiceData.h"
#include <tuple>
#include <cassert>

namespace ska {
namespace panda {

template<typename DerivedType, typename... ChunkTypes>
Producer<DerivedType, ChunkTypes...>::Producer()
    : _chunk_manager(nullptr)
{
}

template<typename DerivedType, typename... ChunkTypes>
Producer<DerivedType, ChunkTypes...>::~Producer()
{
}

template<typename DerivedType, typename... ChunkTypes>
template<typename ChunkType, typename... Args>
inline std::shared_ptr<typename DataType<ChunkType>::type> Producer<DerivedType,ChunkTypes...>::get_chunk(Args&&... args)
{
    assert(_chunk_manager != nullptr);
    return _chunk_manager->template get_writable<ChunkType>(std::forward<Args>(args)...);
}

template<typename DerivedType, typename... ChunkTypes>
void Producer<DerivedType,ChunkTypes...>::set_chunk_manager(ChunkManagerType* manager)
{
    _chunk_manager = manager;
    init();
}

template<typename DerivedType, typename... ChunkTypes>
template<typename DataType>
void Producer<DerivedType, ChunkTypes...>::mark_as_service_data()
{
    this->_chunk_manager->template mark_as_service_data<DataType>();
}

template<typename DerivedType, typename... ChunkTypes>
bool Producer<DerivedType, ChunkTypes...>::process()
{
    return false;
}

} // namespace panda
} // namespace ska
