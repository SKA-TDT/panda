/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/PoolManager.h"
#include "panda/TupleUtilities.h"
#include "panda/Error.h"
#include <string>


namespace ska {
namespace panda {

namespace detail {
    template<typename Architecture>
    struct DefaultProvisionHelper {

        template<typename PoolManagerType, typename System, typename PoolType>
        inline
        void operator()(PoolManagerType& pm, System& sys, PoolType& pool) {
            // use all remaining resources
            std::size_t available = sys.template count<Architecture>();
            std::size_t reserved = pm.template reserved_count<Architecture>();
            if( available > reserved ) {
                sys.template provision<Architecture>(pool, available - reserved);
            }
        }
    };

    template<typename PoolType, typename SystemType, typename PoolConfigType>
    class ProvisionHelper {
        public:
            ProvisionHelper(SystemType& sys, PoolType& pool)
                : _sys(sys)
                , _pool(pool)
            {
            }

            template<typename Architecture>
            void operator()(Reservation<Architecture> const& reservation) {
                PoolConfigType const& config = static_cast<PoolConfigType const&>(reservation.config());
                auto it = config.subsection(config.template tag<Architecture>());
                while(it != config.subsection_end()) {
                    DeviceConfig<Architecture> const& device_config = static_cast<DeviceConfig<Architecture> const&>(*it);
                    auto new_devices = _sys.template provision<Architecture>(_pool, device_config.device_count());
                    for(auto& device_ptr : new_devices ) {
                        device_ptr->configure(device_config);
                    }
                    ++it;
                }
            }

        private:
            SystemType& _sys;
            PoolType& _pool;
    };
} // namespace detail



template<typename System>
PoolManager<System>::PoolManager(System& s, PoolManagerConfig<System>& config)
    : _system(s)
    , _config(config)
{
}

template<typename System>
PoolManager<System>::~PoolManager()
{
    wait();
}

template<typename System>
typename PoolManager<System>::PoolConfigType& PoolManager<System>::config(std::string const& pool_id)
{
    return _config.pool_config(pool_id);
}

template<typename System>
std::vector<std::string> PoolManager<System>::pool_ids() const
{
    return _config.pool_ids();
}

template<typename System>
ProcessingEngine& PoolManager<System>::engine(std::string const& pool_id)
{
    auto it = _engine_map.find(pool_id);
    if(it == _engine_map.end()) {
        ProcessingEngineConfig& engine_cfg = config(pool_id).engine_config();
        // unless restricted provide enough threads to manage easch device
        if(engine_cfg.number_of_threads() == 0 ) {
            engine_cfg.number_of_threads(_config.pool_config(pool_id).total_device_count());
        }
        std::unique_ptr<ProcessingEngine> e(new ProcessingEngine(engine_cfg));
        ProcessingEngine& rv = *e;
        _engine_map.emplace(std::move(std::make_pair(pool_id, e.get())));
        _engines.emplace_back(std::move(e));
        return rv;
    }
    return *(it->second);
}

template<typename System>
template<typename Architecture>
std::size_t PoolManager<System>::reserved_count() const
{
    return _config.template reserved_count<Architecture>();
}

template<typename System>
typename PoolManager<System>::PoolType& PoolManager<System>::default_pool()
{
    if(!_default_pool) {
        _default_pool.reset(new PoolType(engine("")));
        // provision the default pool with all remainign resoures
        panda::ForEach<Architectures, detail::DefaultProvisionHelper>::exec(*this, _system, *_default_pool);
    }
    return *_default_pool;
}

template<typename System>
void PoolManager<System>::reserve(std::string const& pool_id)
{
    if(pool_id == "default") return;
    if(_config.reserve(pool_id) == 0) {
        panda::Error e("Unknown or empty pool requested :\"");
        e << pool_id << "\"";
        throw e;
    }
}

template<typename System>
template<typename PoolType>
PoolType& PoolManager<System>::create_pool(std::string const& pool_id)
{
    std::unique_ptr<PoolType> pool(new PoolType(engine(pool_id)));
    PoolType& ret = *(pool.get());
    _pool_map.emplace(std::move(std::make_pair(pool_id, pool.get())));
    _pools.emplace_back(std::move(pool));
    provision<PoolType>(ret, pool_id);
    return ret;
}

template<typename System>
typename PoolManager<System>::PoolType& PoolManager<System>::pool(std::string const& pool_id) const
{
    auto it = _pool_map.find(pool_id);
    if(it == _pool_map.end()) {
        if(pool_id == "default") {
            return const_cast<PoolManager<System>&>(*this).default_pool();
        }
        return const_cast<PoolManager<System>&>(*this).create_pool<PoolType>(pool_id);
    }
    return *(it->second);
}

template<typename System>
template<typename PoolTypeT>
void PoolManager<System>::provision(PoolTypeT& pool, std::string const& pool_id)
{
    auto it = _config.reservations().find(pool_id);
    if(it == _config.reservations().end()) {
        // reserve the resources if not already done so
        this->reserve(pool_id);
        provision<PoolType>(pool, pool_id);
        return;
    }
    panda::for_each(it->second, detail::ProvisionHelper<PoolType, System, PoolConfigType>(_system, pool));
}

template<typename System>
void PoolManager<System>::wait() const
{
    for(auto const& pool_ptr : _pools) {
        pool_ptr->wait();
    }
    if(_default_pool) _default_pool->wait();
}

} // namespace panda
} // namespace ska
