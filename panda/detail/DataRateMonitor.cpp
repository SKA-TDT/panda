/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/DataRateMonitor.h"
#include "panda/Log.h"

namespace ska {
namespace panda {


template<typename ClockType>
DataRateMonitor<ClockType>::DataRateMonitor()
    : _data_rates(boost::accumulators::tag::rolling_window::window_size=100)
{
}

template<typename ClockType>
DataRateMonitor<ClockType>::~DataRateMonitor()
{
}

template<typename ClockType>
void DataRateMonitor<ClockType>::operator()(std::size_t number_of_bytes, typename ClockType::duration const& duration)
{
    _data_rates(number_of_bytes/std::chrono::duration_cast<std::chrono::duration<double>>(duration).count()); // convert to bytes/sec
}

template<typename ClockType>
typename DataRateMonitor<ClockType>::TimerType DataRateMonitor<ClockType>::operator()(std::size_t number_of_bytes)
{
    auto self=this->shared_from_this();
    return TimerType(
                    [number_of_bytes, self](typename ClockType::duration const& duration)
                    {
                        (*self)(number_of_bytes, duration);
                    }
           );
}

template<typename ClockType>
typename DataRateMonitor<ClockType>::DataRateType DataRateMonitor<ClockType>::mean() const
{
    return DataRateType(boost::accumulators::rolling_mean(_data_rates) * bytes_per_second);
}

} // namespace panda
} // namespace ska
