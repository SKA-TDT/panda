/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/PropertyTreeUtils.h"
#include <vector>


namespace ska {
namespace panda {

static inline
std::vector<boost::property_tree::ptree const *> search_tree(boost::property_tree::ptree const& pt, typename boost::property_tree::ptree::path_type path)
{
    std::vector<boost::property_tree::ptree const*> res;
    if(path.empty()) {
        res.push_back(&pt);
    }
    else {
        auto iters = pt.equal_range(path.reduce());
        while(iters.first != iters.second) {
            for( boost::property_tree::ptree const* tree : search_tree((iters.first)->second, path) ) {
                if(tree) res.push_back(tree);
            }
            ++(iters.first);
        }
    }
    return res;
}


template<class charT>
boost::program_options::basic_parsed_options<charT>
parse_property_tree(boost::property_tree::ptree const& pt, const boost::program_options::options_description& desc)
{
    boost::program_options::parsed_options result(&desc);

    auto const& options = desc.options();
    for (unsigned i = 0; i < options.size(); ++i)
    {
        const boost::program_options::option_description& d = *options[i];
        
        if (d.long_name().empty())
            continue;
            
        typename boost::property_tree::ptree::path_type p(d.long_name());
        for(const boost::property_tree::ptree* v: search_tree(pt, p)) {
            boost::program_options::option opt;
            opt.string_key = d.long_name();
            opt.value.push_back(v->get_value<std::string>());
            opt.unregistered = false;
            opt.original_tokens.push_back(d.long_name());
            opt.original_tokens.emplace_back(v->get_value<std::string>());
            result.options.emplace_back(std::move(opt));

        }
    }   
    
    // Convert char strings into desired type.
    return boost::program_options::basic_parsed_options<charT>(result);
}

} // namespace panda
} // namespace ska
