/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_SOCKETCONNECTIONIMPL_H
#define SKA_PANDA_SOCKETCONNECTIONIMPL_H

#include "panda/Buffer.h"
#include "panda/AbstractConnection.h"
#include "panda/ConnectionTraits.h"
#include "panda/ErrorPolicyConcept.h"

#include <deque>
#include <mutex>


namespace ska {
namespace panda {
namespace detail {
namespace SocketConnection {
    enum class State { NotConnected, Connecting, Connected, Close };
} // namespace SocketConnection

/**
 * @brief
 *    Implementation of the Socket Connection
 *
 * @details
 *     In a seperate object so we can reference count it within the main class
 */

template<class DerivedType, class ConnectionTraits>
class SocketConnectionImpl : public std::enable_shared_from_this<DerivedType>
{
    BOOST_CONCEPT_ASSERT((ConnectionTraitsConcept<ConnectionTraits>));

    private:
        typedef SocketConnectionImpl<DerivedType, ConnectionTraits> SelfType;

    protected:
        typedef typename ConnectionTraits::EndPointType EndPointType;
        typedef typename ConnectionTraits::ErrorPolicyType ErrorPolicyType;
        typedef typename ConnectionTraits::SocketType SocketType;
        typedef typename ConnectionTraits::EngineType EngineType;
        typedef typename panda::AbstractConnection::BufferType BufferType;
        typedef typename panda::AbstractConnection::SendHandler SendHandler;
        typedef typename panda::AbstractConnection::ReceiveHandler ReceiveHandler;

        BOOST_CONCEPT_ASSERT((ErrorPolicyConcept<ErrorPolicyType>));

    public:
        typedef SocketConnection::State State;

    public:
        SocketConnectionImpl(EngineType& engine);
        SocketConnectionImpl(EngineType& engine, SocketType socket);
        ~SocketConnectionImpl();

        /**
         * @brief asyncronously send the data in the buffer through the connection.
         * @details The handler will be called after the data has been sent or an error occured.
         *          Notice that errors will be propagated according to the policy in place.
         */
        void send(BufferType const buffer, SendHandler const& handler);

        /**
         * @brief asyncronously send the data in the buffer through the connection.
         * @details The handler will be called after the data has been sent or an error occured.
         *          Notice that errors will be propagated according to the policy in place.
         */
        void receive(BufferType buffer, ReceiveHandler handler);

        /**
         * @brief open the socket
         */
        void open();

        /**
         * @brief close an open connection
         */
        void close();

        /**
         * @brief set the remote endpoint
         */
        void set_remote_end_point(EndPointType const& end_point);

        /**
         * @brief return the remote endpoint
         */
        EndPointType const& remote_end_point() const;

        /**
         * @brief return the error handling policy
         */
        ErrorPolicyType& policy();

        /**
         * @brief return the underlying socket object
         */
        SocketType& socket();
        SocketType const& socket() const;

        /**
         * @brief return the underlying engine
         */
        EngineType& engine();

    protected:
        // override for altenative connect behaviour n.b. this is CRTP not virtual methods
        void connect(std::function<void(boost::system::error_code)> const& handler);

        /**
         * @brief default socket async_recv
         * @details override for altenative recieve behaviour n.b. this is CRTP not virtual methods
         * @tparam HandlerType is a void(boost::system::error_code, std::size_t bytes_received)
         */
        template<typename HandlerType>
        void recv(BufferType& buffer, HandlerType handler);

    private:
        void async_connect(std::function<void(Error)> const& connection_handler);
        void _receive(std::shared_ptr<SelfType> self, BufferType buffer, ReceiveHandler handler);

    private:
        std::mutex _mutex; // _awaiting_connection protection mutex
        std::mutex _close_mutex; // for marshalling calls to close
        std::deque<std::function<void(Error const&)>> _awaiting_connection;
        ErrorPolicyType _error_policy;

    protected:
        State _state;
        SocketType _socket;
        EngineType& _engine;
        EndPointType _remote_endpoint;
        std::atomic<std::size_t> _safe_exit; // only safe when 0
};

} // namespace detail
} // namespace panda
} // namespace ska
#include "panda/detail/SocketConnectionImpl.cpp"

#endif // SKA_PANDA_SOCKETCONNECTIONIMPL_H 
