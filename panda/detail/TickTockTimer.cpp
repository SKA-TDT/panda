/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/TickTockTimer.h"

namespace ska {
namespace panda {

template<typename ClockType>
TickTockTimer<ClockType, void*>::TickTockTimer()
    : _last(ClockType::now())
{
}

template<typename ClockType>
TickTockTimer<ClockType, void*>::~TickTockTimer()
{
}

template<typename ClockType>
void TickTockTimer<ClockType, void*>::tick()
{
    _last = ClockType::now();
}

template<typename ClockType>
typename TickTockTimer<ClockType, void*>::TimeDurationType TickTockTimer<ClockType>::operator()()
{
    TimePointType last = _last;
    tick();
    return _last - last;
}

template<typename ClockType>
typename TickTockTimer<ClockType, void*>::TimeDurationType TickTockTimer<ClockType>::tock()
{
    return (*this)();
}

// -- handler specialisation
template<typename ClockType, typename Handler>
TickTockTimer<ClockType, Handler>::TickTockTimer(Handler handler)
    : _handler(std::move(handler))
{
}

template<typename ClockType, typename Handler>
typename TickTockTimer<ClockType>::TimeDurationType TickTockTimer<ClockType, Handler>::operator()()
{
    auto duration = this->BaseT::operator()();
    _handler(duration);
    return duration;
}

template<typename ClockType, typename Handler>
typename TickTockTimer<ClockType, Handler>::TimeDurationType TickTockTimer<ClockType, Handler>::tock()
{
    return (*this)();
}

} // namespace panda
} // namespace ska
