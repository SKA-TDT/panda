/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <algorithm>

namespace ska {
namespace panda {


template<typename ArchT
       , typename PoolType
       , typename std::enable_if<HasType<ArchT, typename PoolType::Architectures>::value, bool>::type
        >
std::size_t PoolLimits::minimum_memory(PoolType const& pool)
{
    if(pool.template free_resources_count<ArchT>() == 0)
    {
        return 0;
    }

    return (*std::min_element(pool.template free_resources<ArchT>().begin()
                            , pool.template free_resources<ArchT>().end()
                            , []( std::shared_ptr<panda::PoolResource<ArchT>> const& a
                                , std::shared_ptr<panda::PoolResource<ArchT>> const& b)
                              {
                                  return a->device_memory() < b->device_memory();
                              }
                             ))->device_memory();
}

template<typename ArchT
       , typename PoolType
       , typename std::enable_if<!HasType<ArchT, typename PoolType::Architectures>::value, bool>::type
        >
constexpr std::size_t PoolLimits::minimum_memory(PoolType const&)
{
    return 0;
}

} // namespace panda
} // namespace ska
