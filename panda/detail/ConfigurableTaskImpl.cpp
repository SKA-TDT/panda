/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/detail/ConfigurableTaskImpl.h"
#include <functional>
#include <thread>

namespace ska {
namespace panda {
namespace detail {


template<class TaskTraitsT, typename... DataTypes>
ConfigurableTaskImpl<TaskTraitsT, DataTypes...>::ConfigurableTaskImpl(PoolManager pool, AlgorithmTuple&& algos, Handler& handler)
    : _algos(std::move(algos))
    , _pool(pool)
    , _handler(handler)
    , _ref_count(0U)
{
}

template<class TaskTraitsT, typename... DataTypes>
ConfigurableTaskImpl<TaskTraitsT, DataTypes...>::~ConfigurableTaskImpl()
{
    while(_ref_count > 0) { std::this_thread::yield(); }
}


template<class TaskTraitsT, typename... DataTypes>
template<typename... Ts>
std::shared_ptr<panda::ResourceJob> ConfigurableTaskImpl<TaskTraitsT, DataTypes...>::do_submit(Ts&&... data)
{
    ++_ref_count;
    try {
        return _pool.submit(*this, std::forward<Ts>(data)...);
    }
    catch(...) {
        --_ref_count;
        throw;
    }
}

template<typename ArchTuple, typename BaseType, class TaskType, typename... DataTypes>
template<typename... BaseArgs>
ExecAlgoGenerator<ArchTuple, BaseType, TaskType, DataTypes...>::ExecAlgoGenerator(BaseArgs&&... args)
    : BaseType(std::forward<BaseArgs>(args)...)
{
}

template<typename ArchTuple, typename BaseType, class TaskType, typename DataType, typename... DataTypes>
template<typename... BaseArgs>
ExecAlgoGenerator<ArchTuple, BaseType, TaskType, DataType, DataTypes...>::ExecAlgoGenerator(BaseArgs&&... args)
    : BaseType(std::forward<BaseArgs>(args)...)
{
}

template<class BaseType, class TaskType, class Arch, typename... Archs, typename DataType, typename... DataTypes>
template<typename... BaseArgs>
ExecAlgoGenerator<std::tuple<Arch, Archs...>, BaseType, TaskType, DataType, DataTypes...>::ExecAlgoGenerator(BaseArgs&&... args)
    : BaseT(std::forward<BaseArgs>(args)...)
{
}

template<typename ArchTuple, typename BaseType, class TaskType, typename DataType, typename... DataTypes>
std::shared_ptr<panda::ResourceJob> ExecAlgoGenerator<ArchTuple, BaseType, TaskType, DataType, DataTypes...>::submit(DataTypeFwd<DataType> d1, DataTypeFwd<DataTypes>... data)
{
    return static_cast<TaskType&>(*this).do_submit(std::forward<DataTypeFwd<DataType>>(d1), std::forward<DataTypeFwd<DataTypes>>(data)...);
}

template<class BaseType, class TaskType, class Arch, typename... Archs, typename DataType, typename... DataTypes>
void ExecAlgoGenerator<std::tuple<Arch, Archs...>, BaseType, TaskType, DataType, DataTypes...>::exec_algo(panda::PoolResource<Arch>& a, DataType& d1, DataTypes&... data) const
{
    static_cast<TaskType const&>(*this).do_exec_algo(a, std::forward<DataType>(d1), std::forward<DataTypes>(data)...);
}

template<class TaskTraitsT, typename... DataTypes>
template<typename Architecture, typename... DataT>
inline void ConfigurableTaskImpl<TaskTraitsT, DataTypes...>::do_exec_algo(panda::PoolResource<Architecture>& a, DataT&&... data) const
{
    typedef typename AlgorithmTraitsType::AlgoArchMapType AlgoArchMapType;
    typedef typename boost::mpl::at<AlgoArchMapType, Architecture>::type AlgoType;
    auto& algo = std::get<panda::Index<AlgoType, decltype(_algos)>::value>(_algos);
    CallHandler<decltype(algo),typename std::decay<decltype(a)>::type, decltype(algo(a, std::forward<DataT>(data)...))>::exec(algo, a, _handler, std::forward<DataT>(data)...);
}

template<class TaskTraitsT, typename... DataTypes>
template<typename Architecture, typename... DataT>
inline void ConfigurableTaskImpl<TaskTraitsT, DataTypes...>::do_sig_exec_algo(panda::PoolResource<Architecture>& a, DataT&&... data) const
{
    typedef typename AlgorithmTraitsType::AlgoArchMapType AlgoArchMapType;
    typedef typename boost::mpl::at<AlgoArchMapType, Architecture>::type AlgoType;
    auto& algo = std::get<panda::Index<AlgoType, decltype(_algos)>::value>(_algos);
    algo(a, std::forward<DataT>(data)...);
}

template<class TaskTraitsT, typename... DataTypes>
template<typename Architecture, typename... Ts>
inline void ConfigurableTaskImpl<TaskTraitsT, DataTypes...>::operator()(panda::PoolResource<Architecture>& a, Ts&&... data) {
    try {
        do_exec_algo(a, std::forward<Ts>(data)...);
    } catch(...) {
        --_ref_count;
        throw;
    }
    --_ref_count;
}

// Generic CallHandler
template<class TaskTraitsT, typename... DataTypes>
template<typename AlgoType, typename ResourceType, typename ReturnType>
template<typename... DataTs>
inline void ConfigurableTaskImpl<TaskTraitsT, DataTypes...>::CallHandler<AlgoType, ResourceType, ReturnType>::exec(AlgoType algo, ResourceType& a, Handler& handler, DataTs&&... data)
{
    handler(algo(a, std::forward<DataTs>(data)...));
}

// CallHandler specialisation for void return types
template<class TaskTraitsT, typename... DataTypes>
template<typename AlgoType, typename ResourceType>
struct ConfigurableTaskImpl<TaskTraitsT, DataTypes...>::CallHandler<AlgoType, ResourceType, void>
{
    template<typename... DataTs>
    static inline void exec(AlgoType algo, ResourceType& a, Handler&, DataTs&&... data);
};

template<class TaskTraitsT, typename... DataTypes>
template<typename AlgoType, typename ResourceType>
template<typename... DataTs>
inline void ConfigurableTaskImpl<TaskTraitsT, DataTypes...>::CallHandler<AlgoType, ResourceType, void>::exec(AlgoType algo, ResourceType& a, Handler& handler, DataTs&&... data)
{
    algo(a, std::forward<DataTs>(data)...);
    handler();
}

template<class TaskTraitsT, typename... SubmitSignature, typename... DataTypes>
template<typename... Args>
ConfigurableTaskImpl<TaskTraitsT, SubmitMethod<SubmitSignature...>, DataTypes...>::ConfigurableTaskImpl(PoolManager pool_manager, AlgorithmTuple&& tuple, Handler& handler, Args&&... args)
    : BaseT(pool_manager, std::move(tuple), handler, std::forward<Args>(args)...)
{
}

template<class TaskTraitsT, typename... SubmitSignature, typename... DataTypes>
std::shared_ptr<panda::ResourceJob> ConfigurableTaskImpl<TaskTraitsT, SubmitMethod<SubmitSignature...>, DataTypes...>::submit(DataTypeFwd<SubmitSignature>... data)
{
    return this->do_submit(std::forward<DataTypeFwd<SubmitSignature>>(data)...);
}

template<class TaskTraitsT, typename MethodFunctor, typename... MethodArgs, typename... DataTypes>
template<typename... Args>
ConfigurableTaskImpl<TaskTraitsT, Method<MethodFunctor, MethodArgs...>, DataTypes...>::ConfigurableTaskImpl(PoolManager pool_manager, AlgorithmTuple&& tuple, Handler& handler, MethodFunctor& method, Args&&... args)
    : BaseT(pool_manager, std::move(tuple), handler, std::forward<Args>(args)...)
    , _functor(method)
{
}

template<class TaskTraitsT, typename MethodFunctor, typename... MethodArgs, typename... DataTypes>
void ConfigurableTaskImpl<TaskTraitsT, Method<MethodFunctor, MethodArgs...>, DataTypes...>::exec_functor(MethodArgs... args)
{
    panda::for_each(this->_algos, _functor, std::forward<MethodArgs>(args)...);
}

template<class TaskTraitsT, typename MethodFunctor, typename... MethodArgs, typename... DataTypes>
template<typename... Args>
ConfigurableTaskImpl<TaskTraitsT, Method<MethodFunctor, Signature<MethodArgs...>>, DataTypes...>::ConfigurableTaskImpl(PoolManager pool_manager, AlgorithmTuple&& tuple, Handler& handler, MethodFunctor& method, Args&&... args)
    : BaseT(pool_manager, std::move(tuple), handler, method, std::forward<Args>(args)...)
{
}

template<class TaskTraitsT, typename MethodFunctor, typename... MethodArgs, typename... OtherMethodArgs, typename... DataTypes>
template<typename... Args>
ConfigurableTaskImpl<TaskTraitsT, Method<MethodFunctor, Signature<MethodArgs...>, OtherMethodArgs...>, DataTypes...>::ConfigurableTaskImpl(PoolManager pool_manager, AlgorithmTuple&& tuple, Handler& handler, MethodFunctor& method, Args&&... args)
    : BaseT(pool_manager, std::move(tuple), handler, method, std::forward<Args>(args)...)
{
}

template<class TaskTraitsT, typename MethodFunctor, typename... MethodArgs, typename... OtherMethodArgs, typename... DataTypes>
void ConfigurableTaskImpl<TaskTraitsT, Method<MethodFunctor, Signature<MethodArgs...>, OtherMethodArgs...>, DataTypes...>::exec_functor(MethodArgs... args)
{
    panda::for_each(this->_algos, this->_functor, std::forward<MethodArgs>(args)...);
}

// ----------------------------------------------------
// ----- Specialisation to support top level Signature declarations
// ----------------------------------------------------
template<typename ArchTuple, typename BaseType, class TaskType, typename... DataTypes>
template<typename... BaseArgs>
SignatureExecAlgoGenerator<ArchTuple, BaseType, TaskType, DataTypes...>::SignatureExecAlgoGenerator(BaseArgs&&... args)
    : BaseType(std::forward<BaseArgs>(args)...)
{
}

template<typename ArchTuple, typename BaseType, class TaskType, typename DataType, typename... DataTypes>
template<typename... BaseArgs>
SignatureExecAlgoGenerator<ArchTuple, BaseType, TaskType, DataType, DataTypes...>::SignatureExecAlgoGenerator(BaseArgs&&... args)
    : BaseType(std::forward<BaseArgs>(args)...)
{
}

template<class BaseType, class TaskType, class Arch, typename... Archs, typename DataType, typename... DataTypes>
template<typename... BaseArgs>
SignatureExecAlgoGenerator<std::tuple<Arch, Archs...>, BaseType, TaskType, DataType, DataTypes...>::SignatureExecAlgoGenerator(BaseArgs&&... args)
    : BaseT(std::forward<BaseArgs>(args)...)
{
}

template<class BaseType, class TaskType, class Arch, typename... Archs, typename DataType, typename... DataTypes>
void SignatureExecAlgoGenerator<std::tuple<Arch, Archs...>, BaseType, TaskType, DataType, DataTypes...>::exec_algo(panda::PoolResource<Arch>& a, DataType& d1, DataTypes&... data) const
{
    static_cast<TaskType const&>(*this).do_sig_exec_algo(a, std::forward<DataType>(d1), std::forward<DataTypes>(data)...);
}

template<class TaskTraitsT, typename... SigArgs, typename... DataTypes>
template<typename... Args>
ConfigurableTaskImpl<TaskTraitsT, Signature<SigArgs...>, DataTypes...>::ConfigurableTaskImpl(PoolManager pool, AlgorithmTuple&& tuple, Handler& handler, Args&&... args)
    : BaseT(pool, std::move(tuple), handler, std::forward<Args>(args)...)
{
}

} // namespace detail
} // namespace panda
} // namespace ska
