/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CONFIGURABLETASKIMPL_H
#define SKA_PANDA_CONFIGURABLETASKIMPL_H
#include "panda/Method.h"
#include "panda/ResourcePool.h"
#include "panda/AlgorithmInfo.h"
#include "panda/AlgorithmTuple.h"
#include "panda/TupleUtilities.h"
#include "panda/Error.h"
#include <type_traits>

namespace ska {
namespace panda {

template<typename... DataTypes>
class SubmitMethod;

namespace detail {

namespace {
template<typename T, typename Enable=void>
struct DataTypeForwarder
{
    typedef T type;
};

template<typename T>
struct DataTypeForwarder<T, typename std::enable_if<std::is_rvalue_reference<T>::value>::type>
{
    typedef T&& type;
};

template<typename T>
struct DataTypeForwarder<T, typename std::enable_if<
                                        std::is_move_constructible<typename std::decay<T>::type>::value
                                    && !std::is_copy_constructible<typename std::decay<T>::type>::value
                                    && !std::is_rvalue_reference<T>::value
                                                   >::type>
{
    typedef T&& type;
};

template<typename T>
using DataTypeFwd = typename DataTypeForwarder<T>::type;

template<typename Arch>
void throw_bad_algo() {
    panda::Error e("algorithm not enabled for architecture ");
    e << to_string<Arch>();
    throw e;
}

} // namespace

template<class BaseTypeT, class PoolManagerT, typename AlgorithmTupleT, typename HandlerT>
struct ConfigurableTaskTraits
{
    typedef BaseTypeT BaseType;
    typedef PoolManagerT PoolManager;
    typedef AlgorithmTupleT AlgorithmTuple;
    typedef HandlerT Handler;
};

/**
 * @brief
 *   Implementation details for ConfigurableTask
 * @details
 *
 */
template<class ArchTuple, typename... DataTypes>
class ConfigurableTaskImplBaseDirectOp
{
    public:
};

template<class Arch1, typename DataType, typename... DataTypes>
class ConfigurableTaskImplBaseDirectOp<std::tuple<Arch1>, DataType, DataTypes...>
{
    public:
        virtual void exec_algo(panda::PoolResource<Arch1>&, DataType&, DataTypes&...) const {
            throw_bad_algo<Arch1>();
        }
};

template<class Arch1, class...Archs, typename DataType, typename... DataTypes>
class ConfigurableTaskImplBaseDirectOp<std::tuple<Arch1, Archs...>, DataType, DataTypes...>
    : public ConfigurableTaskImplBaseDirectOp<std::tuple<Archs...>, DataType, DataTypes...>
{
        typedef ConfigurableTaskImplBaseDirectOp<std::tuple<Archs...>, DataType, DataTypes...> BaseT;

    public:
        using BaseT::exec_algo;
        virtual void exec_algo(panda::PoolResource<Arch1>&, DataType&, DataTypes&...) const {
            throw_bad_algo<Arch1>();
        }
};

template<class PoolManager, typename... DataTypes>
class ConfigurableTaskImplBase : public ConfigurableTaskImplBaseDirectOp<typename std::decay<PoolManager>::type::Architectures, DataTypes...>
{
    public:
        ConfigurableTaskImplBase() {};
        virtual ~ConfigurableTaskImplBase() {};
};

template<class PoolManager, typename... SubmitSignature>
class ConfigurableTaskImplBase<PoolManager, SubmitMethod<SubmitSignature...>>
    : public ConfigurableTaskImplBase<PoolManager>
    , public ConfigurableTaskImplBaseDirectOp<typename std::decay<PoolManager>::type::Architectures, SubmitSignature...>
{
        typedef ConfigurableTaskImplBase<PoolManager> BaseT;
        typedef ConfigurableTaskImplBaseDirectOp<typename std::decay<PoolManager>::type::Architectures, SubmitSignature...> Base2T;

    public:
        virtual std::shared_ptr<panda::ResourceJob> submit(DataTypeFwd<SubmitSignature>...) { return std::shared_ptr<panda::ResourceJob>(); };
};

template<class PoolManager, typename... SubmitSignature, typename DataType, typename... DataTypes>
class ConfigurableTaskImplBase<PoolManager, SubmitMethod<SubmitSignature...>, DataType, DataTypes...>
    : public ConfigurableTaskImplBase<PoolManager, DataType, DataTypes...>
    , public ConfigurableTaskImplBaseDirectOp<typename std::decay<PoolManager>::type::Architectures, SubmitSignature...>
{
        typedef ConfigurableTaskImplBase<PoolManager, DataType, DataTypes...> BaseT;
        typedef ConfigurableTaskImplBaseDirectOp<typename std::decay<PoolManager>::type::Architectures, SubmitSignature...> Base2T;

    public:
        virtual std::shared_ptr<panda::ResourceJob> submit(DataTypeFwd<SubmitSignature>...) { return std::shared_ptr<panda::ResourceJob>(); };
        using BaseT::submit;
        using BaseT::exec_algo;
        using Base2T::exec_algo;
};

template<class PoolManager, typename MethodFunctor, typename... MethodArgs, typename... DataTypes>
class ConfigurableTaskImplBase<PoolManager, Method<MethodFunctor, MethodArgs...>, DataTypes...> : public ConfigurableTaskImplBase<PoolManager, DataTypes...>
{
    public:
        virtual void exec_functor(MethodArgs...) {}
};

template<class PoolManager, typename MethodFunctor, typename... MethodArgs, typename... DataTypes>
class ConfigurableTaskImplBase<PoolManager, Method<MethodFunctor, Signature<MethodArgs...>>, DataTypes...> : public ConfigurableTaskImplBase<PoolManager, DataTypes...>
{
    public:
        virtual void exec_functor(MethodArgs...) {}
};

template<class PoolManager, typename MethodFunctor, typename... MethodArgs, typename... OtherMethodArgs, typename... DataTypes>
class ConfigurableTaskImplBase<PoolManager, Method<MethodFunctor, Signature<MethodArgs...>, OtherMethodArgs...>, DataTypes...>
    : public ConfigurableTaskImplBase<PoolManager, Method<MethodFunctor, OtherMethodArgs...>, DataTypes...>
{
        typedef ConfigurableTaskImplBase<PoolManager, Method<MethodFunctor, OtherMethodArgs...>, DataTypes...> BaseT;

    public:
        using BaseT::exec_functor;
        virtual void exec_functor(MethodArgs...) {};
};

template<class PoolManager, typename MethodFunctor, typename... MethodArgs, typename MethodFunctor2, typename... MethodArgs2, typename... DataTypes>
class ConfigurableTaskImplBase<PoolManager, Method<MethodFunctor, MethodArgs...>, Method<MethodFunctor2, MethodArgs2...>, DataTypes...>
    : public ConfigurableTaskImplBase<PoolManager, Method<MethodFunctor2, MethodArgs2...>, DataTypes...>
{
        typedef ConfigurableTaskImplBase<PoolManager, Method<MethodFunctor2, MethodArgs2...>, DataTypes...> BaseT;

    public:
        using BaseT::exec_functor;
        virtual void exec_functor(MethodArgs...) {}
};

template<class PoolManager, typename... SigArgs>
class ConfigurableTaskImplBase<PoolManager, Signature<SigArgs...>>
    : public ConfigurableTaskImplBase<PoolManager>
    , public ConfigurableTaskImplBaseDirectOp<typename std::decay<PoolManager>::type::Architectures, SigArgs...>
{
        typedef ConfigurableTaskImplBase<PoolManager> BaseT;
        typedef ConfigurableTaskImplBaseDirectOp<typename std::decay<PoolManager>::type::Architectures, SigArgs...> Base2T;

    public:
        using Base2T::exec_algo;
};

template<class PoolManager, typename... SigArgs, typename... DataTypes>
class ConfigurableTaskImplBase<PoolManager, Signature<SigArgs...>, DataTypes...>
    : public ConfigurableTaskImplBase<PoolManager, DataTypes...>
    , public ConfigurableTaskImplBaseDirectOp<typename std::decay<PoolManager>::type::Architectures, SigArgs...>
{
        typedef ConfigurableTaskImplBase<PoolManager, DataTypes...> BaseT;
        typedef ConfigurableTaskImplBaseDirectOp<typename std::decay<PoolManager>::type::Architectures, SigArgs...> Base2T;

    public:
        using Base2T::exec_algo;
        using BaseT::exec_algo;
};

/* the function of the ExecAlgoGenerator classes is to:
 * 1) provide overrides for the virtual methods in the ConfigurableTaskImplBase
 * 2) unwind the ArchTuple for Architecture specific arguments
 */
template<typename ArchTuple, typename BaseType, class TaskType, typename... DataTypes>
class ExecAlgoGenerator : public BaseType
{
    public:
        template<typename... BaseArgs>
        ExecAlgoGenerator(BaseArgs&&...);
};

template<typename ArchTuple, typename BaseType, class TaskType, typename DataType, typename... DataTypes>
class ExecAlgoGenerator<ArchTuple, BaseType, TaskType, DataType, DataTypes...> : public BaseType
{
    public:
        template<typename... BaseArgs>
        ExecAlgoGenerator(BaseArgs&&...);

        std::shared_ptr<panda::ResourceJob> submit(DataTypeFwd<DataType>, DataTypeFwd<DataTypes>...) override;
        using BaseType::submit;
};

template<typename BaseType, class TaskType, class Arch, class... Archs, typename DataType, typename... DataTypes>
class ExecAlgoGenerator<std::tuple<Arch, Archs...>, BaseType, TaskType, DataType, DataTypes...>
    : public ExecAlgoGenerator<std::tuple<Archs...>, BaseType, TaskType, DataType, DataTypes...>
{
        typedef ExecAlgoGenerator<std::tuple<Archs...>, BaseType, TaskType, DataType, DataTypes...> BaseT;

    public:
        template<typename... BaseArgs>
        ExecAlgoGenerator(BaseArgs&&...);
        using BaseT::exec_algo;
        void exec_algo(panda::PoolResource<Arch>&, DataType&, DataTypes&...) const override;
};

template<class TaskTraitsT, typename... DataTypes>
class ConfigurableTaskImpl
    : public ExecAlgoGenerator<panda::tuple_intersection_t<
                                    typename MixedAlgoTraits<typename TaskTraitsT::AlgorithmTuple>::Architectures
                                  , typename std::decay<typename TaskTraitsT::PoolManager>::type::Architectures>
                                  , typename TaskTraitsT::BaseType
                                  , ConfigurableTaskImpl<TaskTraitsT, DataTypes...>, DataTypes...>
{
    private:
        typedef MixedAlgoTraits<typename TaskTraitsT::AlgorithmTuple> AlgorithmTraitsType;
        typedef ExecAlgoGenerator<panda::tuple_intersection_t<
                                    typename MixedAlgoTraits<typename TaskTraitsT::AlgorithmTuple>::Architectures
                                  , typename std::decay<typename TaskTraitsT::PoolManager>::type::Architectures>
                                  , typename TaskTraitsT::BaseType
                                  , ConfigurableTaskImpl, DataTypes...> BaseT;

        template<typename, typename, class, typename... Types>
        friend class ExecAlgoGenerator;

    protected:
        typedef typename TaskTraitsT::PoolManager PoolManager;
        typedef typename TaskTraitsT::AlgorithmTuple AlgorithmTuple;
        typedef typename TaskTraitsT::Handler Handler;

    public:
        typedef typename AlgorithmTraitsType::Architectures Architectures;
        typedef typename AlgorithmTraitsType::Capabilities Capabilities;

    public:
        ConfigurableTaskImpl(PoolManager, AlgorithmTuple&&, Handler&);
        ConfigurableTaskImpl(ConfigurableTaskImpl const&) = delete;
        ~ConfigurableTaskImpl() override;

        template<typename Architecture, typename... Ts>
        inline void operator()(panda::PoolResource<Architecture>& a, Ts&&... data);

        template<typename Architecture, typename... DataT>
        inline void do_exec_algo(panda::PoolResource<Architecture>& a, DataT&&... data) const;

        template<typename Architecture, typename... DataT>
        void do_sig_exec_algo(panda::PoolResource<Architecture>& a, DataT&&... data) const;

    private:
        template<typename AlgoType, typename ResourceType, typename ReturnType>
        struct CallHandler {
            template<typename... DataT>
            static inline void exec(AlgoType algo, ResourceType& a, Handler&, DataT&&... data);
        };

        template<typename AlgoType, typename ResourceType>
        struct CallHandler<AlgoType, ResourceType, void>;

    protected:
        template<typename... Ts>
        std::shared_ptr<panda::ResourceJob> do_submit(Ts&&...);

    protected:
        mutable AlgorithmTuple _algos;
        PoolManager _pool;
        Handler& _handler;
        std::atomic<unsigned> _ref_count;
};

template<class TaskTraitsT, typename... SubmitSignature, typename... DataTypes>
class ConfigurableTaskImpl<TaskTraitsT, SubmitMethod<SubmitSignature...>, DataTypes...>
    : public ExecAlgoGenerator<tuple_intersection_t<
                typename MixedAlgoTraits<typename TaskTraitsT::AlgorithmTuple>::Architectures
              , typename std::decay<typename TaskTraitsT::PoolManager>::type::Architectures>
                             , ConfigurableTaskImpl<TaskTraitsT, DataTypes...>
                             , ConfigurableTaskImpl<TaskTraitsT, DataTypes...>
                             , SubmitSignature...>
{
        typedef ConfigurableTaskImpl<TaskTraitsT, DataTypes...> ImplBaseT;
        typedef typename TaskTraitsT::PoolManager PoolManager;
        typedef typename TaskTraitsT::AlgorithmTuple AlgorithmTuple;
        typedef typename TaskTraitsT::Handler Handler;
        typedef ExecAlgoGenerator<
                    tuple_intersection_t<
                                typename MixedAlgoTraits<AlgorithmTuple>::Architectures
                              , typename std::decay<PoolManager>::type::Architectures
                              >
                  , ImplBaseT, ImplBaseT, SubmitSignature...> BaseT;
        friend class ExecAlgoGenerator<std::tuple<>, typename TaskTraitsT::BaseType, ImplBaseT, SubmitSignature...>;

    public:
        template<typename... Args>
        ConfigurableTaskImpl(PoolManager, AlgorithmTuple&&, Handler&, Args&&...);

        std::shared_ptr<panda::ResourceJob> submit(DataTypeFwd<SubmitSignature>...) override;
        using BaseT::submit;
};

template<class TaskTraitsT, typename MethodFunctor, typename... MethodArgs, typename... DataTypes>
class ConfigurableTaskImpl<TaskTraitsT, Method<MethodFunctor, MethodArgs...>, DataTypes...>
    : public ConfigurableTaskImpl<TaskTraitsT, DataTypes...>
{
        typedef ConfigurableTaskImpl<TaskTraitsT, DataTypes...> BaseT;
        typedef typename TaskTraitsT::PoolManager PoolManager;
        typedef typename TaskTraitsT::AlgorithmTuple AlgorithmTuple;
        typedef typename TaskTraitsT::Handler Handler;

    public:
        template<typename... Args>
        ConfigurableTaskImpl(PoolManager, AlgorithmTuple&&, Handler&, MethodFunctor&, Args&&...);
        void exec_functor(MethodArgs...) override;
        using BaseT::exec_functor;

    protected:
        MethodFunctor& _functor;
};

template<class TaskTraitsT, typename MethodFunctor, typename... MethodArgs, typename... DataTypes>
class ConfigurableTaskImpl<TaskTraitsT, Method<MethodFunctor, Signature<MethodArgs...>>, DataTypes...>
    : public ConfigurableTaskImpl<TaskTraitsT, Method<MethodFunctor, MethodArgs...>, DataTypes...>
{
        typedef ConfigurableTaskImpl<TaskTraitsT, Method<MethodFunctor, MethodArgs...>, DataTypes...> BaseT;
        typedef typename TaskTraitsT::PoolManager PoolManager;
        typedef typename TaskTraitsT::AlgorithmTuple AlgorithmTuple;
        typedef typename TaskTraitsT::Handler Handler;

    public:
        template<typename... Args>
        ConfigurableTaskImpl(PoolManager, AlgorithmTuple&&, Handler&, MethodFunctor&, Args&&...);
        using BaseT::exec_functor;
};

template<class TaskTraitsT, typename MethodFunctor, typename... MethodArgs, typename... OtherMethodArgs, typename... DataTypes>
class ConfigurableTaskImpl<TaskTraitsT, Method<MethodFunctor, Signature<MethodArgs...>, OtherMethodArgs...>, DataTypes...>
    : public ConfigurableTaskImpl<TaskTraitsT, Method<MethodFunctor, OtherMethodArgs...>, DataTypes...>
{
        typedef ConfigurableTaskImpl<TaskTraitsT, Method<MethodFunctor, OtherMethodArgs...>, DataTypes...> BaseT;
        typedef typename TaskTraitsT::PoolManager PoolManager;
        typedef typename TaskTraitsT::AlgorithmTuple AlgorithmTuple;
        typedef typename TaskTraitsT::Handler Handler;

    public:
        template<typename... Args>
        ConfigurableTaskImpl(PoolManager, AlgorithmTuple&&, Handler&, MethodFunctor&, Args&&...);
        void exec_functor(MethodArgs...) override;
        using BaseT::exec_functor;
};

// ----------------------------------------------------
// ----- Specialisation to support top level Signature declarations
// ----------------------------------------------------
template<typename ArchTuple, typename BaseType, class TaskType, typename... DataTypes>
class SignatureExecAlgoGenerator : public BaseType
{
    public:
        template<typename... BaseArgs>
        SignatureExecAlgoGenerator(BaseArgs&&...);
        using BaseType::exec_algo;
};

template<typename ArchTuple, typename BaseType, class TaskType, typename DataType, typename... DataTypes>
class SignatureExecAlgoGenerator<ArchTuple, BaseType, TaskType, DataType, DataTypes...> : public BaseType
{
    public:
        template<typename... BaseArgs>
        SignatureExecAlgoGenerator(BaseArgs&&...);
        using BaseType::exec_algo;
};

template<typename BaseType, class TaskType, class Arch, class... Archs, typename DataType, typename... DataTypes>
class SignatureExecAlgoGenerator<std::tuple<Arch, Archs...>, BaseType, TaskType, DataType, DataTypes...>
    : public SignatureExecAlgoGenerator<std::tuple<Archs...>, BaseType, TaskType, DataType, DataTypes...>
{
        typedef SignatureExecAlgoGenerator<std::tuple<Archs...>, BaseType, TaskType, DataType, DataTypes...> BaseT;

    public:
        template<typename... BaseArgs>
        SignatureExecAlgoGenerator(BaseArgs&&...);
        using BaseT::exec_algo;
        void exec_algo(panda::PoolResource<Arch>&, DataType&, DataTypes&...) const override;
};

template<class TaskTraitsT, typename... SigArgs, typename... DataTypes>
class ConfigurableTaskImpl<TaskTraitsT, Signature<SigArgs...>, DataTypes...>
    : public SignatureExecAlgoGenerator<panda::tuple_intersection_t<
                                            typename MixedAlgoTraits<typename TaskTraitsT::AlgorithmTuple>::Architectures
                                          , typename std::decay<typename TaskTraitsT::PoolManager>::type::Architectures>
                                      , ConfigurableTaskImpl<TaskTraitsT, DataTypes...>
                                      , ConfigurableTaskImpl<TaskTraitsT, DataTypes...>
                                      , SigArgs...>
{
        typedef typename TaskTraitsT::PoolManager PoolManager;
        typedef typename TaskTraitsT::AlgorithmTuple AlgorithmTuple;

        typedef SignatureExecAlgoGenerator<panda::tuple_intersection_t<
                                                typename MixedAlgoTraits<AlgorithmTuple>::Architectures
                                              , typename std::decay<PoolManager>::type::Architectures>
                                          , ConfigurableTaskImpl<TaskTraitsT, DataTypes...>
                                          , ConfigurableTaskImpl<TaskTraitsT, DataTypes...>
                                          , SigArgs...> BaseT;

        typedef typename TaskTraitsT::Handler Handler;

    public:
        template<typename... Args>
        ConfigurableTaskImpl(PoolManager, AlgorithmTuple&&, Handler&, Args&&...);
};

} // namespace detail
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_CONFIGURABLETASKIMPL_H
