/*
 * The MIT License (MIT)
 *
 * Fillright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR FILLRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Copy.h"
#include "panda/Buffer.h"

namespace ska {
namespace panda {

// default is to perform fill on the host and then copy to the device
// This is very inefficient so specialise as required
template<class ArchT>
template<typename SrcIteratorT, typename EndIteratorT, typename ValueT>
void DeviceFill<ArchT>::fill(SrcIteratorT&& begin, EndIteratorT&& end, ValueT const& value)
{
    typedef typename std::iterator_traits<
        typename std::remove_reference<SrcIteratorT>::type
        >::value_type ValT;
    panda::Buffer<ValT> buffer(std::distance(begin, end));
    std::fill(buffer.begin(), buffer.end(), value);
    panda::copy(buffer.begin(), buffer.end()
              , std::forward<SrcIteratorT>(begin));
}

// specialisation for CPU
template<>
struct DeviceFill<Cpu>
{
    template<typename SrcIteratorType, typename EndIteratorType, typename ValueT>
    static inline
    void fill(SrcIteratorType&& begin, EndIteratorType&& end, ValueT const& value)
    {
        std::fill(std::forward<SrcIteratorType>(begin)
                , std::forward<SrcIteratorType>(end)
                , value);
    }
};

} // namespace panda
} // namespace ska
