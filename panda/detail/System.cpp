/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/System.h"
#include "panda/ResourcePool.h"
#include "panda/Error.h"
#include "panda/TupleUtilities.h"
#include "panda/ResourceUtilities.h"
#include "panda/PoolConfig.h"
#include <memory>

namespace ska {
namespace panda {

namespace detail {

    // initialisation helpers
    template<typename Arch>
    struct resource_filler {
        std::vector<std::shared_ptr<PoolResource<Arch>>> operator()() {
            return DiscoverResources<Arch>()();
        }
    };

    template<typename Arch>
    struct count_filler {
        template<typename SystemType>
        unsigned operator()(SystemType& sys) {
            return sys.template count<Arch>();
        }
    };

} // namespace detail

// -----------------------------
// general provisioning struct - finite resource
// -----------------------------
template<typename... Architectures>
template<typename Arch, typename Pool, typename SystemRef>
struct System<Architectures...>::provision_helper {

    template<typename Capability>
    static inline std::vector<std::shared_ptr<PoolResource<Arch>>> provision(SystemRef ref, Pool& pool, unsigned number_of_resources)
    {
        std::vector<std::shared_ptr<PoolResource<Arch>>> new_devices;
        unsigned skipped = 0;
        auto& resources = ref.template unallocated_resources<Arch>();
        while(number_of_resources > 0) {
            if(resources.size() == skipped ) {
                panda::Error e("System: insufficent resources available to meet provisioning request:");
                e << to_string<Arch>();
                throw e;
            }
            if(ResourceSupports<Capability, Arch>::compatible(*resources.back()))
            {
                std::shared_ptr<PoolResource<Arch>>& resource = resources.back();
                std::shared_ptr<PoolResource<Arch>> dev(resource.get()
                                                       , [&](PoolResource<Arch>*)
                                                         {
                                                             // mark for reuse when released by a pool
                                                             ref.make_available(resource);
                                                         });
                //new_devices.push_back(ref.template resources<Arch>().back());
                new_devices.push_back(dev);
                pool.template add_resource<Arch>(dev);
                resources.pop_back();
                --number_of_resources;
            }
            else {
                ++skipped;
            }
        }
        return new_devices;
    }
};

template<typename... Architectures>
template<typename Arch>
struct System<Architectures...>::wait_helper {
    inline
    void operator()(System<Architectures...>& sys) {
        sys.template wait<Arch>();
    }
};

template<typename... Architectures>
System<Architectures...>::System()
    : _resources( detail::resource_filler<Architectures>()()... )
    , _available_resources(_resources)
    , _system_counts( { detail::count_filler<Architectures>()(*this)... } )
{
}

template<typename... Architectures>
System<Architectures...>::~System()
{
    typedef std::tuple<Architectures...> ArchitectureTypes;
    panda::ForEach<ArchitectureTypes, wait_helper>::exec(*this);
}

template<typename... Architectures>
template<typename Arch>
void System<Architectures...>::make_available(std::shared_ptr<PoolResource<Arch>> const& res)
{
    {
        std::lock_guard<std::mutex> lk(_mutex);
        unallocated_resources<Arch>().push_back(res);
    }
    _wait.notify_all();
}

template<typename... Architectures>
template<typename Arch>
std::vector<std::shared_ptr<PoolResource<Arch>>>& System<Architectures...>::unallocated_resources()
{
    return std::get<panda::Index<Arch, SupportedArchitectures>::value>(_available_resources);
}

template<typename... Architectures>
template<typename Arch>
std::vector<std::shared_ptr<PoolResource<Arch>>> const& System<Architectures...>::unallocated_resources() const
{
    return std::get<panda::Index<Arch, SupportedArchitectures>::value>(_available_resources);
}

template<typename... Architectures>
template<typename Arch>
std::vector<std::shared_ptr<PoolResource<Arch>>> const& System<Architectures...>::resources() const
{
    return std::get<panda::Index<Arch, SupportedArchitectures>::value>(_resources);
}

template<typename... Architectures>
template<typename Arch, typename PoolType, typename Capability>
std::vector<std::shared_ptr<PoolResource<Arch>>> System<Architectures...>::provision(PoolType& pool, unsigned number_of_resources)
{
    return provision_helper<Arch, PoolType, decltype(*this)>::template provision<Capability>(*this, pool, number_of_resources);
}

template<typename... Architectures>
template<typename Arch>
std::size_t System<Architectures...>::count() const
{
    return unallocated_resources<Arch>().size();
}

template<typename... Architectures>
template<typename Arch>
std::size_t System<Architectures...>::system_count() const
{
    return _system_counts[panda::Index<Arch, SupportedArchitectures>::value];
}

template<typename... Architectures>
template<typename Arch>
void System<Architectures...>::wait() const
{
    std::unique_lock<std::mutex> lk(_mutex);
    _wait.wait(lk, [this]()
                   {
                        return this->template count<Arch>() == this->template resources<Arch>().size();
                   });
}

} // namespace panda
} // namespace ska
