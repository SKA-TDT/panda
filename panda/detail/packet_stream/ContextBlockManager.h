/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CONTEXTBLOCKMANAGER_H
#define SKA_PANDA_CONTEXTBLOCKMANAGER_H

#include "ContextBlock.h"
#include <memory>

namespace ska {
namespace panda {
   class Engine;
namespace packet_stream {

/**
 * @brief
 *     Primary interface to manage the interface between context blocks
 * @details
 *
 */
template<typename ContextT>
class ContextBlockManager
{
        typedef typename ContextT::ChunkFactory ChunkFactory;
        typedef typename ContextT::DataTraits DataTraits;

    public:
        typedef ContextT ContextType;
        typedef typename ContextT::LimitType LimitType;

    public:
        template<typename... ConfigArgs>
        ContextBlockManager(panda::Engine& engine, ChunkFactory factory, std::size_t number_of_blocks, std::size_t contexts_per_block, ConfigArgs&&... args);
        ContextBlockManager(ContextBlockManager const&) = delete;
        ~ContextBlockManager();

        /**
         * @brief set the number of missing packets that are allowed (i.e will receive missing_packet notifiactions)
         *        before the current chunk should be resized and flushed and a new one started
         */
        void missing_packets_before_realign(LimitType num);

        /// ensure all contexts are purged and data held released
        void release();

        /// return the context for the packet, or nullptr if none available
        ContextT* get_context(typename ContextT::PacketInspector const& packet);

        /// return the currently stored context without matching sequence number
        ContextT& context();

        /// set the engine to be used for delayed job processing
        void engine(panda::Engine& engine);

        /*
         * @brief setup the purge cadence (number of contexts to wait before a context is purged)
         * @param cadence 0=prev ctx, 1=ctx prior to this etc.
         */
        void purge_cadence(LimitType cadence);

    private:
        std::size_t number_of_contexts() const;

    private:
        std::vector<std::unique_ptr<ContextBlock<ContextT>>> _blocks;
        ContextT* _current;
        ChunkFactory _chunk_factory;
};

} // namespace packet_stream
} // namespace panda
} // namespace ska

#include "ContextBlockManager.cpp"

#endif // SKA_PANDA_CONTEXTBLOCKMANAGER_H
