/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "panda/Error.h"

namespace ska {
namespace panda {
namespace packet_stream {

template<typename ContextT>
template<typename... ConfigArgs>
ContextBlockManager<ContextT>::ContextBlockManager(panda::Engine& engine, ChunkFactory factory, std::size_t number_of_blocks, std::size_t contexts_per_block, ConfigArgs&&... args)
    : _blocks(number_of_blocks)
    , _chunk_factory(std::move(factory))
{
    assert(contexts_per_block);
    assert(number_of_blocks);
    _blocks[0].reset(new ContextBlock<ContextT>(engine, _chunk_factory
                                               , contexts_per_block
                                               , std::forward<ConfigArgs>(args)...));
    for(std::size_t i=1; i < number_of_blocks; ++i)
    {
        _blocks[i].reset(new ContextBlock<ContextT>(engine, _chunk_factory
                                                   , contexts_per_block
                                                   , std::forward<ConfigArgs>(args)...));
        // point previous block to this new one
        _blocks[i-1]->next_block(*_blocks[i]);
        _blocks[i]->prev_block(*_blocks[i-1]);
    }
    // create a circular ring of blocks
    _blocks.back()->next_block(*_blocks.front());
    _blocks.front()->prev_block(*_blocks.back());
    _current=_blocks.front()->front();
    assert(_current);

    // setup purge characteristics (requires circular structure to be in place)
    for(std::size_t i=0; i < number_of_blocks; ++i) {
        _blocks[i]->purge_cadence(contexts_per_block);
    }
}

template<typename ContextT>
ContextBlockManager<ContextT>::~ContextBlockManager()
{
    release();
    // we must wait for all contexts to be finished with before
    // we can destroy any ChunkHolder in any block as any context
    // could be dependent on any chunk holder to exist.
    for(auto const& block : _blocks ) {
        block->destroy_contexts();
    }
}

template<typename ContextT>
void ContextBlockManager<ContextT>::missing_packets_before_realign(LimitType num)
{
    for(auto const& block : _blocks ) {
        block->missing_packets_before_realign(num);
    }
}

template<typename ContextT>
std::size_t  ContextBlockManager<ContextT>::number_of_contexts() const
{
    std::size_t s = 0;
    for(auto const& block : _blocks ) {
        s += block->size();
    }
    return s;
}

template<typename ContextT>
void ContextBlockManager<ContextT>::purge_cadence(LimitType num)
{
    if(num > number_of_contexts()) throw panda::Error("purge cadence larger than number of concurrent contexts");
    for(auto const& block : _blocks ) {
        block->purge_cadence(num);
    }
}

template<typename ContextT>
ContextT* ContextBlockManager<ContextT>::get_context(typename ContextT::PacketInspector const& packet_inspector)
{
    ContextT* ctx = _current->get_context(packet_inspector);
    if(ctx) {
        _current=ctx;
    }
    return ctx;
}

template<typename ContextT>
ContextT& ContextBlockManager<ContextT>::context()
{
    return *_current;
}

template<typename ContextT>
void ContextBlockManager<ContextT>::release()
{
    if(_current == nullptr) return;
    _current->release();
}

template<typename ContextT>
void ContextBlockManager<ContextT>::engine(panda::Engine& engine)
{
    for(std::size_t i=0; i < _blocks.size(); ++i) {
        _blocks[i]->engine(engine);
    }
}

} // namespace packet_stream
} // namespace panda
} // namespace ska
