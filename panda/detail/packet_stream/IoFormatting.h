/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_PACKET_STREAM_IOFORMATTING_H
#define SKA_PANDA_PACKET_STREAM_IOFORMATTING_H

#include "panda/CharAsInt.h"
#include <iomanip>

namespace ska {
namespace panda {
namespace packet_stream {

/**
 * @brief
 *   A collection of functions to fix up formatting for packet_stream debug output
 *
 */

template<typename StreamT, typename Arg>
inline
void column_output(StreamT& os, std::string const& name, Arg const& arg)
{
    auto current_width = os.width();
    os << "|" << std::setw(12) << std::right << name << std::setw(3) << " = " << std::left << std::setw(15)
       << static_cast<typename CharAsInt<Arg>::type const&>(arg) << std::setw(current_width);
    static std::string const sep="";
    os << sep;
}

template<typename StreamT, typename Arg>
inline
void last_column_output(StreamT& os, std::string const& name, Arg const& arg)
{
    column_output(os, name, arg);
    static const std::string end("|\n");
    os << end;
}


template<typename StreamT, typename Arg>
static
void row_output(StreamT&& os, std::string const& name, Arg const& arg1, Arg const& arg2)
{
    os << "    "; column_output(os, name, arg1);
    last_column_output(std::forward<StreamT>(os), name, arg2);
}

template<typename StreamT, typename Arg>
static
void row_output(StreamT&& os, std::string const& name, Arg const& arg1, Arg const& arg2, Arg const& arg3)
{
    os << "    ";
    column_output(os, name, arg1);
    column_output(std::forward<StreamT>(os), name, arg2);
    last_column_output(std::forward<StreamT>(os), name, arg3);
}

} // namespace packet_stream
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_PACKET_STREAM_IOFORMATTING_H
