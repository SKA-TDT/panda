/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_MODULONUMBER_H
#define SKA_PANDA_MODULONUMBER_H

#include "panda/Atomic.h"


namespace ska {
namespace panda {

/**
 * @brief Represetation of a number with Modulo arithmetic
 *
 * @details
 *
 */
template<typename T, typename StorageType=T>
class ModuloNumber
{
    public:
        ModuloNumber() = default;
        explicit ModuloNumber(T const& modulo, T const& value = 0);
        ModuloNumber(ModuloNumber const&) = default;
        ModuloNumber(ModuloNumber&&) = default;

        operator StorageType& ();
        operator StorageType const& () const;

        ModuloNumber& operator=(T const&);
        template<typename S>
        ModuloNumber& operator=(ModuloNumber<T, S> const&);
        ModuloNumber& operator+=(T const&);
        template<typename S>
        ModuloNumber& operator+=(ModuloNumber<T, S> const&);
        ModuloNumber& operator-=(T const&);
        template<typename S>
        ModuloNumber& operator-=(ModuloNumber<T, S> const&);

        bool operator==(T const&) const;
        bool operator!=(T const&) const;
        template<typename S>
        bool operator==(ModuloNumber<T, S> const&) const;
        template<typename S>
        bool operator!=(ModuloNumber<T, S> const&) const;

        ModuloNumber& operator++();
        ModuloNumber operator++(int);
        ModuloNumber& operator--();
        ModuloNumber operator--(int);

        ModuloNumber operator+(T const&) const;
        ModuloNumber operator-(T const&) const;
        template<typename S>
        ModuloNumber operator+(ModuloNumber<T, S> const&) const;
        template<typename S>
        ModuloNumber operator-(ModuloNumber<T, S> const&) const;

        /**
         * @brief ordering operator
         * @detail This operator behaves differently from simiilar operator acting on normal types.
         *         In modulo arithmetic the number line can be though of being bent around into a circle
         *         (like a clock, which is ModuloNumber(12)), so that it joins. That means the normal concepts of <
         *         we are familiar with do not apply as any number can be though of < than any other number.
         *         So instead we ask, given our starting point on the clock rim, which way do I have go around
         *         the clock to reach the other number in the shortest path. If the answer is anti-clockwise
         *         then this operator will return true. It will return false if the answer is clockwise or
         *         if it is equi-distant.
         */
        bool operator<(T const&) const;
        template<typename S>
        bool operator<(ModuloNumber<T, S> const&) const;

    private:
        StorageType _val;
        T _modulo;
};

// THis is a partial solution
// TODO Create our own Atomic class wrapping std::atomic to ensure all operations are using the chosen memory ordering
//
template<typename T, std::memory_order MemoryOrdering=std::memory_order_seq_cst>
class AtomicModuloNumber : public ModuloNumber<T, Atomic<T, MemoryOrdering>>
{
        typedef ModuloNumber<T, Atomic<T, MemoryOrdering>> BaseT;

    public:
        explicit AtomicModuloNumber(T const& modulo, T const& value = 0);
        operator T () const;

        T load( std::memory_order order = MemoryOrdering ) const;
};

} // namespace panda
} // namespace ska
#include "detail/ModuloNumber.cpp"

#endif // SKA_PANDA_MODULONUMBER_H
