/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_SYSTEM_H
#define SKA_PANDA_SYSTEM_H

#include "panda/ResourceJob.h"
#include "panda/DiscoverResources.h"
#include "panda/SystemConcept.h"
#include "panda/ConceptChecker.h"

#include <tuple>
#include <array>
#include <tuple>
#include <vector>
#include <map>
#include <type_traits>
#include <memory>
#include <mutex>
#include <condition_variable>

namespace ska {
namespace panda {

// forward declarations
class Resource;
template <typename T> class PoolResource;

/**
 *  \ingroup resource
 *  @{
 *
 * @brief
 *   Describes the available system resources, and arranges them into pools.
 * @tparam Architectures... the architectures to attempt to discover and allocate in the system
 *
 * @details
 *  # Device discovery
 *  For each Architecture in the Architectures parameter list the DiscoverResources<Architecture>::operator()() is invoked
 *  to return a std::vector of suitable types.
 *  If adding new types, a suitable DiscoverResources specialisation should be provided (n.b. this must be in the panda namespace).
 *
 *  # Resource Pools & Use
 *  Each device should be represented as an object of type PoolResource<Architecture>, where Architecture is a tag reperesenting the Architecture of the device
 *  To use the device you need to assign them to an appropriate ResourcePool (using the provision method), and submit suitable tasks for processing
 *  to the pool
 *
 */
template<typename... Architectures>
class System
{
        typedef ConceptChecker<ArchitectureConcept>::Check<Architectures...> ArchitectureConceptCheckArchitectures;

        BOOST_CONCEPT_ASSERT((SystemConcept<System>));

    public:
        typedef std::string ResourceId;
        typedef std::tuple<Architectures...> SupportedArchitectures;

    public:
        System();
        ~System();

        /**
         * @brief assign the number of requested resources to the @param pool
         * @returns list of newly added resources
         * @throw panda::Error if unable to provision requested resources
         * @tparam Arch the type of resource to provision
         */
        template<typename Arch, typename PoolType, typename Capability=std::true_type>
        std::vector<std::shared_ptr<PoolResource<Arch>>> provision(PoolType&, unsigned number_of_resources);

        /**
         * @brief return the number of unassigned resources of the
         *        specified @tparam Arch architecture
         */
        template<typename Arch>
        std::size_t count() const;

        /**
         * @brief return the number of resources that were discovered on the system
         *        specified @tparam Arch architecture
         */
        template<typename Arch>
        std::size_t system_count() const;

        /**
         * @brief return list of detected resources of a specific architecture
         */
        template<typename Arch>
        std::vector<std::shared_ptr<PoolResource<Arch>>> const& resources() const;

        /**
         * @brief return list of available resources of a specific architecture
         * @details does not remove resources from availabel allocation pool
         */
        template<typename Arch>
        std::vector<std::shared_ptr<PoolResource<Arch>>> const& unallocated_resources() const;

    protected:
        /**
         * @brief return list of available resources of a specific architecture
         * @details does not remove resources from availabel allocation pool
         */
        template<typename Arch>
        std::vector<std::shared_ptr<PoolResource<Arch>>>& unallocated_resources();

        template<typename Arch>
        void make_available(std::shared_ptr<PoolResource<Arch>> const&);

        // wait for all resources of the specified type to become unallocated
        template<typename Arch>
        void wait() const;

    private:
        template<typename Arch, typename Pool, typename System> struct provision_helper;
        template<typename Arch> struct wait_helper;

    private:
        std::tuple<std::vector<std::shared_ptr<PoolResource<Architectures>>>...> _resources;
        std::tuple<std::vector<std::shared_ptr<PoolResource<Architectures>>>...> _available_resources;
        std::vector<unsigned> _system_counts;
        mutable std::mutex _mutex;
        mutable std::condition_variable _wait;
};

} // namespace panda
} // namespace ska
#include "panda/detail/System.cpp"

#endif // SKA_PANDA_SYSTEM_H
/**
 *  @}
 *  End Document Group
 **/
