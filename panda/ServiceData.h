/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_SERVICEDATA_H
#define SKA_PANDA_SERVICEDATA_H


namespace ska {
namespace panda {

/**
 * @brief
 *     A template for tagging data types as Service data.
 *     This will affect how the Data is treated in the @class DataManager
 *
 * @details
 *     See the DataManager doc for how his tag is treated in the DataManager 
 */
template<typename Arg>
struct ServiceData {
    ServiceData(ServiceData const&) = delete;
};

/// static function to return the underlying data type, stripping put any ServiceData wrapper
template<typename Arg>
struct DataType {
    typedef Arg type;
};

template<typename Arg>
struct DataType<panda::ServiceData<Arg>> {
    typedef Arg type;
};

template<typename... Args>
struct DataType<std::tuple<Args...>> {
    typedef std::tuple<typename DataType<Args>::type...> type;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_SERVICEDATA_H 
