/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_COMMASEPARATEDOPTION_H
#define SKA_PANDA_COMMASEPARATEDOPTION_H
#include <boost/any.hpp>
#include <vector>

namespace ska {
namespace panda {

/**
 * @brief
 *    Parse commas seperated strings into the required types
 * @details
 *    Use as you would a boost::program_options::value<std::vector<T>>
 *    This will work in exaclty the same way with the addition that it will accept "," seperated values also
 */
template<typename T, class ContainerType=std::vector<T>>
class CommaSeparatedOptions
{
    public:
        explicit CommaSeparatedOptions();
        CommaSeparatedOptions(ContainerType const&);
        ~CommaSeparatedOptions();

        /**
         * @brief conversion operator
         */
        operator ContainerType const&() const { return _values; }

        /**
         * @brief read values from a text input stream
         */
        std::istream& read(std::istream& in);

        /**
         * @brief move the provided data into the container
         */
        void emplace_back(T&& t);


    private:
        ContainerType _values;
};

template<typename T, typename ContainerType>
std::istream& operator>>(std::istream& in, CommaSeparatedOptions<T, ContainerType>& value);

} // namespace panda
} // namespace ska

namespace boost { 
namespace program_options {

// overide function to integrate with boost::program_options
template<typename T, typename ContainerType>
void validate(boost::any& v,
              const std::vector<std::string>& values,
              ska::panda::CommaSeparatedOptions<T, ContainerType>*, int);

} // namespace boost
} // namespace program_options

#include "panda/detail/CommaSeparatedOptions.cpp"
#endif // SKA_PANDA_COMMASEPARATEDOPTION_H 
