/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ERRORPOLICYCONCEPT_H
#define SKA_PANDA_ERRORPOLICYCONCEPT_H

#include "panda/Error.h"
#include "panda/Concept.h"



/**
 * @brief The ErrorPolicyConcept class check the requirements on a SocketErrorPolicy type
 *
 * @details ErrorPolicy types must support a number of methods
 *
 * ErrorPolicy classes must support:
 * @code
 *     bool connection_error(Error const&); // returns true if connection is to be made immediately
 *     bool error(Error const&); // returns true if connection is to be made immediately
 *     bool try_connect(); // returns true if connection is to be made immediately
 *     void connected(); // Tells the object that a connection has been made
 * @endcode
 *
 * \ingroup concepts
 * @{
 */

namespace ska {
namespace panda {

template <class X>
struct ErrorPolicyConcept: Concept<X> {
        typedef Concept<X> Base;

    public:
        BOOST_CONCEPT_USAGE(ErrorPolicyConcept) {
            X& x(Base::reference());

            Error const error;

            Base::is_same(bool(),x.connection_error(error));
            Base::is_same(bool(),x.send_error(error));
            Base::is_same(bool(),x.error(error));
            Base::is_same(bool(),x.try_connect());

            x.connected();
        }
};

} // namespace panda
} // namespace ska

/**
 *  @}
 *  End Document Group
 **/

#endif // SKA_PANDA_ERRORPOLICYCONCEPT_H
