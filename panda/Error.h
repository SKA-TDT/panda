/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ERROR_H
#define SKA_PANDA_ERROR_H

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#define BOOST_SYSTEM_NO_DEPRECATED
#include <boost/system/error_code.hpp>
#pragma GCC diagnostic pop
#include <exception>
#include <stdexcept>
#include <string>
#include <sstream>

namespace ska {
namespace panda {

/**
 * @brief
 *    Save the error condition during async execution
 * 
 */
class Error : public std::runtime_error
{
    public:
        Error();
        Error(Error const&);
        Error(Error&&);
        Error( boost::system::error_code const & ec );
        Error( std::exception const& e );
        Error( std::string const& msg );
        ~Error();

        Error& operator=(Error const& error);

        /**
         * @brief convenience operator<< for adding error information
         */
        Error& operator<<(std::string const& val);
        template<typename T> Error& operator<<(T const& val);


        operator bool() const;

        void set( std::string const & msg );
        void append( std::string const & msg );
        void set( std::exception_ptr const & ex ) { _exception_ptr = ex; }
        void set( boost::system::error_code const & ec ) { _msg = ec.message(); }

        std::exception_ptr const& exception() const { return _exception_ptr; }

        /**
         * @brief returns any error message
         */
        const char* what() const noexcept override;

    private:
        std::exception_ptr _exception_ptr;
        mutable std::string _msg;

};

/**
 * @brief Convenience function template for comparing the type of exception with an expected value
 */
template<typename T> 
bool compare_exception(T const& reference, std::exception_ptr const& e_ptr)
{
    try {
        if(e_ptr) std::rethrow_exception(e_ptr);
    }
    catch(T const& e) {
        return reference == e;
    }
    catch(...) {}
    return false;
}

bool operator==(Error const& e1, Error const& e2);
bool operator!=(Error const& e1, Error const& e2);

std::ostream& operator<<(std::ostream& os, const Error& e);

template<typename T>
Error& Error::operator<<(T const& val)
{
    try {
        std::stringstream ss;
        ss << val;
        if(ss.good()) {
            append(ss.str());
        }
    }
    catch(...)
    {
        // must not throw
    }
    return *this;
}

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_ERROR_H 
