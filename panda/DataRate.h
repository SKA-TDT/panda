/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_DATARATE_H
#define SKA_PANDA_DATARATE_H

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"
#include <boost/units/quantity.hpp>
#include <boost/units/conversion.hpp>
#include <boost/units/derived_dimension.hpp>
#include <boost/units/physical_dimensions/time.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/systems/si/time.hpp>
#include <boost/units/io.hpp>
#pragma GCC diagnostic pop

/**
 * @brief
 *    Representation of data rates in terms of dimension
 *
 * @details
 *    Implemented using the boost units library. The DataRate is a boost::units::quantity
 *    with the base unit bytes/second.
 *    Note you must specify the units when initialising the variable.
 *
 * @code
 * #include "panda/DataRate.h"
 *
 * DataRate<float> dr_float(1.6 * bytes_per_second); // multiple bytes/s
 * DataRate<double> dr_dble(1.0 * byte_per_second);  // a single byte/s n.b.just syntactical sugar, it refers to the same unit
 * 
 * std::cout << dr_float << std::endl;
 *
 * @endcode
 * 
 */


#if BOOST_VERSION < 105700
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"
#include <boost/units/config.hpp>
#include <boost/units/base_dimension.hpp>
#include <boost/units/make_system.hpp>
#include <boost/units/unit.hpp>
#include <boost/units/static_constant.hpp>
#include <boost/units/base_units/si/second.hpp>
#pragma GCC diagnostic pop
// borrowed from later boost::units versions
namespace boost {
namespace units {

/// base dimension of information
struct information_base_dimension : 
    boost::units::base_dimension<information_base_dimension, -700>
{ };

/// dimension of information
typedef information_base_dimension::dimension_type information_dimension;

namespace information {

struct bit_base_unit : public base_unit<bit_base_unit, information_dimension, -700>
{
    static std::string name()   { return("bit"); }
    static std::string symbol() { return("b"); }
};

typedef scaled_base_unit<boost::units::information::bit_base_unit, scale<2, static_rational<3> > > byte_base_unit;
//typedef make_system<byte_base_unit>::type system;

} // namespace information

template<>
struct base_unit_info<information::byte_base_unit> {
    static const char* name()   { return("byte"); }
    static const char* symbol() { return("B"); }
};

} // namespace units
} // namespace boost
#else
#include <boost/units/systems/information/byte.hpp>
#endif

namespace ska {
namespace panda {

typedef boost::units::make_system<boost::units::information::byte_base_unit,
                                  boost::units::si::second_base_unit
                                 >::type data_rate_system;

typedef boost::units::derived_dimension<boost::units::information_base_dimension, 1, boost::units::time_base_dimension, -1>::type DataRateDimensionType;
typedef boost::units::unit<DataRateDimensionType, data_rate_system> DataRateMeasureUnit;
BOOST_UNITS_STATIC_CONSTANT(bytes_per_second, DataRateMeasureUnit); 
BOOST_UNITS_STATIC_CONSTANT(byte_per_second, DataRateMeasureUnit); 

template<typename T>
    using DataRate = boost::units::quantity<DataRateMeasureUnit, T>;


} // namespace panda
} // namespace ska

#endif // SKA_PANDA_DATARATE_H 
