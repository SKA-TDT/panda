/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_DATACHANNEL_H
#define SKA_PANDA_DATACHANNEL_H

#include "panda/ChannelId.h"
#include "panda/Atomic.h"
#include "panda/DataSwitchConfig.h"
#include "panda/ProcessingEngine.h"
#include "panda/detail/HandleWrapper.h"
#include <map>

namespace ska {
namespace panda {

/**
 * @brief
 *    Send data to different named channels. Channels can be dynamically switched on/off
 *
 * @details
 *    A Data Channel consists of one or more Functors that accepts the data type as an argument.
 */
template<typename DataType>
class DataChannel
{
    public:
        typedef std::function<void(DataType&)> DataChannelHandler;

    public:
        DataChannel(DataSwitchConfig const& config);
        DataChannel(DataChannel const&) = delete;
        ~DataChannel();

        /**
         * @brief send the data to the named channel
         * @details Each functor in the channel is called asyncronously
         *          The DataType must support shared_from_this() and be owned by a shared_ptr
         * @throws bad_weak_ptr if the data is not already owned by a shared_ptr
         */
        void send(ChannelId const& channel_id, DataType&);

        /**
         * @brief send the data to the named channel
         * @details send the data to each of the subscribers on the named channel
         */
        void send(ChannelId const& channel_id, std::shared_ptr<DataType> const&);

        /**
         * @brief associate the DataChannelHandler to the named channel
         * @details the channel will automatically become active unless it it explicitly deactivated in the config
         */
        template<typename ChannelHandlerType>
        DataChannelHandler& add(ChannelId const& channel_id, ChannelHandlerType);

        /**
         * @brief activate a channel
         * @throw pand::Error if the channel does not exist
         */
        void activate(ChannelId const& channel_id);

        /**
         * @brief deactivate a channel
         * @details if channel does not exist nothing will happen
         */
        void deactivate(ChannelId const& channel_id);

        /**
         * @brief report if a channel is active
         */
        bool is_active(ChannelId const& channel_id) const;

        /**
         * @brief return the engine associated with a particular channel
         */
        ProcessingEngine& engine(ChannelId const& channel_id);

    private:
        struct HandlerWrapper : DataChannelHandler
        {
            public:
                HandlerWrapper(DataChannelHandler h)
                    : DataChannelHandler(std::move(h)) {}
                HandlerWrapper(HandlerWrapper const &) = delete;
        };

        typedef std::shared_ptr<HandlerWrapper> HandleWrapperType;
        struct ChannelInfo {
            bool _active;
            ChannelInfo(ProcessingEngine& engine) : _active(true), _engine(engine) {}
            ProcessingEngine& _engine;
            std::vector<HandleWrapperType> _handlers;
        };

        DataSwitchConfig const& _config;
        std::map<ChannelId, ChannelInfo> _channel_info;
        Atomic<std::size_t, std::memory_order_relaxed> _handle_count;
};

} // namespace panda
} // namespace ska
#include "panda/detail/DataChannel.cpp"

#endif // SKA_PANDA_DATACHANNEL_H
