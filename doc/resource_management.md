@page resource_management Panda Resource Management

# Introduction
Panda uses the concept of resource pools to manage access to compute hardware.

As a panda user you no longer have to worry about:
- finding a suitable accelerator device (scanning the pci bus etc)
- verifying if a device capabilities matches the requirements for the algorithm
- managing access to the device from competing threads in the same process

Instead the algorithm writer can assume that a device of the correct type will be available
and they can concentrate on optimising the algorithm itself.

# Panda Resource Pools
Any C++ developer who has used a thread_pool will be familiar with the basic concepts
of panda resource pools. To use a thread_pool your code requests an unused thread to
execute your code. When the code is finished, it releases the thread back to the pool
so it can be reassigned to another task.

Panda takes this same idea and extends it beyond just CPU based threads to include any type of accelerator device.

## Using a panda resource pool
Given a panda @ref ska::panda::ResourcePool "resource pool" object a user needs to submit a task to be executed to the pool.
This is in the form of a task object which at its heart is a callback handler that is executed whenever a suitable resource becomes available.
For convenience you can pass other data along with the task object that will be forwarded on to the handler at the apropriate time.

e.g.
~~~~{.cpp}
pool.submit(my_task, data_ptr); // n.b. any data types passed will be copied, therefore shared_ptr/unique_ptr are recommended for large objects
~~~~

In order for the ResourcePool to know what type of device is required the Task object must provide certain other information:
- It must provide one or more @ref ska::panda::Architecture "Architecture tags" to indicate which sort of devices it can use.
- For each Architecture it can also specifiy a list of minimal capabilities (e.g memory size, API version) that a device of that sort must support.

See the  @ref ska::panda::ResourcePool::submit "resource pool" doc for more details.

## Setting Up ResourcePools
Before any accelerator device can be put into a ResourcePool a process of system discovery must take place.
Panda is designed to adapt to the system it is being run on and does not assume fixed architecture configurations.
The job of device discovery is given to the @ref ska::panda::System object.

Once initialised the System object provides methods to assign any number of devices of a given type to a pool.
e.g.
~~~~{.cpp}
ska::panda::System<Cuda, Cpu> system; // we only care about Cuda and Cpu (cores) in this example
system.template provision<ska::panda::Cuda>(pool, 2); // add 2 Cuda devices to the pool object
~~~~

Panda allows you the flexibility to create as many pools as you wish and assign devices to them appropriately.
By providing different pools to different parts of your processing pipeline you can have a great deal of control
over which parts of the code can use a particular resource and what parts must share a resource between them.
Within pools you can also assign priority queues to further refine access.

@anchor pool_configuration_helpers
### Pool Configuration Helpers
As this is the panda model for resource management it should come as no surprise that panda provides several other
classes to facilitate the configuration and setup of pools for application developers. These are:

| Panda class     | Function         |
|-----------------|------------------|
| @ref ska::panda::PoolConfig "PoolConfig"     | A configuration module to allow users to specify and name a single pool |
| @ref ska::panda::PoolManagerConfig "PoolManagerConfig"     | A drop in user configuration module to allow users to specify and name multiple pools (a collection of PoolConfig objects) |
| @ref ska::panda::PoolManager "PoolManager"     | A pool factory to generate pools according to the settings of a PoolManagerConfig object  |
| @ref ska::panda::PoolSelector "PoolSelector"   | Extends a configuration node with options to allow users to associate a named pool\. This extended node has a pool() method that gives the correct pool object (and priority queue) specified by the user configurations |
| @ref ska::panda::Config "Config"   | An application level configuration tree featuring a PoolManager, help, and config file selection |

The last of these @ref ska::panda::Config "panda::Config" is probably the best place to start. An example
of its use is the @ref foobar_example "foobar example application".
