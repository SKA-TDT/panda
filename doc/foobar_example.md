@page foobar_example Asyncronous Tasks in Panda: The foobar_example async application

This tutorial is designed to demonstrate a working asyncronous pipeline that uses the panda
resource manager.

The foobar_example application is a somewhat contrived two stage asyncronous pipeline.
In this application a message is passed to the Foo module for processing asyncronously. It is then passed onto the Bar module, also for processing asyncronously at which point the program ends.

We focus only on async tasks and using the panda resource management facilities.\n
We deliberately exclude elements of a full panda app, such as producers and exporters
in order to maintain focus on the async resouce management issues.

The application has configuration options that allow the application user to specify pools,
and assign them to the Foo and Bar modules. It comes complete with help and other useful options.

## Learning Points
- create an application that accepts user options on the command line and/or a configuration file
   - automatically generated user --help option
   - automated configuration file description generation
   - user selectable pools in config
- how to create an async module
    - async module configurations
        - obtain the correct user configured pool
        - adding simple options
    - the difference between sync and async calls
    - specifying acceptable accelerator types to perform a task
    - how to submit a job to the pool
    - error handling in async tasks
    - callback handlers
        - basic use
        - understanding the main handler pattern API designs
- create a simple pipeline containing a daisy chain of two async modules

## Walk Through
### The main executable

We start by taking a quick look at the main function (the entry point for all C++ programs).
@include foobar/main.cpp

We see that there are two objects, one that manages the application's configuration to which we pass down the
command line arguments.\n
The second is the app object itself, which takes the configuration in its constructor.

The main thread (i.e. the thread that is executing the code in the main function) calls the run() method on the application object.
The run method has been written to be asyncronous and so it will return as soon as it has scheduled the foobar pipeline for execution. It will not wait until the foobar pipeline has finished.\n
Continuing to trace through the code snippet above, the next thing the main thread will do is to exit main() terminating the program. In so doing it will call the FooBarApp's destructor.\n
As the app object is in use, busy scheduling and completing the
required work we have requested, this could prove to be highly inconvenient.
It is important therefore, when designing such an app, that the destructor will not complete until all the associated scheduled
async tasks are finished.

### The Application Pipeline FooBarApp
The role of the FooBarApp is to link the processing modules together
to form a processing pipeline, so that the result from an async task
launched by Foo is passed on as input to the Bar module.
The pipeline also has to provide an API to allow the developer using it
to launch and manage this pipeline safely.

#### Connecting Async Modules to Form a Pipeline
The FooBarApp class is composed (i.e. they are members of our class)
from the Foo and Bar objects. These objects are will do the real
work, all this class has to do is to make sure they are connected together in the correct order.\n

This chaining together of modules is done in the constructor
@snippet foobar/FooBarApp.h FooBarApp Constructor
Here the object _bar, is passed to our _foo object as its async handler.
We will discuss async handlers later in this tutorial, but for now it
is enough to know that they are the thing (a Functor) that gets called
when a tssk is complete. So in this example we have told _foo to
call _bar whenever it finishes an async task.\n
Notice the order of construction is the reverse of the order in which
we expect to call these modules inside the pipeline.

#### The pipeline launcher
We provide a run() method to allow the user of our class to launch
the foo bar pipeline.
@snippet foobar/FooBarApp.h FooBarApp run

The run() method does nothing more than call the first module in
the chain using its async interface.

The full FooBarApp source and its Config can be found below:
@include foobar/FooBarApp.h

### The async modules
Each of our modules, Foo and Bar, are designed to have an asyncronous interface. In order to implement such an interface
the module needs access to a ResourcePool object to which it can submit jobs for processing. The pool object is provided
in the modules contructor via its Config object, FooConfig and BarConfig respectively.

#### Module configuration
These configuration classes are written to be @ref ska::panda:ConfigModule nodes so that they can easily be added to any configuration tree.
By using the panda::PoolSelector module, in this example by inheritance, we add the ability to associate a named pool, and a queue priority
level for the module. The PoolSelector will provide us with a @ref ska::panda::PoolSelector::pool "pool()" method that will provide us
with the configured pool. Note the object provided is not the pool object itself, but an object that wraps the requested pool object,
and provides a submit() method for jobs that will use the requested priority queue on that pool.

The Bar module offers no other options than pool selection.
@include foobar/BarConfig.h

The Foo module, by contrast allows you to specify a prefix string to add to its message.
This is a very simple option utilising the functionality of boost::program_options library for illustrative purposes.
The full range of boost::program_options libary and panda::ConfigModule are available to you to extend this to be as complex as you need.
@include foobar/FooConfig.h

You should run the executable with the --help option to see how these options are made available to the end user:
@code
foobar_example --help
@endcode
@code
foobar_example --help-module foo
@endcode

The same options are also available to you via a configuration file. To see the format of this file:
@code
foobar_example --help-config-file
@endcode

#### Module API - Async tasks
Both of our modules support a single async call that takes a std::string.
The purpose of such an async task is to schedule a job to be performed by the resources available
in the allocated pool whenever they become available. Once scheduled the call will return immediately,
not waiting for the task to complete. This allows the calling thread to go and do other things without blocking.

Note that if for some reason you did want to wait for the result, the pool submit method does return a
@ref ska::panda::ResourcePoolJob object which allows you to monitor the status of any given job and
@ref ska::panda::ResourcePoolJob::wait() for the it to complete.

As a user of such a module you are spared the details of how a
task is actually performed, or to find a suitable accelerator device to run on etc.
You just need to ask for the task to be done and provide the data on which to perform the operation.

If we look at the async Foo::operator() we see how we can use our Config object to get a pool to which we can submit a job.

@snippet foobar/Foo.h Async operator

The @ref ska::panda::ResourcePool::submit "submit method" of the pool
takes as its first argument a functor to be called when a suitable resource becomes available. The remaining arguments are data that will be
passed to that functor when the resource becomes ready. The functor and its associated data is called a task or job.

In this case we are using the Foo object itself as the functor and so, in order to meet the requirements of the submit method, this functor has to provide:
- a public typdef Architecture to indicate the type of resource required from the panda Scheduler.\n
    e.g. in Bar we tell the scheduler we want a CPU thread with:
~~~~{.cpp}
class Bar {
    public:
        typedef ska::panda::Cpu Architecture;
~~~~
- alternatively a public tyepdef Architectures which should be a std::tuple of Architecture flags if the
  job object can utilise more than one architecture.\n
    e.g. in Foo we tell the scheduler we can use either
    a CPU thread, or an NVidia GPU with:

    @snippet foobar/Foo.h Architectures Spec

    Note the macro processor guards which allow us to specify cuda
    only when panda has been compiled with nvidia Cuda support
    (with the cmake -DENABLE_CUDA=true switch).

    These definitions are found in panda/Configuration.h

- For each supported Architecture indicated by these tags, the job must provide an operator() method that
  takes as its first argument a reference to a matching @ref ska::panda::PoolResource object. The remaining
  arguments should match those passed to the pool submit function in the async call.
    e.g. Foo
    @snippet foobar/Foo.h Sync operator

    and in Bar
    @snippet foobar/Bar.h Sync operator

An advantage of designing our module API as a job, is that these same operator() methods called by the panda scheduler
can also be used directly by users who have already secured a device and wish to call the module in a syncronous fashion.
This gives the library user the option to choose on a case by case basis wheter to avoid the scheduler overhead
at the expense of less load nbalancing etc.

#### the async handler
Your other duty as a user is to specify what should happen when the task is finished.\n
This is the role of a Handler, which is a callback that is executed (in a seperate thread) when the task is complete,
passing on the results of the computation just performed.

In our example, only our Foo module allows you to specify a Handler callback.\n
We have chosen to require the type of handler to be passed as a template parameter, and this has to be initialised at construction.

@snippet foobar/Foo.h Handler
Alternatively, we could have asked the user to provide a fixed type std::function as a parameter to each async call.
e.g
~~~~{.cpp}
void operator()(std::string const& msg, std::function<void(std::string const&)> const& results_handler);
~~~~
Whilst this might prove to be more convenient to the user in some circumstances,
we have chosen the former technique as:
- it is more efficient (the std::function requires a virtual, and the compiler has more options to optimise)
- it allows you to specify the same handler object for different results e.g. error conditions
- We can always provide an interface that accepts a handler in the async call if required by wrapping this object with a suitable interface

Full source code for Foo and Bar modules can be found va the following pages:
- @ref foobar/Foo.h
- @ref foobar/Bar.h

#### async error handling
When a call is made to an async method, any errors encountered during the submit will be immediatley reported
via an exception, to the caller. After submission, when the task begins to execute, the caller cannot be directly notified if an error is encountered.\n
There are several ways to handle such errors. It is rather up to the module interface designer which
is the best method.\n
We outline the three main methods here for completeness:
- throw an exception: An exeception not caught by the task will be caught by the panda resource sheduler.
  The scheduler will report and log the exception but otherwise ignore it. This is what would happen in our example modules here.
- Require both a results handler, and an error handler. The task would have to trap any exceptions and call the appropriate
  handler. As mentioned above, having a predefined Handler type allows you to combine these two into a single object that supports
  multiple methods specifalised for handling error status objects or the results.
- Define an handler that takes both the results and some sort of error object. The onus is then on the user to check the error object
  for its status when it receives the callback and act appropriately (this is the strategy used by boost::asiolibrary). This is slightly
  less efficient as we have to insert an if condition to do the check at the callback level, rahter than using tha knowledge that we already
  have an error condition and call the appropriate handler directly.
