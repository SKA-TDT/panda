\anchor Main
\mainpage Introduction

# PANDA - the Pipeline for ANalysis of DAta (PANDA)

\section main_whatis What is PANDA?
This project provides a framework for building data processing
applications.\n
Panda is a c++ 2011 library relying heavily on templates. This allows for very strong typing and compile time optimisation.

Panda is designed to support:
- multi-threaded async tasks
- multiple heterogeneous hardware acceleration devices
  (e.g. GPU, FPGA)
- automatic load balancing
- detection and automated adaptation for supported hardware accelerators
- user configurable allocation of compute resource to specific parts of the processing pipeline

An application using panda would structure their data processing pipeline
as a series of daisy chained async processing tasks.
Each task is submitted to a panda managed resource pool for completion.

# Getting Started:
- @subpage download "Obtaining a copy of PANDA"
- @subpage build "Building PANDA"
- @subpage for_cpp_developers "C++ in PANDA"

# Panda Functionality
- @subpage resource_management "Resource Management With Resource Pools"
- @subpage device_algorithms "Device Algorithms"
- @subpage app_config "Application Configuration Utilities"
- @subpage network_utils "Network Utilities"
- @subpage cmake_functions "cmake functions"

Its primary goal is to provide configurable pools of compute resources to which jobs (async tasks) can be
submitted for processing via a queue.

These async tasks can be composed of more than one algorithm, each optimised to a sepcific device.
As a resource in the pool becomes available, the next task in the queue that supports that device
will be executed.

## Other documents

- [Developer Guide](@ref md_doc_developers_guide)
- [Pipeline Documentation](@ref PDPPipeline)
- [User Guide](@ref md_doc_user_guide)

\defgroup config Configuration
\defgroup connectivity Network Utilities
\defgroup resource Tasks and Resource Management
\defgroup sinks Data Sinks
\defgroup concepts Concepts
\defgroup testutil Test Utilities
